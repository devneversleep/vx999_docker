<?php

namespace App\Libraries;

use Illuminate\Support\Facades\Http;
use Log;

class SMS
{
    protected static $instance;

    protected function __construct()
    {
        // $this->secret_key = config('variable.igame_secret');
        // $this->api_url = config('variable.igame_url');
    }

    public static function get_instance()
    {
        if (empty(SMS::$instance)) {
            SMS::$instance = new SMS();
        }

        return SMS::$instance;
    }

    public function send_otp($phone_number, $random_otp, $country_code)
    {      
        $username = 'noiinong';
        $password = 'Na30112531';
        $phone_number_laos = $country_code . substr($phone_number, 1) ;
        $user_pass = \base64_encode($username . ':' . $password);
        $message_otp = 'OTP ' . $random_otp;

        $response = Http::withHeaders(['Authorization' => 'Basic ' . $user_pass])->send('get', 'https://api.bulksms.com/v1/messages/send?to=' . $phone_number_laos . '&body=' . $message_otp, []);
        // Log::notice($response);
        // $result = $this->send_message( json_encode($messages), 'https://api.bulksms.com/v1/messages?auto-unicode=true&longMessageMaxParts=30', $username, $password );

        return $response;
        
    } 

    // private function send_message( $post_body, $url, $username, $password) 
    // {
    //     $ch = curl_init( );
    //     $headers = array(
    //         'Content-Type:application/json',
    //         'Authorization:Basic '. base64_encode("$username:$password")
    //     );
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    //     curl_setopt ( $ch, CURLOPT_URL, $url );
    //     curl_setopt ( $ch, CURLOPT_POST, 1 );
    //     curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
    //     curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_body );
    //     // Allow cUrl functions 20 seconds to execute
    //     curl_setopt ( $ch, CURLOPT_TIMEOUT, 20 );
    //     // Wait 10 seconds while trying to connect
    //     curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
    //     $output = array();
    //     $output['server_response'] = curl_exec( $ch );
    //     $curl_info = curl_getinfo( $ch );
    //     $output['http_status'] = $curl_info[ 'http_code' ];
    //     $output['error'] = curl_error($ch);
    //     curl_close( $ch );
    //     return $output;
    //   }

}