<?php

namespace App\Libraries;

use Illuminate\Support\Facades\Http;
use Jenssegers\Agent\Agent;

class IGT
{
    protected static $instance;

    protected function __construct()
    {
        $this->secret_key = config('variable.igame_secret');
        $this->api_url = config('variable.igame_url');
    }

    // * Singleton Method ------------------------------------------------------------------
    public static function get_instance()
    {
        if (empty(IGT::$instance)) {
            IGT::$instance = new IGT();
        }

        return IGT::$instance;
    }
    // * -----------------------------------------------------------------------------------

    // * private Method --------------------------------------------------------------------
    private function set_header($auth = false, $ip = null)
    {
        $header = [];
        if (!is_null($ip)) {
            $header['X-IGAME-CLIENT-IP'] = $ip;
        }

        if ($auth) {
            $header['X-IGAME-TOKEN'] = session('token', null);
        }

        return $header;
    }

    private function encrypt_data($data)
    {
        $encrypt_data = \base64_encode(\openssl_encrypt(json_encode($data), 'DES-CBC', $this->secret_key, OPENSSL_RAW_DATA, $iv = 'IGameApi'));
        return $encrypt_data;
    }

    private function http_request($method, $url, $header = [], $body = null)
    {
        $response = Http::withHeaders($header ? $header : [])->send($method, $this->api_url . $url, ['body' => $body]);

        return $response->json();
    }
    // * -----------------------------------------------------------------------------------

    // * Public Method =====================================================================

    // * Bill Method -----------------------------------------------------------------------
    public function deposit($amount, $ip, $promotion_code = null)
    {
        $header = $this->set_header(true, $ip);
        $json = ['amount' => (float) $amount];
        if (!is_null($promotion_code)) {
            $json['promotion_code'] = $promotion_code;
            $json['validate_promotion'] = (int) 1;
        }

        $response = $this->http_request('POST', 'account/deposit', $header, $this->encrypt_data($json));
        switch ($response['code']) {
            case 200:
                $response['status'] = 'success';
                return $response;
            case 400:
                $error = [];
                if (isset($response['field_errors']['errors'])) {
                    $error['errors'] = $response['field_errors']['errors'][0];
                }
                foreach ($response['field_errors']['children'] as $key => $value) {
                    if (array_key_exists('errors', $value)) {
                        $error[$key] = $value['errors'];
                    } else if (!empty($value)) {
                        $error[$key] = $value;
                    }
                }
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $error];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }

    public function withdraw($amount)
    {
        $header = $this->set_header(true);

        $json = ['amount' => (float) $amount];
        $response = $this->http_request('POST', 'account/withdraw', $header, $this->encrypt_data($json));

        switch ($response['code']) {
            case 200:
                $response['status'] = 'success';
                return $response;
            case 400:
                $error = [];
                foreach ($response['field_errors']['children'] as $key => $value) {
                    if (array_key_exists('errors', $value)) {
                        $error[$key] = $value['errors'];
                    }
                }
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $error];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }

    public function confirm_deposit()
    {
        $header = $this->set_header(true);
        $response = $this->http_request('PUT', 'account/bill/confirm/deposit', $header);

        switch ($response['code']) {
            case 200:
                $response['status'] = 'success';
                return $response;
            case 404:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $response['message']];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }

    public function cancel_deposit()
    {
        $header = $this->set_header(true);
        $response = $this->http_request('PUT', 'account/bill/cancel-pending/deposit', $header);

        switch ($response['code']) {
            case 200:
                $response['status'] = 'success';
                return $response;
            case 404:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $response['message']];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }

    public function check_pending_deposit()
    {
        $header = $this->set_header(true);

        $response = $this->http_request('GET', 'account/bill/check-pending/deposit', $header);
        switch ($response['code']) {
            case 200:
                $response['status'] = 'success';
                return $response;
            case 404:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $response['message']];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }

    public function check_pending_withdraw()
    {
        $header = $this->set_header(true);

        $response = $this->http_request('GET', 'account/bill/check-pending/withdraw', $header);
        switch ($response['code']) {
            case 200:
                $response['status'] = 'success';
                return $response;
            case 404:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $response['message']];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }

    // * -----------------------------------------------------------------------------------

    // * Promotion Method ------------------------------------------------------------------
    public function get_promotions($type = null)
    {
        $header = session()->has('token') ? $this->set_header(true) : [];

        $response = $this->http_request('GET', 'promotions' . (!is_null($type) ? '?promotion_type=' . $type : null), $header);

        return $response;
    }

    public function get_promotion_by_code($promotionCode)
    {
        $promotions = $this->http_request('GET', 'promotion/' . $promotionCode);

        return $promotions;
    }

    public function calculate_turnover($promotionCode, $amount)
    {
        $header = $this->set_header(true);
        $json = ['amount' => (float) $amount];
        $promotions = $this->http_request('POST', 'account/promotion/calculate-turnover/' . $promotionCode, $header, $this->encrypt_data($json));
        return $promotions;
    }
    // * -------------------------------------------------------------------------------------

    // * Provider Method ------------------------------------------------------------------
    public function get_providers()
    {
        $providers = $this->http_request('GET', 'providers', [], null);
        return $providers;
    }

    public function get_provider_games($provider)
    {
        $games = $this->http_request('GET', 'provider/game/' . $provider, [], null);

        return $games;
    }

    public function launch_test_mode($provider, $game_id = null)
    {
        $agent = new Agent();

        if (!is_null($game_id)) {
            $json = ['metadata' => ['game_id' => $game_id]];
        }

        $response = $this->http_request('POST', 'launch-test-mode/' . $provider . ($agent->isMobile() ? '?mobile=1' : ''), null, (isset($json) ? $this->encrypt_data($json) : null));

        switch ($response['code']) {
            case 200:
                return ['code' => $response['code'], 'status' => 'success', 'url' => $response['url']];
            case 401:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $response['message']];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }

    public function launch_game($provider, $game_id = null, $ip)
    {
        $agent = new Agent();

        $header = $this->set_header(true, $ip);
        if (!is_null($game_id)) {
            $json = ['metadata' => ['game_id' => $game_id]];
        }

        $response = $this->http_request('POST', 'account/launch/' . $provider . ($agent->isMobile() ? '?mobile=1' : ''), $header, (isset($json) ? $this->encrypt_data($json) : null));

        switch ($response['code']) {
            case 200:
                return ['code' => $response['code'], 'status' => 'success', 'url' => $response['url']];
            case 400:
                switch ($response['own_code']) {
                    case (40001):
                        return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ກະລຸນາຝາກ'];
                    case (40002):
                        return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ບໍ່ສາມາດຫຼີ້ນໄດ້ເພາະວ່າການ ບຳ ລຸງຮັກສາໄດ້ປິດແລ້ວ ຫລືບັນຊີດັ່ງກ່າວໄດ້ຖືກຫ້າມ'];
                    case (40003):
                        return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ບໍ່ສາມາດເຊື່ອມຕໍ່ Api ກັບປະເທດອື່ນ'];
                }
            case 401:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $response['message']];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }
    // * -------------------------------------------------------------------------------------

    // * Common Method ------------------------------------------------------------------
    public function get_siteinfo()
    {
        $site_info = $this->http_request('GET', 'site-info', [], null);
        
        return $site_info;
    }
    // * -------------------------------------------------------------------------------------

    // * Customer Method ------------------------------------------------------------------
    public function get_accountinfo()
    {
        $header = $this->set_header(true);
        $response = $this->http_request('GET', 'account/info', $header, null);

        switch ($response['code']) {
            case 200:
                $response['status'] = 'success';
                return $response;
            case 401:
                $response['status'] = 'failed';
                return $response;
            default:
                return ['code' => $response['code'] || 500, 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }

    public function get_balance()
    {
        $header = $this->set_header(true);
        $response = $this->http_request('GET', 'account/get-balance', $header, null);

        switch ($response['code']) {
            case 200:
                return ['code' => $response['code'], 'status' => 'success', 'current_balance' => $response['current_balance']];
            case 401:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $response['message']];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }

    public function check_phone_number($phone)
    {
        $json = ['phone_number' => $phone];
        $response = $this->http_request('POST', 'check-phone-number', null, $this->encrypt_data($json));

        switch ($response['code']) {
            case 200:
                return ['code' => 200, 'status' => 'No phone in system.'];
            case 400:
                if ($response['field_errors']['children']['phone_number']['errors'][0] == 'เบอร์โทรศัพท์ต้องเป็นตัวเลข และมีความยาว 10 ตัวเท่านั้น') {
                    return ['code' => 400, 'status' => 'Invalid form.'];
                } else {
                    return ['code' => 401, 'status' => 'Phone already in use.'];
                }
            default:
                return ['code' => 500, 'status' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }

    public function change_password($current_password, $new_password)
    {
        $header = $this->set_header(true);
        $json = ['current_password' => $current_password, 'new_password' => $new_password];
        $response = $this->http_request('PUT', 'account/change-password', $header, $this->encrypt_data($json));

        switch ($response['code']) {
            case 200:
                return ['code' => $response['code'], 'status' => 'success'];
            case 400:
                $error = [];
                foreach ($response['field_errors']['children'] as $key => $value) {
                    if (array_key_exists('errors', $value)) {
                        $error[$key] = $value['errors'][0];
                    }
                }
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $error];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }

    public function reset_password($phone_number, $new_password)
    {
        $json = ['phone_number' => $phone_number, 'new_password' => $new_password];
        $response = $this->http_request('PUT', 'reset-password', null, $this->encrypt_data($json));

        switch ($response['code']) {
            case 200:
                return ['code' => $response['code'], 'status' => 'success'];
            case 400:
                $error = [];
                foreach ($response['field_errors']['children'] as $key => $value) {
                    if (array_key_exists('errors', $value)) {
                        $error[$key] = $value['errors'][0];
                    }
                }
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $error];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }
    // * -------------------------------------------------------------------------------------

    // * Auth Method ------------------------------------------------------------------
    public function login($phone, $password, $ip)
    {
        $header = $this->set_header(false, $ip);
        $json = ['phone_number' => $phone, 'password' => $password];
        $response = $this->http_request('POST', 'auth', $header, $this->encrypt_data($json));

        switch ($response['code']) {
            case 200:
                session(['phone_number' => $phone, 'token' => $response['token']]);
                return ['code' => $response['code'], 'status' => 'success'];
            case 400:
            case 403:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ເບີໂທລະສັບຫຼືລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ'];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }

    public function logout()
    {
        $header = $this->set_header(true);
        $response = $this->http_request('POST', 'account/logout', $header, null);
        session()->flush();
        return ['status' => 'success'];
    }

    public function create_user($phone, $password, $bank, $bank_name, $bank_number, $ip, $affiliate = null)
    {
        $header = $this->set_header(false, $ip);
        $json = ['phone_number' => $phone, 'password' => $password, 'bank' => ['bank' => $bank, 'name' => $bank_name, 'number' => $bank_number]];
        if (!\is_null($affiliate)) {
            $json['affiliate'] = $affiliate;
        }

        $response = $this->http_request('POST', 'create-user', $header, $this->encrypt_data($json));

        switch ($response['code']) {
            case 200:
                session(['phone_number' => $phone, 'token' => $response['token']]);
                return ['code' => $response['code'], 'status' => 'success'];
            case 400:
                $error = [];
                foreach ($response['field_errors']['children'] as $key => $value) {
                    if (array_key_exists('children', $value)) {
                        foreach ($value['children'] as $child_key => $child_value) {
                            if (array_key_exists('errors', $child_value)) {
                                $error[$child_key] = $child_value['errors'][0];
                            }
                        }
                    } else if (array_key_exists('errors', $value)) {
                        $error[$key] = $value['errors'][0];
                    }
                }
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $error];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }
    // * ---------------------------------------------------------------------------------------

    // * Otp Method ----------------------------------------------------------------------------
    public function request_otp($phone_number)
    {
        $json = ['phone_number' => $phone_number];

        $response = $this->http_request('POST', 'request-otp', [], $this->encrypt_data($json));

        switch ($response['code']) {
            case 200:
                return ['code' => $response['code'], 'status' => 'success'];
            case 400:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $response['field_errors']['children']['otp']['errors'][0]];
            case 501:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $response['message']];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }

    public function verify_otp($phone_number, $otp)
    {
        $json = ['phone_number' => $phone_number, 'otp' => $otp];

        $response = $this->http_request('POST', 'verify-otp', [], $this->encrypt_data($json));

        switch ($response['code']) {
            case 200:
                return ['code' => $response['code'], 'status' => 'success'];
            case 400:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $response['field_errors']['children']['otp']['errors'][0]];
            case 501:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => $response['message']];
            default:
                return ['code' => $response['code'], 'status' => 'failed', 'error' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
        }
    }
    // * ---------------------------------------------------------------------------------------
    // * =======================================================================================
}
