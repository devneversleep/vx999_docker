<?php

namespace App\Libraries;

use Illuminate\Support\Facades\Http;

class OTP
{
    public static function random_otp($digits)
    {
        $otp = '';
        for ($i = 0; $i < $digits; $i++) {
            $otp = $otp . rand(0,9);
        }

        return $otp;
    }
}