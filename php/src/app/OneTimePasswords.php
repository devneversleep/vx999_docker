<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Log;
use Carbon\Carbon;

class OneTimePasswords extends Model
{
    public static function insert($phone_number, $random_otp)
    {
        $otp = new OneTimePasswords;
        $otp->phone_number = $phone_number;
        $otp->number = $random_otp; 
        $otp->save();
    }

    public static function deleteOneTimePasswordsTimeOut()
    {
        $time_delete = date("Y-m-d H:i:s", strtotime('-5 minutes'));
        // $left = OneTimePasswords::where('created_at', '<', $date )->get();
        DB::table('one_time_passwords')
            ->where('created_at', '<', $time_delete)
            ->delete();
        // $a = OneTimePasswords::where('created_at', '<', $time_delete)->get();
        // Log::notice(json_encode($a));
    }
}
