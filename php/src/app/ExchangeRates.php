<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BackLogs; 

class ExchangeRates extends Model
{
    public static function setExchangeRate($user_id, $id, $new_rate)
    {
        $exchange_rate = ExchangeRates::find($id);

        if (isset($exchange_rate)) {
            $back_log = new BackLogs;
            $back_log->user_id = $user_id;
            $back_log->table_name = 'exchange_rates';
            $back_log->id_in_table = $id;
            $back_log->field_name = 'thai_1_bath_to_laos';
            $back_log->before_value = $exchange_rate->thai_1_bath_to_laos;
            $back_log->after_value = $new_rate;
            $back_log->save();
        }

        $exchange_rate->thai_1_bath_to_laos = $new_rate;
        $exchange_rate->save();


    }
}
