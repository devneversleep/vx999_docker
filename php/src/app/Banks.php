<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Banks extends Model
{
    public static function insert($code, $name, $number, $user_id)
    {
        $bank = new Banks;
        $bank->code = $code;
        $bank->name = $name;
        $bank->number = $number;
        $bank->user_id = $user_id;
        $bank->status = 0;
        $bank->save();
    }

    public static function changeActiveBanks($user_id, $bank_id)
    {
        $banks_actived_before = Banks::where('status', '=', 1)->get();
        // $before_value = array_column(json_decode(json_encode($bank_active_before)), 'id');
        
        foreach ($banks_actived_before as $bank_actived_before) {
            $bank_actived = Banks::find($bank_actived_before->id);
        
            $back_log = new BackLogs;
            $back_log->user_id = $user_id;
            $back_log->table_name = 'banks';
            $back_log->id_in_table = $bank_actived->id;
            $back_log->field_name = 'status';
            $back_log->before_value = '1';
            $back_log->after_value = '0';
            $back_log->save();
            
            $bank_actived->status = 0;
            $bank_actived->save();
        }

        $bank_active = Banks::find($bank_id);

        if (isset($bank_active)) {
            $back_log = new BackLogs;
            $back_log->user_id = $user_id;
            $back_log->table_name = 'banks';
            $back_log->id_in_table = $bank_active->id;
            $back_log->field_name = 'status';
            $back_log->before_value = '0';
            $back_log->after_value = '1';
            $back_log->save();
        }

        $bank_active->status = 1;
        $bank_active->save();
    }
}
