<?php

namespace App\Http\Controllers;

use App\BackLogs;
use Illuminate\Http\Request;

class BackLogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BackLogs  $backLogs
     * @return \Illuminate\Http\Response
     */
    public function show(BackLogs $backLogs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BackLogs  $backLogs
     * @return \Illuminate\Http\Response
     */
    public function edit(BackLogs $backLogs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BackLogs  $backLogs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BackLogs $backLogs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BackLogs  $backLogs
     * @return \Illuminate\Http\Response
     */
    public function destroy(BackLogs $backLogs)
    {
        //
    }
}
