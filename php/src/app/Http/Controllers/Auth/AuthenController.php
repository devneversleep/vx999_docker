<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Libraries\IGT;
use App\Libraries\SMS;
use App\Libraries\OTP;
use App\OneTimePasswords;
use Illuminate\Http\Request;
use Log;

class AuthenController extends Controller
{
    private $igt;
    private $sms;

    public function __construct()
    {
        $this->igt = IGT::get_instance();
        $this->sms = SMS::get_instance();
    }

    public function login(Request $request)
    {
        $input = $request->all();

        // Log::notice($input);

        if ($input['phone_country'] == strlen($input['phone_number'])) {
            $phone_number = $input['phone_number'];
        } else if ($input['phone_country'] == strlen($input['phone_number']) + 1) {
            $phone_number = 0 . $input['phone_number'];
        } else {
            $phone_number = '9876543210123456789';
        }

        $response = $this->igt->login($phone_number, $input['password'], $request->ip());

        // if ($response['status'] != 'success') {
        //     if ($input['phone_country'] == strlen($input['phone_number'])) {
        //         $phone_number = $input['phone_number'];
        //     } else if ($input['phone_country'] == strlen($input['phone_number']) + 1) {
        //         $phone_number = '0' . $input['phone_number'];
        //     } else {
        //         $phone_number = '9876543210123456789';
        //     }
    
        //     $response = $this->igt->login($phone_number, $input['password'], $request->ip());
        // }

        // Log::notice($response);

        if ($response['status'] == 'success') {
            if (isset($input['_p'])) {
                return redirect('/promotion?_p=' . $input['_p']);
            } else {
                return redirect('/');
            }
        } else {
            return redirect('/')->withErrors([$response['error']]);
        }
    }

    public function logout(Request $request)
    {
        $response = $this->igt->logout();

        return redirect('/');
    }

    public function create_user(Request $request)
    {
        $input = $request->all();

        $response = $this->igt->create_user($input['phone_number'], $input['password'], $input['bank'], $input['bank_name'], $input['bank_number'], $request->ip(), ($input['affiliate'] ? $input['affiliate'] : null));
        return response($response);
    }

    // * ajax method
    public function register_phone(Request $request)
    {

        // Log::notice($request);
        // $register = session()->get('register', null);
        // $register['phone_number'] = $request->all()['register_phone_number'];
        // session()->put('register', $register);
        
        $input = $request->all();
        // $phone_number = $request->all()['register_phone_number'];
        // Log::notice($input);

        if ($input['phone_country'] == strlen($input['register_phone_number'])) {
            $phone_number = $input['register_phone_number'];
        } else if ($input['phone_country'] == strlen($input['register_phone_number']) + 1) {
            if ($input['register_phone_number'][0] == 0) {
                return response('ເບີໂທລະສັບບໍ່ຖືກຕ້ອງ', 500);
            }
            $phone_number = 0 . $input['register_phone_number'];
        } else {
            return response('ເບີໂທລະສັບບໍ່ຖືກຕ້ອງ', 500);
        }

        if ($input['phone_country'] == 11) {
            $country_code = '856';
        } else if ($input['phone_country'] == 10) {
            $country_code = '66';
        }

        // if ($phone_number == '0846160587') {
        //     $this->sms->send_otp($phone_number, '9765', $country_code);
        //     return response('test', 500);
        // }

        $check_phone_number = $this->igt->check_phone_number($phone_number);
        // return response($check_phone_number, 500);
        if ($check_phone_number['code'] == 200) {
            
            $register = session()->get('register', null);
            
            if (!is_null($register)) {
                $register['phone_number'] = $phone_number;
                session()->put('register', $register);
            } else {
                session()->put('register', ['phone_number' => $phone_number]);
            }

            // comment if want send otp
            $returnHTML = view('modals.register3')->render();
            return response($returnHTML);

            $has_otp =  OneTimePasswords::where('phone_number', '=', $phone_number)->get();        
            if ($has_otp->count()) {
                $request_otp = ['type' => 'OLD'];
            } else {
                $random_otp = OTP::random_otp(4);     
                $request_otp = $this->sms->send_otp($phone_number, $random_otp, $country_code);
                // $request_otp = [['type' => 'SENT']];
            }
            
            // $request_otp['type'] == 'SENT';
            if (isset($request_otp[0])) {
                if ($request_otp[0]['type'] == 'SENT') {
                    OneTimePasswords::insert($phone_number, $random_otp);
                    $returnHTML = view('modals.register2')->render();
                    return response($returnHTML);
                } else {
                    return response('ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ', 500);
                }             
            } if ($request_otp['type'] == 'OLD') {
                $returnHTML = view('modals.register2')->render();
                return response($returnHTML);
            } else {
                return response('ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ', 500);
            }

            // if (!session('site_info')['otp_disabled']) {
                
                
            // } else {
            //     $returnHTML = view('modals.register3')->render();
            //     return response($returnHTML);
            // }
        } else {
            // Log::notice($check_phone_number);
            if ($check_phone_number['code'] == 400) {
                
                return response('ເບີໂທລະສັບບໍ່ຖືກຕ້ອງ', 400);
            } else if ($check_phone_number['code'] == 401) {
                return response('ມີເບີໂທລະສັບນີ້ຢູ່ໃນລະບົບແລ້ວ', 401);
            } else {
                return response('ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ', 500);
            }
        }

        // if ($result['http_status'] != 201) {
        //     print "Error sending: " . ($result['error'] ? $result['error'] : "HTTP status ".$result['http_status']."; Response was " .$result['server_response']);
        // } else {
        //     print "Response " . $result['server_response'];
        //     // Use json_decode($result['server_response']) to work with the response further
        // }
    }

    public function cancel_register()
    {
        session()->forget('register');

        $returnHTML = view('modals.register1');
        return response($returnHTML);
    }

    public function register_otp(Request $request)
    {
        $register = session()->get('register', null);

        // $returnHTML = view('modals.register3')->render();
        // return response($returnHTML);
        
        $otp = $request->all()['otp'];

        if (!is_null($register)) {
    
            $otp_user = OneTimePasswords::where('phone_number', '=', $register['phone_number'])->first();

            if (isset($otp_user)) {
                if ($otp_user->number == $otp) {
                    $returnHTML = view('modals.register3')->render();
                    return response($returnHTML);
                } else {
                    return response('OTP ແມ່ນບໍ່ຖືກຕ້ອງຫຼື ໝົດ ອາຍຸແລ້ວ. ກະລຸນາພະຍາຍາມສະ ໝັກ ອີກຄັ້ງ', '500');
                }
            } else {
                return response('OTP ແມ່ນບໍ່ຖືກຕ້ອງຫຼື ໝົດ ອາຍຸແລ້ວ. ກະລຸນາພະຍາຍາມສະ ໝັກ ອີກຄັ້ງ', '500');
            }       
        } else {
            return response('ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ', 500);
        }
    }

    public function register_password(Request $request)
    {
        $password = $request->all()['register_password'];
        $register = session()->get('register', null);

        if (!is_null($register)) {
            $register['password'] = $password;
            session()->put('register', $register);         

            $returnHTML = view('modals.register4')->render();
            return response($returnHTML);
        } else {
            return response('ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ', 500);
        }
    }

    public function register_user(Request $request)
    {
        $bank = $request->all()['bank'];

        // $returnHTML = view('modals.register5')->render();

        $register = session()->get('register', null);
        if (!is_null($register)) {
            // return response($register['affliate'], 500);
            $response = $this->igt->create_user($register['phone_number'], $register['password'], 'bank-kk', 'n' . $bank['name'], '888' . $bank['number'], $request->ip(), (isset($register['affliate']) ? $register['affliate'] : 'laocasino'));
            Log::notice($response);
            if ($response['code'] == 200) {
                $returnHTML = view('modals.register5')->render();
                return response($returnHTML);
            } else if ($response['code'] == 400) {
                // Log::notice($response);
                if (isset($response['error']['name'])) {              
                    if ($response['error']['name'] == 'This account was duplicated, Please contact our admin') {
                        return response('ເຮັດຊ້ ຳ ບັນຊີນີ້ ກະລຸນາຕິດຕໍ່ຜູ້ເບິ່ງແຍງລະບົບ', 500);
                    }
                    return response('ກະລຸນາໃສ່ຊື່ແລະນາມສະກຸນ', 500);
                }
                else if (isset($response['error']['number'])) {
                    if ($response['error']['number'] == 'This account number has already been used.') {
                        return response('ເລກບັນຊີນີ້ຖືກໃຊ້ແລ້ວ', 500);
                    }
                }
                return response('ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ', 500);
            } else {
                return response('ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ', 500);
            }
        } else {
            // Log::notice('message');
            return response('ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ', 500);
        }
    }
}
