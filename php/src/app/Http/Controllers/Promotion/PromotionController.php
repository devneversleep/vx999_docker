<?php

namespace App\Http\Controllers\Promotion;

use App\Http\Controllers\Controller;
use App\Libraries\IGT;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    private $igt;

    public function __construct()
    {
        $this->igt = IGT::get_instance();
    }

    public function get_promotions(Request $request)
    {
        $type = $request->query('type');

        $response = $this->igt->get_promotions(isset($type) ? $type : null);

        return response($response);
    }

    public function get_promotion_by_code($promotionCode)
    {
        $response = $this->igt->get_promotion_by_code($promotionCode);

        return response($response);
    }

    public function calculate_turnover(Request $request, $promotionCode)
    {
        $input = $request->all();

        $response = $this->igt->calculate_turnover($promotionCode, $input['amount']);

        return response($response);
    }
}
