<?php

namespace App\Http\Controllers\Bill;

use App\Http\Controllers\Controller;
use App\Libraries\IGT;
use App\ExchangeRates;
use App\Banks;
use Illuminate\Http\Request;
use Log;

class BillController extends Controller
{
    private $igt;

    public function __construct()
    {
        $this->igt = IGT::get_instance();
    }

    public function deposit(Request $request)
    {
        $ip = $request->ip();
        $input = $request->all();

        $time = rand(0, 100) * 10000;
        usleep($time);
        $response = $this->igt->deposit($input['amount'], $ip, isset($input['promotion_code']) ? $input['promotion_code'] : null);
        $bank = Banks::where('status', '=', 1)->first();
        $returnHTML = view('modals/deposit2', ['bank' => $bank])->render();
        return response()->json(array(
            'html' => $returnHTML,
            'bill' => $response));
    }

    public function withdraw(Request $request)
    {
        $ip = $request->ip();
        $input = $request->all();

        $response = $this->igt->withdraw($input['amount']);
        // Log::notice($response);
        return response($response);
    }

    public function confirm_deposit()
    {
        $response = $this->igt->confirm_deposit();
        $bank = Banks::where('status', '=', 1)->first();
        $returnHTML = view('modals/deposit3', ['bank' => $bank])->render();
        return response()->json(array(
            'html' => $returnHTML,
            'bill' => $response));
    }

    public function cancel_deposit()
    {
        $response = $this->igt->cancel_deposit();
        $exchange_rate = ExchangeRates::first();
        $returnHTML = view('modals/deposit', ['exchange_rate' => $exchange_rate])->render();
        return response()->json(array(
            'html' => $returnHTML,
            'exchange_rate' => $exchange_rate,
            'bill' => $response));
    }

    public function deposit_again()
    {
        return response()->json(array('html' => view('modals/deposit')->render()));
    }

    public function check_pending_deposit()
    {
        $response = $this->igt->check_pending_deposit();
        $returnHTML = null;
        if ($response["code"] == 200) {
            if ($response["confirmed"]) {
                $bank = Banks::where('status', '=', 1)->first();
                $returnHTML = view('modals/deposit3', ['bank' => $bank])->render();
            } else {
                $bank = Banks::where('status', '=', 1)->first();
                $returnHTML = view('modals/deposit2', ['bank' => $bank])->render();
            }
        }

        return response()->json(array(
            'html' => $returnHTML,
            'bill' => $response));
    }

    public function check_pending_withdraw()
    {
        $response = $this->igt->check_pending_withdraw();

        return response()->json(array(
            // 'html' => $returnHTML,
            'bill' => $response));
    }
}
