<?php

namespace App\Http\Controllers;

use App\ExchangeRates;
use App\Banks;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Log;

class ExchangeRatesController extends Controller
{
    public function index()
    {
        if (!auth()->check()) {          
            return redirect('/login-admin');
        }

        $exchange_rate = ExchangeRates::first();
        $banks = Banks::all();
        return view('exchange_rate', ['exchange_rate' => $exchange_rate, 'banks' => $banks]);
    }

    public function setExchangeRate(Request $request)
    {
        try {
            $request->validate([
                'id' => ['required'],
                'new_rate' => ['required', 'numeric'],
            ]);
         
            ExchangeRates::setExchangeRate(auth()->user()->id, $request->id, $request->new_rate);
            return response([
                'success' => true,
            ], 200);
        } catch ( QueryException $e) {
            return $e->errorInfo;
        }
    }
}
