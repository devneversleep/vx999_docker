<?php

namespace App\Http\Controllers\Provider;

use App\Http\Controllers\Controller;
use App\Libraries\IGT;
use Illuminate\Http\Request;
use Log;
use Session;

class ProviderController extends Controller
{
    private $igt;

    public function __construct()
    {
        $this->igt = IGT::get_instance();
    }

    public function usort_object($icon_game)
    {
        $sorted = usort($icon_game, function ($a, $b) {
            return ($a['support_test'] < $b['support_test']);
        });
        return $sorted;
    }

    public function get_providers()
    {
        $response = $this->igt->get_providers();
        if (session()->has('token')) {
            $accountinfo = $this->igt->get_accountinfo();
            if ($accountinfo['code'] == 401) {
                session()->flush();
                return redirect('/')->withErrors(['ກະລຸນາເຂົ້າສູ່ລະບົບອີກຄັ້ງ']);
            } else {
                return view('lobby', ['games' => $response, 'accountinfo' => $accountinfo]);
            }
        } else {
            return view('lobby', ['games' => $response]);
        }
    }

    public function get_provider_games(Request $request, $provider)
    {
        $response = $this->igt->get_provider_games($provider);
        $balance = $this->igt->get_balance();
        if (empty($response["slot_game"])) {
            if (empty($response["game_id"]) && empty($response["code"])) {
                $response_game = $response["slot_game"];
            } else if (!empty($response["code"])) {
                $response_url = $this->igt->launch_game($provider, null, $request->ip());
                if (!empty($response_url["url"])) {
                    return \redirect($response_url["url"]);
                } else {
                    return \redirect('/lobby')->withErrors([$response_url['error']]);
                }
            } else {
                $response_game = $response;
            }
        } else {
            $response_game = $response;
        }
        return view("game_lobby/" . $provider . "_lobby", ['games' => $response_game, 'balance' => (isset($balance["current_balance"]) ? $balance["current_balance"] : 0)]);
    }

    public function get_provider_games_id($provider, $game_id)
    {
        $response = $this->igt->get_provider_games($provider);

        return view("game_lobby/game_lobby_list/" . $provider . "_lobby", ['games' => $response["video_poker"]["game_id"][$game_id], 'balance' => (isset($balance) ? $balance["current_balance"] : 0)]);
    }

    public function launch_test_mode($provider, $game_id = null)
    {
        $response = $this->igt->launch_test_mode($provider, $game_id);
        return \redirect($response["url"]);
    }

    public function launch_game(Request $request, $provider, $game_id = null)
    {
        if (Session::has('token')) {
            $ip = $request->ip();
            $response = $this->igt->launch_game($provider, $game_id, $ip);
            if (!empty($response["url"])) {
                return \redirect($response["url"]);
            } else {
                return \redirect('/lobby')->withErrors([$response['error']]);
            }
        } else {
            $response = $this->igt->launch_test_mode($provider, $game_id);
            if (!empty($response["url"])) {
                return \redirect($response["url"]);
            } else {
                return \redirect('/lobby')->withErrors([$response['error']]);
            }
        }

    }
}
