<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Http\Controllers\WordPressController;
use App\Libraries\IGT;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    private $igt;
    protected $ContentController;
    public function __construct(WordPressController $wp)
    {
        $this->igt = IGT::get_instance();
        $this->ContentController = $wp;
    }

    public function index(Request $request)
    {
        $contents = $this->ContentController->getContents(null, $request->query('page', 1));

        if (session()->has('token')) {
            $accountinfo = $this->igt->get_accountinfo();
            if ($accountinfo['code'] == 401) {
                session()->flush();
                return redirect('/')->withErrors(['ກະລຸນາເຂົ້າສູ່ລະບົບອີກຄັ້ງ']);
            } else {
                return view('contents', ['contents' => $contents['contents'], 'total' => $contents['total'], 'accountinfo' => $accountinfo]);
            }
        } else {
            return view('contents', ['contents' => $contents['contents'], 'total' => $contents['total']]);
        }
    }

    public function detail(Request $request, $slug)
    {
        $content = $this->ContentController->getContents($slug);

        if (session()->has('token')) {
            $accountinfo = $this->igt->get_accountinfo();
            if ($accountinfo['code'] == 401) {
                session()->flush();
                return redirect('/')->withErrors(['ກະລຸນາເຂົ້າສູ່ລະບົບອີກຄັ້ງ']);
            } else {
                return view('content_detail', ['content' => $content, 'seo_head' => $content['seo_head'], 'accountinfo' => $accountinfo]);
            }
        } else {
            return view('content_detail', ['content' => $content, 'seo_head' => $content['seo_head']]);
        }
    }
}
