<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Log;

class WordPressController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function getPosts()
    {
        $response = Http::get('https://test.sa365.bet/content/index.php?rest_route=/wp/v2/posts');
        $images_arr = [];
        foreach (json_decode($response) as $element) {
            $images = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT06oG1k2dsYk2hzIqniw-m4C8r9--C2Hwbyg&usqp=CAU";
            if (isset($element->_links->{'wp:featuredmedia'})) {
                $images = Http::get($element->_links->{'wp:featuredmedia'}[0]->href)->json()['guid']['rendered'];
                // $category = Http::get($element->_links->{'wp:term'}[0]->href)->json();
                // $categories = [];
            }
            array_push($images_arr, $images == null ? "" : $images);
        }
        return [json_decode($response), $images_arr];
    }

    public function getContents($slug = null, $page = null)
    {
        if (is_null($slug)) {
            $response = Http::get('http://localhost/content/index.php?rest_route=/wp/v2/posts&per_page=9&page=' . (!is_null($page) ? $page : 1))->json();
            $headers = Http::get('http://localhost/content/index.php?rest_route=/wp/v2/posts')->headers();

            Log::info($response);

            $contents = [];
            foreach ($response as $element) {
                $tmp = ['id' => $element['id'], 'title' => $element['title']['rendered'], 'content' => $element['content']['rendered'], 'date' => $element['date'], 'slug' => $element['slug']];
                
                $image = Http::get($element['_links']['wp:featuredmedia'][0]['href'])->json();
                $tmp['image'] = $image['guid']['rendered'];
                $tmp['image_alt'] = $image['alt_text'];

                $tmp['categories'] = [];
                $categories = Http::get($element['_links']['wp:term'][0]['href'])->json();
                foreach ($categories as $cate) {
                    array_push($tmp['categories'], $cate['name']);
                }
                array_push($contents, $tmp);
            }

            return ['contents' => $contents, 'total' => (isset($headers['X-WP-TotalPages']) ? $headers['X-WP-TotalPages'] : $headers['x-wp-totalpages'])[0]];
        } else {
            $response = Http::get('http://localhost/content/index.php?rest_route=/wp/v2/posts&slug=' . $slug)->json()[0];

            Log::info($response);

            $contents = ['id' => $response['id'], 'title' => $response['title']['rendered'], 'content' => $response['content']['rendered'], 'date' => $response['date'], 'slug' => $response['slug'], 'seo_head' => $response['yoast_head']];;
            $image = Http::get($response['_links']['wp:featuredmedia'][0]['href'])->json();
            $contents['image'] = $image['guid']['rendered'];
            $contents['image_alt'] = $image['alt_text'];

            $contents['categories'] = [];
            $categories = Http::get($response['_links']['wp:term'][0]['href'])->json();
            foreach ($categories as $cate) {
                array_push($contents['categories'], $cate['name']);
            }

            return $contents;
        }
    }
    
    public function getHomeContentsView()
    {
        $response = Http::get('http://localhost/content/index.php?rest_route=/wp/v2/posts&per_page=6')->json();

        $contents = [];
        foreach ($response as $element) {
            $tmp = ['id' => $element['id'], 'title' => $element['title']['rendered'], 'content' => $element['content']['rendered'], 'date' => $element['date'], 'slug' => $element['slug']];

            $image = Http::get($element['_links']['wp:featuredmedia'][0]['href'])->json();
            $tmp['image'] = $image['guid']['rendered'];
            $tmp['image_alt'] = $image['alt_text'];

            $tmp['categories'] = [];
            $categories = Http::get($element['_links']['wp:term'][0]['href'])->json();
            foreach ($categories as $cate) {
                array_push($tmp['categories'], $cate['name']);
            }
            array_push($contents, $tmp);
        }

        return view('home_contents_view', ['content' => $contents])->render();
    }

    public function generateSitemap()
    {
        $pages = Http::get('http://localhost/content/index.php?rest_route=/wp/v2/posts')->headers();
        $pages = intval((isset($pages['X-WP-TotalPages']) ? $pages['X-WP-TotalPages'] : $pages['x-wp-totalpages'])[0]);

        $blogs = [];

        for ($i = 1; $i <= $pages; $i++) {
            $blog = Http::get('http://localhost/content/index.php?rest_route=/wp/v2/posts&page=' . $i)->json();
            $blogs = array_merge($blogs, $blog);
        }

        return response()->view('sitemap', ['blog' => $blogs])->header('Content-Type', 'text/xml');
    }
}
