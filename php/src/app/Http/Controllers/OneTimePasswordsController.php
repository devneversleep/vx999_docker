<?php

namespace App\Http\Controllers;

use App\OneTimePasswords;
use Illuminate\Http\Request;

class OneTimePasswordsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OneTimePasswords  $oneTimePasswords
     * @return \Illuminate\Http\Response
     */
    public function show(OneTimePasswords $oneTimePasswords)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OneTimePasswords  $oneTimePasswords
     * @return \Illuminate\Http\Response
     */
    public function edit(OneTimePasswords $oneTimePasswords)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OneTimePasswords  $oneTimePasswords
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OneTimePasswords $oneTimePasswords)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OneTimePasswords  $oneTimePasswords
     * @return \Illuminate\Http\Response
     */
    public function destroy(OneTimePasswords $oneTimePasswords)
    {
        //
    }
}
