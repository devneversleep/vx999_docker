<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Libraries\IGT;
use App\Libraries\SMS;
use App\Libraries\OTP;
use App\OneTimePasswords;
use Illuminate\Http\Request;
use Log;

class CustomerController extends Controller
{
    private $igt;

    public function __construct()
    {
        $this->igt = IGT::get_instance();
        $this->sms = SMS::get_instance();
    }

    public function get_accountinfo()
    {
        $response = $this->igt->get_accountinfo();

        return response($response);
    }

    public function get_balance()
    {
        $response = $this->igt->get_balance();

        return response($response, $response['code']);
    }

    public function check_phone_number(Request $request)
    {
        $input = $request->all();

        $response = $this->igt->check_phone_number($input['phone_number']);

        return response($response);
    }

    public function change_password(Request $request)
    {
        $input = $request->all();

        $response = $this->igt->change_password($input['old_password'], $input['new_password']);

        return response($response);
    }

    public function reset_password(Request $request)
    {
        $input = $request->all();

        $response = $this->igt->reset_password(session()->get('phone_number'), $input['register_password']);

        return response($response);
    }

    // * ajax method
    public function reset_password_request_otp(Request $request)
    {
        $input = $request->all();
        // Log::notice($input);

        // $phone_number = $input['request_phone_number'];

        if ($input['phone_country'] == strlen($input['request_phone_number'])) {
            $phone_number = $input['request_phone_number'];
        } else if ($input['phone_country'] == strlen($input['request_phone_number']) + 1) {
            if ($input['request_phone_number'][0] == 0) {
                return ['status' => 'wrong_number', 'response' => 'ເບີໂທລະສັບບໍ່ຖືກຕ້ອງ'];
            }
            $phone_number = 0 . $input['request_phone_number'];
        } else {
            return ['status' => 'wrong_number', 'response' => 'ເບີໂທລະສັບບໍ່ຖືກຕ້ອງ'];
        }

        if ($input['phone_country'] == 11) {
            $country_code = '856';
        } else if ($input['phone_country'] == 10) {
            $country_code = '66';
        }

        $check = $this->igt->check_phone_number($input['request_phone_number']);
        // Log::notice($check);
        // return ['status' => 'failed', 'response' => 'ເບີໂທລະສັບບໍ່ຖືກຕ້ອງ'];

        if ($check['code'] == 401) {
            
            $has_otp =  OneTimePasswords::where('phone_number', '=', $phone_number)->get();        
            if ($has_otp->count()) {
                $request_otp = ['type' => 'OLD'];
            } else {
                $random_otp = OTP::random_otp(4);     
                $request_otp = $this->sms->send_otp($phone_number, $random_otp, $country_code);
            }
            
            if (isset($request_otp[0])) {
                if ($request_otp[0]['type'] == 'SENT') {
                    OneTimePasswords::insert($phone_number, $random_otp);
                    session()->put(['phone_number' => $phone_number]);
                    return ['status' => 'member', 'response' => $check];
                } else {
                    return ['status' => 'failed', 'response' => 'ການສົ່ງ otp ລົ້ມເຫລວ'];
                }             
            } if ($request_otp['type'] == 'OLD') {
                session()->put(['phone_number' => $phone_number]);
                return ['status' => 'member', 'response' => $check];
            } else {
                return ['status' => 'failed', 'response' => 'ມີບາງຢ່າງຜິດປົກກະຕິ ກະລຸນາລອງ ໃໝ່ ໃນພາຍຫຼັງ'];
            }
            
        } else if ($check['code'] == 400) {
            return ['status' => 'wrong_number', 'response' => $check];
        } else if ($check['code'] == 200) {
            return ['status' => 'non_member', 'response' => $check];
        } else {
            return ['status' => 'failed', 'response' => $check];
        }
    }
}
