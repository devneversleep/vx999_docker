<?php

namespace App\Http\Controllers;

use App\Libraries\IGT;
use Illuminate\Http\Request;
use App\ExchangeRates;
use App\Banks;
use App\OneTimePasswords;
use Illuminate\Support\Facades\Auth;
use Log;

class HomeController extends Controller
{
    private $igt;
    protected $ContentController;
    public function __construct(WordPressController $wp)
    {
        $this->igt = IGT::get_instance();
        $this->ContentController = $wp;
    }

    public function index(Request $request, $affliate = null, $promotion_code = null)
    {
        $request->session()->forget('register');

        if (!is_null($affliate)) {
            if (session()->has('token')) {
                return redirect('/');
            } else {
                session()->put('register', ['affliate' => $affliate]);
            }
        }

        OneTimePasswords::deleteOneTimePasswordsTimeOut();

        $site_info = $this->igt->get_siteinfo();
        session()->put('site_info', $site_info);
        Log::notice($site_info);

        if (session()->has('token')) {
            $accountinfo = $this->igt->get_accountinfo();
            if ($accountinfo['code'] == 401) {
                session()->flush();
                return redirect('/')->withErrors(['ກະລຸນາເຂົ້າສູ່ລະບົບອີກຄັ້ງ']);
            } else {
                $exchange_rate = ExchangeRates::first();
                return view('index', ['accountinfo' => $accountinfo, 'affliate' => $affliate, 'new' => in_array('new', array_keys($request->query())), 'promotion_code' => $promotion_code, 'exchange_rate' => $exchange_rate]);
                // return view('index', ['accountinfo' => $accountinfo, 'affliate' => $affliate, 'new' => in_array('new', array_keys($request->query())), 'promotion_code' => $promotion_code]);
            }
        } else {
            return view('index');
            // return view('index');
        }
    }

    public function loginAdmin()
    {
        // Auth::logout();  
        if($user = Auth::user()) {
            return redirect('/exchange-rate');
        }
        return view('/login_admin');
    }

    public function Select_bank()
    {
        if (!auth()->check()) {          
            return redirect('/login-admin');
        }
        $banks = Banks::orderBy('id', 'DESC')->get();
        return view('/select_bank', ['banks' => $banks]);
        // return view('/select_bank');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login-admin');
    }
}
