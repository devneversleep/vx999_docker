<?php

namespace App\Http\Controllers;

use App\Banks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class BanksController extends Controller
{

    public function test()
    {
        $banks = Banks::all();
        return view('test_bank', ['banks' => $banks]);
    }

    public function addBank(Request $request)
    {
        try {
            $request->validate([
                'code' => ['required'],
                'number' => ['required', 'size:18', 'regex:/^[0-9]+$/'],
                'name' => ['required'],        
            ]);
        
            Banks::insert($request->code, $request->name, $request->number, auth()->user()->id);
            return response([
                'success' => true,
            ], 200);
        } catch ( QueryException $e) {
            return $e->errorInfo;
        }
    }

    public function changeActiveBanks(Request $request)
    {
        try {     
            Banks::changeActiveBanks(auth()->user()->id, $request->bank_id);
            return response([
                'success' => true,
            ], 200);
        } catch ( QueryException $e) {
            return $e->errorInfo;
        }
    }
}
