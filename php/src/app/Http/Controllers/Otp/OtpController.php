<?php

namespace App\Http\Controllers\Otp;

use App\Http\Controllers\Controller;
use App\Libraries\IGT;
use App\OneTimePasswords;
use Illuminate\Http\Request;
use Log;

class OtpController extends Controller
{
    private $igt;

    public function __construct()
    {
        $this->igt = IGT::get_instance();
    }

    public function request_otp(Request $request)
    {
        $input = $request->all();
        // $check = $this->igt->check_phone_number($request, $input['request_phone_number']);
        // if ($check['code'] == 400 && $check['field_errors']['children']['phone_number']['errors'][0] == 'เบอร์โทรศัพท์นี้ถูกใช้งานแล้ว') {
        //     $request->session()->put('resetpassword_phone_number', $input['request_phone_number']);
        //     $this->igt->request_otp($request, $request->session()->get('resetpassword_phone_number'));
        //     return ['status' => 'member', 'response' => $check];
        // } else if ($check['code'] == 400 && $check['field_errors']['children']['phone_number']['errors'][0] == 'เบอร์โทรศัพท์ต้องเป็นตัวเลข และมีความยาว 10 ตัวเท่านั้น') {
        //     return ['status' => 'wrong_number', 'response' => $check];
        // } else {
        //     return ['status' => 'non_member', 'response' => $check];
        // }
        $response = $this->igt->request_otp($input['phone_number']);
        return response($response);
    }

    public function verify_otp(Request $request)
    {
        $input = $request->all();
        $phone_number = session()->get('phone_number');
        // Log::alert($phone_number);
        // $response = $this->igt->verify_otp($phone_number, $input['otp']);
        $otp_user = OneTimePasswords::where('phone_number', '=', $phone_number)->first();
        if ($otp_user->number == $input['otp']) {
            return response([
                'success' => 'success',
            ], 200);
        } else {
            return response('OTP ແມ່ນບໍ່ຖືກຕ້ອງຫຼື ໝົດ ອາຍຸແລ້ວ. ກະລຸນາພະຍາຍາມສະ ໝັກ ອີກຄັ້ງ', '500');
        }

        // if ($response['code'] == 200) {
        //     return response($response);
        // } else {
        //     return response($response['error'], $response['code']);
        // }
    }
}
