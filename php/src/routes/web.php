<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Auth::routes();

Route::group(['prefix' => 'api'], function () {
    Route::post('/deposit', 'Bill\BillController@deposit');
    Route::post('/withdraw', 'Bill\BillController@withdraw');
    Route::put('/confirm_deposit', 'Bill\BillController@confirm_deposit');
    Route::put('/cancel_deposit', 'Bill\BillController@cancel_deposit');
    Route::get('/check_pending_deposit', 'Bill\BillController@check_pending_deposit');
    Route::get('/check_pending_withdraw', 'Bill\BillController@check_pending_withdraw');
    Route::get('/deposit_again', 'Bill\BillController@deposit_again');
    Route::get('/withdraw_again', 'Bill\BillController@withdraw_again');

    Route::get('/get_promotions', 'Promotion\PromotionController@get_promotions');
    Route::get('/get_promotion_by_code/{promotionCode}', 'Promotion\PromotionController@get_promotion_by_code');
    Route::post('/calculate_turnover/{promotionCode}', 'Promotion\PromotionController@calculate_turnover');

    Route::get('/get_providers', 'Provider\ProviderController@get_providers');
    Route::get('/get_provider_games/{provider}', 'Provider\ProviderController@get_provider_games');
    Route::get('/get_provider_games/{provider}/{game_id}', 'Provider\ProviderController@get_provider_games_id');
    Route::get('/launch_test_mode/{provider}', 'Provider\ProviderController@launch_test_mode');
    Route::get('/launch_test_mode/{provider}/{game_id}', 'Provider\ProviderController@launch_test_mode');
    Route::get('/launch_game/{provider}', 'Provider\ProviderController@launch_game');
    Route::get('/launch_game/{provider}/{game_id}', 'Provider\ProviderController@launch_game');

    Route::get('/get_accountinfo', 'Customer\CustomerController@get_accountinfo');
    Route::get('/get_balance', 'Customer\CustomerController@get_balance');
    Route::post('/check_phone_number', 'Customer\CustomerController@check_phone_number');
    Route::put('/change_password', 'Customer\CustomerController@change_password');
    Route::put('/reset_password', 'Customer\CustomerController@reset_password'); // working
    Route::post('/reset_password_request_otp', 'Customer\CustomerController@reset_password_request_otp');

    Route::post('/login', 'Auth\AuthenController@login');
    Route::get('/logout', 'Auth\AuthenController@logout');
    Route::post('/create_user', 'Auth\AuthenController@create_user');

    Route::post('/request_otp', 'Otp\OtpController@request_otp');
    Route::post('/verify_otp', 'Otp\OtpController@verify_otp');
    
    Route::get('/get-home-contents', 'WordPressController@getHomeContentsView');

    Route::put('/set-exchange-rate', 'ExchangeRatesController@setExchangeRate');
    Route::post('/login-admin', 'Auth\LoginController@login');
    Route::post('/logout-admin', 'HomeController@logout');
    Route::post('/add-bank', 'BanksController@addBank');
    Route::put('/change-active-banks', 'BanksController@changeActiveBanks');
});

Route::group(['prefix' => '_ajax'], function () {
    Route::post('/register_phone', 'Auth\AuthenController@register_phone');
    Route::put('/cancel_register', 'Auth\AuthenController@cancel_register');
    Route::post('/register_otp', 'Auth\AuthenController@register_otp');
    Route::post('/register_password', 'Auth\AuthenController@register_password');
    Route::post('/register_user', 'Auth\AuthenController@register_user');
});

Route::get('/get-contents', 'WordPressController@getPosts');

Route::get('/', 'HomeController@index');
Route::get('/aff/{affliate}', 'HomeController@index');
Route::get('/aff/promotion/{affliate}/{promotion_code}', 'HomeController@index');

Route::get('/promotion', 'HomeController@index');
Route::get('/promotion/{promotion_code}', 'Promotion\PromotionController@get_promotion_by_code');

Route::get('/lobby', 'Provider\ProviderController@get_providers');

Route::get('/get_provider_games/{provider}', 'Provider\ProviderController@get_provider_games');
Route::get('/get_provider_games/{provider}/{game_id}', 'Provider\ProviderController@get_provider_games_id');

Route::get('/get_promotions', 'PromotionController@get_promotions');

Route::get('/blog', 'Content\ContentController@index');
Route::get('/blog/{slug}', 'Content\ContentController@detail');

Route::get('/exchange-rate', 'ExchangeRatesController@index');
Route::get('/login-admin', 'HomeController@loginAdmin');
Route::get('/select_bank', 'HomeController@Select_bank');

Route::get('/test', 'BanksController@test');

Route::get('/home', 'HomeController@index')->name('home');


