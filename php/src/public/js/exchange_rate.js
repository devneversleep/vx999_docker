$(function () {
    $('#btn_exchange_rate').on('click', function(){
        Swal.fire({
            title: '<span style="color:#000000">Please wait<span> ',
        })
        Swal.showLoading()

        var new_rate = $('#exchange_rate').val();
        // var key = $('#key').val();

        $.ajax({
            type: "PUT",
            url: '/api/set-exchange-rate',
            data: {
                id: id,
                new_rate: new_rate,
            },
            success: (data) => {
                if (data.success == true) {
                    $('#text_th_exchange_rate').text('1 THB = ' + new_rate + ' LAK')
                    // $('#text_lao_exchange_rate').text('( 1 ບາດ = ' + new_rate + ' ກີບ )') 
                    Swal.fire({
                        title: '<span style="color:#000000">Success<span>',
                        icon: 'success',
                    })
                } else {
                    Swal.fire({
                        title: '<span style="color:#000000">Error<span>',
                        icon: 'error',
                    })
                }
                // console.log(data);
            },
            error: function(error){
                // console.log(error);
                var firstKey = Object.keys(error.responseJSON.errors)[0]
                if (firstKey.length > 0) {
                    if (firstKey == 'id') {
                        Swal.fire({
                            icon: 'error',
                            title: '<span style="color:#000000">มีบางอย่างผิดปกติ กรุณาติดต่อผู้พัฒนา<span>',
                        })
                    } else if (firstKey == 'new_rate') {
                        Swal.fire({
                            icon: 'error',
                            title: '<span style="color:#000000">Exchange Rate ต้องเป็นตัวเลข<span>',
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: '<span style="color:#000000">Error<span>',
                        })
                    }
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: '<span style="color:#000000">Error<span>',
                    })
                }
            }
        });
    });
})