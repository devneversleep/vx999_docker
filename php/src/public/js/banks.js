$(function () {
    $('#btn_add_bank').on('click', function(){
        Swal.fire({
            title: '<span style="color:#000000">Please wait<span> ',
        })
        Swal.showLoading()

        var name = $('#name').val();
        var number = $('#number').val();

        $.ajax({
            type: "POST",
            url: '/api/add-bank',
            data: {
                code: 'bank-kk',
                name: name,
                number: number,
            },
            success: (data) => {
                if (data.success == true) {
                    // $('#text_exchange_rate').text('1 THB = ' + new_rate +' LAK') 
                    // Swal.fire({
                    //     title: '<span style="color:#000000">Success<span>',
                    //     icon: 'success',
                    // })
                    window.location.reload();
                } else {
                    Swal.fire({
                        title: '<span style="color:#000000">Error<span>',
                        icon: 'error',
                    })
                }
                // console.log(data);
            },
            error: function (error){
                console.log(error);
                var firstKey = Object.keys(error.responseJSON.errors)[0]
                if (firstKey.length > 0) {
                    if (firstKey == 'name') {
                        Swal.fire({
                            icon: 'error',
                            title: '<span style="color:#000000">กรุณากรอกชื่อ - นามสกุล<span>',
                        })
                    } else if (firstKey == 'number') {
                        Swal.fire({
                            icon: 'error',
                            title: '<span style="color:#000000">กรุณากรอกเลขบัญชีธนาคารเป็นตัวเลข 18 หลัก<span>',
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: '<span style="color:#000000">Error<span>',
                        })
                    }
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: '<span style="color:#000000">Error<span>',
                    })
                }
            }
        });
    });

    
});

function changeActiveBanks()
{
    var bank_id = selected_bank_id[selected_bank_id.length - 1];
    // console.log(bank_id)
    Swal.fire({
        title: '<span style="color:#000000">Please wait<span> ',
    })
    Swal.showLoading()

    $.ajax({
        type: "PUT",
        url: '/api/change-active-banks',
        data: {
            bank_id: bank_id,
        },
        success: (data) => {
            if (data.success == true) {
                // $('#text_exchange_rate').text('1 THB = ' + new_rate +' LAK') 
                // Swal.fire({
                //     title: '<span style="color:#000000">Success<span>',
                //     icon: 'success',
                // })
                window.location.reload();
            } else {
                Swal.fire({
                    title: '<span style="color:#000000">Error<span>',
                    icon: 'error',
                })
            }
            // console.log(data);
        },
        error: function (error){
            // console.log(error);
            // var firstKey = Object.keys(error.responseJSON.errors)[0]
            Swal.fire({
                icon: 'error',
                title: '<span style="color:#000000">Error<span>',
            })
        }
    });
}