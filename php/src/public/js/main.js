$(function () {

    // $('.owl-carousel').owlCarousel({
    //     loop:true,
    //     margin:10,
    //     responsiveClass:true,
    //     items:1,
    //     nav:true,
    //     loop:true,
    //     lazyLoad: true,
    //     autoplay:true,
    //     autoplayTimeout:4000,
    //     autoplayHoverPause:true
    //     // responsive:{
    //     //     0:{
    //     //         items:1,
    //     //         nav:true
    //     //     },
    //     //     600:{
    //     //         items:1,
    //     //         nav:false
    //     //     },
    //     //     1000:{
    //     //         items:1,
    //     //         nav:true,
    //     //         loop:false
    //     //     }
    //     // }
    // })

    //navbar fix
    $(document).scroll(function (e) {
        var scrollTop = $(document).scrollTop();
        if (scrollTop > 0) {
            $('.header').addClass('sticky');
            $('.lobby-header').addClass('sticky-transparent');
        } else {
            $('.header').removeClass('sticky');
            $('.lobby-header').removeClass('sticky-transparent')
        }
    });

  

    // $('.btn-left').on('click', function () {
    //     if ($('.slide-left').hasClass('hide')) {
    //         $('.slide-left').removeClass('hide');
    //         $('.slide-left .wrapper-left').addClass('wow fadeInUp animated');
    //         $(".slide-left .wrapper-left").attr("style", "visibility: visible; animation-name: fadeInUp;");
    //         $('.modal-dialog').addClass('modal-big');
    //     } else {
    //         $('.slide-left').addClass('hide');
    //         $('.modal-dialog').removeClass('modal-big');
    //         $('.slide-left .wrapper-left').removeClass('wow fadeInUp animated');
    //         $(".slide-left .wrapper-left").removeAttr("style");
    //     }
    // });
    $('#OneregisterModal').on('hide.bs.modal', function (e) {
        $('.slide-left').addClass('hide');
        $('.modal-dialog').removeClass('modal-big');
        $('.slide-left .wrapper-left').removeClass('wow fadeInUp animated');
        $(".slide-left .wrapper-left").removeAttr("style");
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target // newly activated tab
        e.relatedTarget // previous active tab
        //slick
        $('.slick-promotion-text').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            centerMode: true,
            //variableWidth: true,
            asNavFor: '.slick-promotion-img'
        });
        $('.slick-promotion-img').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.slick-promotion-text',
            arrows: false,
            dots: true,
            variableWidth: true,
            centerMode: true,
            responsive: [
                /*{
                    breakpoint: 768,
                    settings: {
                    }
                },*/
                {
                    breakpoint: 575.98,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: false
                    }
                }
            ]
        });
    });

    $('.slick-promotion-header').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        arrows: false,
        centerMode: true,
        autoplay: true,
        autoplaySpeed: 5000,
        cssEase: 'linear'
        /*slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        variableWidth: true,
        centerMode: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                }
            },
            {
                breakpoint: 575.98,
                settings: {
                    slidesToShow: 1,
                    variableWidth: false
                }
            }
        ]*/
    });

    //nav slide
    $('#showRight,#showRight1,#customer-balance').click(function () {
        $('.menu-right').toggleClass('right-open');
    });
    $('.backBtn').click(function () {
        $('.menu').removeClass('top-open bottom-open right-open left-open pushleft-open pushright-open');
        $('body').removeClass('push-toleft push-toright');
    });
    $('body').addClass('push');

    $('.menu .navbar-nav a').click(function () {
        $('.menu').removeClass('top-open bottom-open right-open left-open pushleft-open pushright-open');
        $('body').removeClass('push-toleft push-toright');
    });
    $('#showRightPush').click(function () {
        $('body').toggleClass('push-toright');
        $('.push-right').toggleClass('pushright-open');
    });
});


function getBalance() {
    $.ajax({
        url: "/api/get_balance",
        type: "GET",
        datatype: "call_credit",
    }).done(function (data) {
        var money = data['current_balance'].toFixed(2);
        $('#user_credits').html(
            new Intl.NumberFormat('en-US', {
                minimumFractionDigits: 2,
            }).format(
                money
            ).toString()
        );
    }).fail(function (error) {
        console.log(error);
    });
}



function Fx_forgotpassword() {
    var request_phone_number = $('#request_phoneNumber').val();
    var phone_country = $('#register_phone_country').val();
    // console.log(request_phone_number);
    $.ajax({
            url: "api/reset_password_request_otp",
            type: "POST",
            data: {
                request_phone_number: request_phone_number,
                phone_country: phone_country,
            }
        })
        .done(function (data) {
            console.log(data['status']);
            if (data['status'] == 'non_member') {
                Swal.fire({
                    icon: "error",
                    title: "ໝາຍ ເລກສະມາຊິກບໍ່ຖືກຕ້ອງ",
                    customClass: {
                        container: "custom-modal",
                        popup: "modal-content-alert",
                        content: "swal-text-content",
                        title: "title-class",
                        confirmButton: "btn-confirm",
                    },
                }).then((result) => {
                    // $('#forgotModal').modal('hide');
                    // $("#registerModal").modal();
                });
            } else if (data['status'] == 'wrong_number') {
                Swal.fire({
                    icon: "error",
                    title: "ເບີໂທລະສັບບໍ່ຖືກຕ້ອງ",
                    customClass: {
                        container: "custom-modal",
                        popup: "modal-content-alert",
                        content: "swal-text-content",
                        title: "title-class",
                        confirmButton: "btn-confirm",
                    },
                }).then((result) => {
                    // $('#forgotModal').modal('hide');
                    // $("#registerModal").modal();
                });
            } else if (data['status'] == 'failed') {
                Swal.fire({
                    icon: "error",
                    title: data['response'],
                    customClass: {
                        container: "custom-modal",
                        popup: "modal-content-alert",
                        content: "swal-text-content",
                        title: "title-class",
                        confirmButton: "btn-confirm",
                    },
                }).then((result) => {
                    // $('#forgotModal').modal('hide');
                    // $("#registerModal").modal();
                });
            } else {
                $('#forgotModal').modal('hide');
                Swal.fire({
                    icon: "success",
                    title: "OTP ໄດ້ຖືກສົ່ງໄປຫາ ໝາຍ ເລກສະມາຊິກແລ້ວ",
                    showConfirmButton: false,
                    customClass: {
                        container: "custom-modal",
                        popup: "modal-content-alert",
                        content: "swal-text-content",
                        title: "title-class",
                        confirmButton: "btn-confirm",
                    },
                    timer: 2000
                }).then(
                    setTimeout(() => {
                        $("#resetpassword").modal();
                        // console.log(data);
                    }, 2000)
                );

            }
            // $("#user_password").modal();registerModalBody
        })
        .fail(function (error) {
            // console.log(error);
        });
}

function error_alert(message) {
    Swal.fire({
        html: '<p class="f-5">' + message + '</p>',
        confirmButtonText: "ຕົກ​ລົງ",
        customClass: {
            container: "custom-modal",
            popup: "modal-content-alert",
            content: "swal-text-content",
        },
        onBeforeOpen: function (ele) {
            $(ele).find('button.swal2-confirm.swal2-styled').toggleClass('swal2-confirm swal2-styled btn btn-primary col-4')
        }
    })
}

function confirm_alert(message) {
    Swal.fire({
        html: '<p class="f-5">' + message + '</p>',
        showCancelButton: true,
        confirmButtonColor: '#e7ac5a',
        cancelButtonColor: '#f5150',
        confirmButtonText: 'OK',
        customClass: {
            container: "custom-modal",
            popup: "modal-content-alert",
            content: "swal-text-content",
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        // onBeforeOpen: function (ele) {
        //     $(ele).find('button.swal2-confirm.swal2-styled').toggleClass('swal2-confirm swal2-styled btn btn-primary col-4')
        // }
    }).then((result) => {
        if (result.value) {
            cancel_deposit();
        }
    })
}

function success_alert(message, icon_code) {
    var icon = 'error';
    if (icon_code) {
        icon = 'success';
    }
    Swal.fire({
        html: '<p class="f-5">' + message + '</p>',
        icon: icon,
        customClass: {
            container: "custom-modal",
            popup: "modal-content-alert",
            content: "swal-text-content",
        },
    })

}

Object.filter = function( obj, predicate) {
    let result = {}, key;

    for (key in obj) {
        if (obj.hasOwnProperty(key) && !predicate(obj[key])) {
            result[key] = obj[key];
        }
    }

    return result;
};

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function Fx_refresh_credit() {
    $('#imgrefresh').addClass('rotateIn');
    setTimeout(() => {
        $('#imgrefresh').removeClass('rotateIn');
    }, 1000);
    $.ajax({
        url: "/api/get_balance",
        type: "GET",
        datatype: "call_credit",
    })
        .done(function (data) {
            var balance = data['current_balance'].toFixed(2)
            $('#user_credits').html(
                new Intl.NumberFormat('en-US', {
                    minimumFractionDigits: 2,
                }).format(
                    balance
                ).toString()
            );
        })
        .fail(function (error) {
            console.log(error);
        });
}


$('.autoplay').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
  });


// $('.owl-carousel').owlCarousel({
//     loop:true,
//     margin:10,
//     nav:true,
//     responsive:{
//         0:{
//             items:1
//         },
//         600:{
//             items:3
//         },
//         1000:{
//             items:5
//         }
//     }
// })
