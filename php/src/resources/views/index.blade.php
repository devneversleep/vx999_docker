@extends('layouts.main')
{{-- @section('title', 'Sexy Gaming เว็บเซ็กซี่บาคาร่า Game Baccarat อันดับของไทย คาสิโนออนไลน์ 24 ชม.')
@section('description', 'เว็บ Sexy Gaming สุดเซ็กซี่ที่คุณไม่ควรพลาด กับ Game Casino สาวบิกินี่บนเว็บออนไลน์ เล่นสนุก
ถอนเงินง่าย สมัครสมาชิก เล่น Baccarat รับโบนัสทันที ครบวงจร ด้วย เซ็กซี่ บาคาร่า เสือมังกร ไฮโล รูเล็ต
ที่มีสาวสวยมาแจกไพ่ ระบบฝาก-ถอน Auto 10 วินาที ไม่ต้องรอ') --}}
@section('content')

@if (Session::has('token'))
@include('layouts.logon')
@else
@include('layouts.nonlogin')
@endif

<!-- <hr class="hr-border-glow my-0"> -->
<!-- <div class="hexagon"><span></span></div> -->
<section class="section-two">
    
    <div class="container  box-service position-relative">
        
        <div class="row text-center">
            

            <div class="col-lg-4 col-md-4 col-sm-12  ">
                <div class="service-card" style="justify-content: center;">
                    <div class="img-service-">
                        <img src="{{secure_asset('img/ic_step_30s.png')}}" class="ic-vdo"
                            alt="LAOCASINO icon-30s" >
                    </div>
                    <div class="text-vdo pt-4">
                        <h3>ຝາກ ຖອນ 30 ວິນາທີ	</h3>
                        <hr class="hr-border-glow ml-0-m">
                        <p>ດ້ວຍລະບົບ ອັດຕະໂນມັດ	</p>
                    </div>
                </div>
                {{-- <div class="text-vdo-991">
                    <h3>ຝາກ ຖອນ 30 ວິ</h3>
                </div> --}}
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12  ">
                <div class="service-card " style="justify-content: center;">
                    <div class="img-service-">
                        <img src="{{secure_asset('img/ic_step_service.png')}}" class="ic-vdo"
                            alt="LAOCASINO icon-service" >
                    </div>
                    <div class="text-vdo pt-4">
                        <h3>ບໍລິການລູກຄ້າ</h3>
                        <hr class="hr-border-glow ml-0-m">
                        <p>ບໍລິິການຕະຫຼອດ 24 ຊົ່ວໂມງ </p>
                    </div>
                </div>
                {{-- <div class="text-vdo-991">
                    <h3>ບໍ​ລິ​ການ​ລູກ​ຄ້າ</h3>
                </div> --}}
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12  ">
                <div class="service-card " style="justify-content: center;">
                    <div class="img-service-">
                        <img src="{{secure_asset('img/ic_step_secure.png')}}" class="ic-vdo"
                            alt="LAOCASINO icon-bonus" >
                    </div>
                    <div class="text-vdo pt-4">
                        <h3>ຄວາມຫມັ້ນຄົງ</h3>
                        <hr class="hr-border-glow ml-0-m">
                        <p>ຖອນສູງສຸດ 1 ລ້ານບາດຕໍ່ມື້</p>
                    </div>
                </div>
                {{-- <div class="text-vdo-991">
                    <h3>โຄວາມໝັ້ນຄົງ</h3>
                </div> --}}
            </div>
        </div>
    </div>

    <div class="index-tab-container ">
        <div class="-tab-menu-color mb-4 mb-sm-5">
            <div class="container tab-color-m">
                <ul class="nav nav-tabs tab">

                    <li class="nav-item  active index js-tab-scrolled " id="tab-index">
                        <a data-toggle="tab" href="#tab-content-index" class="nav-link active">
                            <img src="{{secure_asset('img/tab_index.png')}}" alt="logo_index" class="ic"><br>
                            <span class="d-sm-none d-inline-block mt-2 text-gray-lighter">LAOCASINO</span>
                            <span class="d-sm-inline-block d-none mt-2 text-gray-lighter">LAOCASINO</span>
                            <hr class="hr-border-glow mb-2">
                        </a>
                    </li>
                    <li class="nav-item  promotion js-tab-scrolled" id="tab-promotion">
                        <a data-toggle="tab" href="#tab-content-promotion" class="nav-link ">
                            <img src="{{secure_asset('img/tab_promotion.png')}}" alt="logo_promotion" class="ic"><br>
                            <span class="d-sm-none d-inline-block mt-2 text-gray-lighter">ໂປໂມຊັ່ນ</span>
                            <span class="d-sm-inline-block d-none mt-2 text-gray-lighter">ໂປໂມຊັ່ນ</span>
                            <hr class="hr-border-glow mb-2">
                        </a>
                    </li>
                    <li class="nav-item manual js-tab-scrolled" id="tab-manual">
                        <a data-toggle="tab" href="#tab-content-manual" class="nav-link ">
                            <img src="{{secure_asset('img/tab_manual.png')}}" alt="logo_manual" class="ic"><br>
                            <span class="d-sm-none d-inline-block mt-2 text-gray-lighter">ແນະນຳ​</span>
                            <span class="d-sm-inline-block d-none mt-2 text-gray-lighter">ແນະນຳການໃຊ້ງານ​</span>
                            <hr class="hr-border-glow mb-2">
                        </a>
                    </li>
                    <li class="nav-item event js-tab-scrolled" id="tab-event">
                        <a data-toggle="tab" href="#tab-content-event" class="nav-link ">
                            <img src="{{secure_asset('img/tab_event.png')}}" alt="logo_event" class="ic"><br>
                            <span class="d-sm-none d-inline-block mt-2 text-gray-lighter">ກິດຈະກຳ​</span>
                            <span class="d-sm-inline-block d-none mt-2 text-gray-lighter">ກິດຈະກຳ​</span>
                            <hr class="hr-border-glow mb-2">
                        </a>
                    </li>
                </ul>
            </div>
        </div>



        <div class="tab-content">
            <div class="tab-pane active" id="tab-content-index">
                <div class="tab-index position-relative">
                    <div class="box-float-2 ">
                        <img class="w-100 lazy " data-src="{{secure_asset('img/bg_float.png')}}" alt="">
                    </div>
                    <div class="container mt-4 blog-ruby ">

                        <div class="notice-box top my-5">
                            <div class="row">
                                <div class="col-12 text-center ">
                                    <img class="img-mobile-" src="{{secure_asset('img/logo_main.png')}}"
                                        alt="LAOCASINO Play">
                                </div>
                            </div>
                            <div class="description position-relative">
                                <hr class="hr-border-glow-float-top">
                                <div class="row m-0 ">
                                    <div class="col-lg-12 text-white position-relative">
                                        <div class="footer-lobby-logo">
                                            <div class="container px-0">
                                                <div class="casino-wrapper">
                                                    <ul class="nav-container">
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <a href="/lobby" target="_blank">
                                                                <img src="https://core.igame.bet/build/admin/img/lobby_main/sa-gaming-logo-circle.png"
                                                                    alt="sa-gaming-logo-circle" class="logo-circle">
                                                            </a>
                                                        </li>
                                                        <!-- <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <a href="/lobby" target="_blank">
                                                                <img src="https://core.igame.bet/build/admin/img/lobby_main/wm-logo-circle.png"
                                                                    alt="" class="logo-circle">
                                                            </a>
                                                        </li> -->
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <a href="/lobby" target="_blank">
                                                                <img src="https://core.igame.bet/build/admin/img/lobby_main/bbin-logo-circle.png"
                                                                    alt="bbin logo-circle" class="logo-circle">
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <a href="/lobby" target="_blank">
                                                                <img src="https://core.igame.bet/build/admin/img/lobby_main/sexy-bac-logo-circle.png"
                                                                    alt="sexy-bac logo-circle" class="logo-circle">
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <a href="/lobby" target="_blank">
                                                                <img src="https://core.igame.bet/build/admin/img/lobby_main/netent-live-logo-circle.png"
                                                                    alt="netent-live logo-circle" class="logo-circle">
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <a href="/lobby" target="_blank">
                                                                <img src="https://core.igame.bet/build/admin/img/lobby_main/dream-gaming-logo-circle.png"
                                                                    alt="dream-gaming logo-circle" class="logo-circle">
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <a href="/lobby" target="_blank">
                                                                <img src="https://core.igame.bet/build/admin/img/lobby_main/asia-gaming-logo-circle.png"
                                                                    alt="asia-gaming logo-circle" class="logo-circle">
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="slot-wrapper mt-3">
                                                    <ul class="nav-container">
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <div class="cv-box">
                                                                <a href="/lobby" target="_blank">
                                                                    <img src="https://core.igame.bet/build/admin/img/lobby_main/evo-play-cover-vertical.png"
                                                                        alt="evo-play cover vertical"
                                                                        class="img-fluid cover-vertical">
                                                                </a>
                                                            </div>
                                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/evo-play-logo-horizontal.png"
                                                                alt="evo-play logo horizontal"
                                                                class="img-fluid logo-horizontal">
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <div class="cv-box">
                                                                <a href="/lobby" target="_blank">
                                                                    <img src="https://core.igame.bet/build/admin/img/lobby_main/ps-cover-vertical.png"
                                                                        alt="ps cover vertical"
                                                                        class="img-fluid cover-vertical">
                                                                </a>
                                                            </div>
                                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/ps-logo-horizontal.png"
                                                                alt="ps logo horizontal"
                                                                class="img-fluid logo-horizontal">
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <div class="cv-box">
                                                                <a href="/lobby" target="_blank">
                                                                    <img src="https://core.igame.bet/build/admin/img/lobby_main/sp-cover-vertical.png"
                                                                        alt="sp cover vertical"
                                                                        class="img-fluid cover-vertical">
                                                                </a>
                                                            </div>
                                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/sp-logo-horizontal.png"
                                                                alt="sp logo horizontal"
                                                                class="img-fluid logo-horizontal">
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <div class="cv-box">
                                                                <a href="/lobby" target="_blank">
                                                                    <img src="https://core.igame.bet/build/admin/img/lobby_main/netent-slot-cover-vertical.png"
                                                                        alt="netent-slot cover vertical"
                                                                        class="img-fluid cover-vertical">
                                                                </a>
                                                            </div>
                                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/netent-slot-logo-horizontal.png"
                                                                alt="netent-slot logo horizontal"
                                                                class="img-fluid logo-horizontal">
                                                        </li>
                                                        <!-- <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <div class="cv-box">
                                                                <a href="/lobby" target="_blank">
                                                                    <img src="https://core.igame.bet/build/admin/img/lobby_main/kingmaker-cover-vertical.png"
                                                                        alt="kingmaker cover vertical"
                                                                        class="img-fluid cover-vertical">
                                                                </a>
                                                            </div>
                                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/kingmaker-logo-horizontal.png"
                                                                alt="kingmaker logo horizontal"
                                                                class="img-fluid logo-horizontal">
                                                        </li> -->
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <div class="cv-box">
                                                                <a href="/lobby" target="_blank">
                                                                    <img src="https://core.igame.bet/build/admin/img/lobby_main/joker-cover-vertical.png"
                                                                        alt="joker cover vertical"
                                                                        class="img-fluid cover-vertical">
                                                                </a>
                                                            </div>
                                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/joker-logo-horizontal.png"
                                                                alt="joker logo horizontal"
                                                                class="img-fluid logo-horizontal">
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <div class="cv-box">
                                                                <a href="/lobby" target="_blank">
                                                                    <img src="https://core.igame.bet/build/admin/img/lobby_main/habanero-cover-vertical.png"
                                                                        alt="habanero cover vertical"
                                                                        class="img-fluid cover-vertical">
                                                                </a>
                                                            </div>
                                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/habanero-logo-horizontal.png"
                                                                alt="habanero logo horizontal"
                                                                class="img-fluid logo-horizontal">
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="notice-box">
                            <div class="box-chip-2">
                                <img class="img-chip-2 lazy" data-src="{{secure_asset('img/bg_extra3.png')}}" alt="">
                            </div>
                            <div class="box-chip-3">
                                <img class="img-chip-3 lazy" data-src="{{secure_asset('img/bg_extra2.png')}}" alt="">
                            </div>
                            <div class="description text-center">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="text-left">
                                            <h3 class="f-2" style="color:#FFE07B;">ເວັບການເງິນອອນລາຍ ຄົບຈົບ ໃນເວັບ ດຽວ ຫມັ່ນຄົງ ປອດໄພ 100%</h3>
                                            <hr class="hr-border-glow m-0">
                                            <p class="my-4">
                                                ເວັບຄາສິໂນຄົບວົງຈອນ ການເງິນໝັ້ນຄົງ ປອດໄພ 100% ເວັບ LaoCasino.bet ເປັນເວັບທີ່ໃຫ້ບໍລິການຫຼິນຄາສິໂນອອນລາຍ ໃນຄອມພິວເຕີ້ ແລະ ມືຖື ມີເກມ ບາຄາລ່າ ຍິງປາ ສະລ໋ອດ ແລະຫຼາກຫຼາຍເກມໃຫ້ຫຼິນໃນເວັບ ສະມາຊິກ ສາມາດຝາກ-ຖອນ ໄດ້ຕະລອດ 24 ຊົ່ວໂມງ ບໍ່ມີການປິດປັບປຸງ ຫຼິນຜ່ານເວັບໄດ້ງ່າຍ ໆ ທີ່ Laocasino.bet						
                                            </p>

                                        </div>
                                        <div class="row">
                                            <div class="col-lg-8 col-md-12">
                                                <p class="text-left">
                                                    ລະບົບພັດທະນາໃຫ້ບໍ່ມີການປິດປັບປຸງເພື່ອໃຫ້ສະມາຊິກ ສະດວກໃນການຫຼິມຕະຫຼອດ 24 ຊົ່ວໂມງ ຕະຫຼອດອາທິດ ທັ້ງນີ້ ຍັງສາມາດຫຼິນຜ່ານ ໂທລະສັບມືຖື ຄອມພິວເຕີ້ ແທັບເລັດ ສະດວກສະບາຍ ດ້ວຍບໍລິການ ຝາກ - ຖອນຕະຫຼອດ 24 ຊົ່ວໂມງ ດ້ວຍທີມງານ ມືອາຊີບ ດ້ວຍລະບົບອັດຕະໂນມັດ ຝາກ - ຖອນ ຜ່ານຫນ້າເວັບໄດ້ເລີຍ ສະດວກສະບາຍ ເວັບຕົງບໍ່ຜ່ານເອເຍ່ນ ຄົບວົງຈອນ ເວັບດຽວທີ່ບໍ່ຕ້ອງຍ້າຍເງີນໄປຍ້າຍ ເງີນມາ ທາງເຮົາໄດ້ນຳຄາສິໂນຊັ້ນນຳມາໃວ້ທີ່ດຽວ ເຊັ່ນ SA gaming , Sexy Baccarat , WM Casino , DG Casino , ALL BET , PRETTY GAMING ສະລ້ອຕຫຼາຍຮູບແບບໃຫ້ເລືອກຫຼິນ ເຊັ່ນ Evo play , JOKER , PG pocket game soft ອີກຫຼາກຫຼາຍໃຫ້ຫຼິນ					
                                                </p>
                                            </div>
                                            <div class="col-lg-4 col-md-12">
                                                <div class="box-snook">
                                                    <img class="img-snook lazy" data-src="{{secure_asset('img/bg_extra5.png')}}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="container mt-5 mb-3">


                        <!-- บทความ -->
                        <div class="row" id="page-1">
                            <div class="col-7 blog mb-5 mt-0">
                                <div class="d-flex">
                                    <div>
                                        <img class="img-blog" src="{{ secure_asset('img/2x/ic_content@2x.png') }}" alt="">
                                    </div>
                                    <div class="text-content">
                                        <h2 class="text-white pt-3">บทความ</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-5 mt-2 pt-4">
                                <div class="float-right">
                                    <a href="/blog" class="btn btn-warning px-4" style="text-decoration: underline">
                                        ดูทั้งหมด
                                    </a>
                                </div>
                            </div>
                            @foreach (array_splice($content, 0, 6) as $item)
                            <div class="col-md-4 mb-5 h-100">
                                <a href="/blog/{{ $item['slug'] }}">
                            <div class="main-blog- ">
                                <div class="blog-img">
                                    <img loading="lazy" src="{{$item['image']}}" alt="{{ $item['image_alt'] }}"
                                        class="img-blog-content">
                                </div>
                                <div class="bg-content text-left p-2">
                                    <h3 class="m-0 text-white -text-overflow">{!! $item['title'] !!}</h3>
                                    <p class="m-0 -font-normal -text-overflow-content">
                                        {!! strip_tags($item['content']) !!}
                                    </p>
                                    <div class="box-date mt-2">
                                        <span><i class="far fa-clock" style="font-size: 12px;"></i>
                                            {{ date_format(date_create($item['date']), 'd/m/Y') }}</span>
                                        <span><i class="fas fa-star" style="font-size: 12px;"></i>
                                            {{ join(',', $item['categories']) }}</span>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div> --}}

                <div class="container my-4">
                    <div class="border-scanner my-5">
                        <div class="row bg-scanner">

                            <div class="col-12 col-md-12 col-lg-6 order-0 order-lg-1 -img-mobile-HD">
                                <div class="streaming  mt-xl-0" style="position: relative;">
                                    <img data-src="{{secure_asset('img/mobile_hd.png')}}" data-wow-duration="0.3s"
                                        alt="Mobile HD" class="image wow fadeInUp lazy">
                                </div>
                            </div>
                            <div class="col-12 col-lg-3  order-1 d-flex align-items-center img-scanner justify-content-center">
                                <div class="scanner d-flex justify-content-center" style="flex-wrap: wrap;">

                                    <div class="" style="border: solid 1px white">
                                        <div class="logo qr-lobby" id="qr_lobby" alt="LAOCASINO QR-code"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-3 order-1 -img-pretty-bottom">
                                <div class="detail text-center px-2 w-100">
                                    <h3 class="f-xl-4 f-6 mb-0 " style="color: #FECC69;">ສະແກນເພື່ອຫຼີ້ນ</h3>
                                    <span class="text-white">Compatible with iOS &amp; Android
                                        Devices</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div style="padding-top: 5px;">


                </div>


            </div>
        </div>
        <div class="tab-pane d-none" id="tab-content-promotion">
            <div class="container">
                <div class="owl-carousel owl-theme">
                    {{-- <div class="item">
                        <img class="w-100" src="images/tab-1-img.png" alt="">
                        <div class="carousel-caption- mt-4">
                            <h3 class="text-yellow">ການສົ່ງເສີມ 1</h3>
                            <p>- ສຳລັບສະມາຊິກ ທຸກຍອດຝາກ ຮັບ 2 % ເຮົດ 1 ເທິນ ຖອນໄດ້ເລິີຍ</p>
                            <p>- ຮັບໄດ້ສູງສຸດ 1000 ບາດ</p>
                        </div>
                        <div class="text-center justify-content-center mt-5">
                            <a href="" class="btn btn-primary -pro">ຮັບສິດ</a>
                        </div>
                    </div>
                    <div class="item">
                        <img class="w-100" src="images/tab-1-img.png" alt="">
                        <div class="carousel-caption- mt-4">
                            <h3 class="text-yellow">ການສົ່ງເສີມ 2</h3>
                            <p>- ສຳລັບສະມາຊິກ ທຸກຍອດຝາກ ຮັບ 2 % ເຮົດ 1 ເທິນ ຖອນໄດ້ເລິີຍ</p>
                            <p>- ຮັບໄດ້ສູງສຸດ 1000 ບາດ</p>
                        </div>
                        <div class="text-center justify-content-center mt-5">
                            <a href="" class="btn btn-primary -pro">ຮັບສິດ</a>
                        </div>
                    </div>
                    <div class="item">
                        <img class="w-100" src="images/tab-1-img.png" alt="">
                        <div class="carousel-caption- mt-4">
                            <h3 class="text-yellow">ການສົ່ງເສີມ 3</h3>
                            <p>- ສຳລັບສະມາຊິກ ທຸກຍອດຝາກ ຮັບ 2 % ເຮົດ 1 ເທິນ ຖອນໄດ້ເລິີຍ</p>
                            <p>- ຮັບໄດ້ສູງສຸດ 1000 ບາດ</p>
                        </div>
                        <div class="text-center justify-content-center mt-5">
                            <a href="" class="btn btn-primary -pro">ຮັບສິດ</a>
                        </div>
                    </div>                  --}}
                </div>
            </div>

            {{-- <div class="intro-promotion" id="promotions_tab">
                <div class="slider slick-promotion-img" id="promotion_img">

                </div>
                <div class="slider slick-promotion-text" id="promotion_text">

                </div>
                
            </div> --}}
        </div>
        <div class="tab-pane " id="tab-content-manual">
            <div class="tab-manual pb-5">
                <div class="container container-wrapper pt-4">
                    <ul class="nav nav-tabs tabs d-flex justify-content-center video-tab-wrapper">
                        <li class="nav-item  active register" id="tab-register">
                            <a data-toggle="tab" href="#tab-content-register" class="nav-link tab-howto-index active">
                                ວິທີການສະໝັກ
                            </a>
                        </li>
                        <li class="nav-item deposit" id="tab-deposit">
                            <a data-toggle="tab" href="#tab-content-deposit" class="nav-link tab-howto-index ">
                                ວິທີການຝາກ
                            </a>
                        </li>
                        <li class="nav-item withdraw" id="tab-withdraw">
                            <a data-toggle="tab" href="#tab-content-withdraw" class="nav-link tab-howto-index ">
                                ວິທີການຖອນ
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content text-center mt-2">
                        <div class="tab-pane active video-container" id="tab-content-register">
                            <div class="service-wrapper mt-2 pt-2">



                                <div class="row justify-content-center my-4">
                                    <div
                                        class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                        <a href="#0" class="d-flex flex-md-column flex-row"
                                            data-target="#register_modal" data-toggle="modal">
                                            <div class="ic-wrapper mb-2">
                                                <img data-src="{{secure_asset('img/ic_step_phone.png')}}" alt="icon register"
                                                    class="ic-register lazy">
                                            </div>
                                            <div class="text-left text-md-center">
                                                <h3><span class="d-inline-block d-md-none">1.</span> ສະຫມັກສະມາຊິກ </h3>
                                                <hr class="hr-border-glow">
                                                <span
                                                    class="d-none d-lg-block text-muted-lighter f-6">ໃສ່ເບີໂທລະສັບມືຖືຂອງທ່ານ ໃຫ້ຖືກຕ້ອງ</span>
                                                <span
                                                    class="d-block d-lg-none text-muted-lighter f-6">ໃສ່ເບີໂທລະສັບມືຖືຂອງ<br class="br-min-425">ທ່ານໃຫ້ຖືກຕ້ອງ</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div
                                        class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                        <div class="ic-wrapper mb-2">
                                            <img data-src="{{secure_asset('img/ic_step_otp.png')}}" alt="icon register"
                                                class="ic-otp lazy">
                                        </div>
                                        <div class="text-left text-md-center">
                                            <h3><span class="d-inline-block d-md-none">2.</span> ລໍຖ້າ SMS ຍືນຍັນ </h3>
                                            <hr class="hr-border-glow">

                                            <span class="d-none d-lg-block text-muted-lighter f-6">ໃສ່ເບີ OTP ໃຫ້ຖືກຕ້ອງ ພ້ອມຕັ້ງລະຫັດຜ່ານໄຊ້ງານ</span>
                                            <span class="d-block d-lg-none text-muted-lighter f-6">ໃສ່ເບີ OTP ໃຫ້ຖືກຕ້ອງ<br class="br-min-425"> ພ້ອມຕັ້ງລະຫັດຜ່ານໄຊ້ງານ		
                                                </span>
                                        </div>
                                    </div>
                                    <div
                                        class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                        <div class="ic-wrapper mb-2">
                                            <img data-src="{{secure_asset('img/ic_step_bank.png')}}" alt="icon register"
                                                class="ic-bank lazy">
                                        </div>

                                        <div class="text-left text-md-center">
                                            <h3><span class="d-inline-block d-md-none">3.</span>
                                                ໃສ່ຊື່ອບັນຊີແລະເລກບັນ<br class="br-min-425">ຊີທະນາຄານການຄ້າ</h3>
                                            <hr class="hr-border-glow">
                                            <span
                                                class="d-none d-lg-block text-muted-lighter f-6">ໃສ່່ຊື່ອແລະເລກບັນຊີຂອງທ່ານ ໃຫ້ຖືອຕ້ອງ		
                                                ­­
                                            </span>
                                            <span class="d-block d-lg-none text-muted-lighter f-6">ໃສ່່ຊື່ອແລະເລກບັນຊີຂອງ<br class="br-min-425">ທ່ານ ໃຫ້ຖືອຕ້ອງ		 
                                                
                                                ­­
                                            </span>
                                        </div>
                                    </div>

                                    <div class="video-outer-wrapper js-video-loaded my-5"
                                    data-source="/build/web/LAOCASINO/videos/register.mp4">
                                        <a id="register_video_btn" href="javascript:playVideo('register_video')">
                                            <img data-src="{{secure_asset('img/video_register_bg.jpg')}}" alt="register-background"
                                                class="img-fluid video-bg lazy">
                                        </a>
                                        <video id="register_video" class="img-fluid video-bg d-none" controls preload>
                                            <source src="{{ secure_asset('img/สมัคร LAOCASINO.mp4') }}" type='video/mp4' />
                                            Your browser does not support the video tag.
                                        </video>
                                        <div class="video-wrapper"></div>
                                    </div>
                                </div>



                            </div>

                        </div>
                        <div class="tab-pane video-container" id="tab-content-deposit">
                            <div class="service-wrapper mt-2 pt-2">


                                <div class="row justify-content-center my-4">
                                    <div
                                        class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                        <a href="#0" class="d-flex flex-md-column flex-row" data-toggle="modal"
                                            data-target="#depositModal">
                                            <div class="ic-wrapper mb-2">
                                                <img data-src="{{secure_asset('img/ic_step_deposit.png')}}" alt="icon register"
                                                    class="ic-register lazy">
                                            </div>
                                            <div class="text-left text-md-center">
                                                <h3><span class="d-inline-block d-md-none">1.</span> ກົດ " ຝາກເງິນ "</h3>
                                                <hr class="hr-border-glow">
                                                <span class="d-none d-lg-block text-muted-lighter f-6">ໃສ່ຈຳນວນເງິນທີ່ຕ້ອງການຝາກເຂ້າ ຫາກຕ້ອງການຮັບໂປໂມຊັ່ນ ກົດ " ຕ້ອງການຮັບໂປໂມຊັ່ນ " ເລືອກໂປໂມຊັ່ນທີ່ຕ້ອງການ ກົດ " ຍືນຍັນ "</span>
                                                <span class="d-block d-lg-none text-muted-lighter f-6">ໃສ່ຈຳນວນເງິນທີ່ຕ້ອງການຝາກເຂ້າ ຫາກຕ້ອງການຮັບໂປໂມຊັ່ນ ກົດ " ຕ້ອງການຮັບໂປໂມຊັ່ນ " ເລືອກໂປໂມຊັ່ນທີ່ຕ້ອງການ ກົດ " ຍືນຍັນ "</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div
                                        class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                        <div class="ic-wrapper mb-2">
                                            <img data-src="{{secure_asset('img/ic_step_deposit_add.png')}}" alt="icon register"
                                                class="ic-register lazy">
                                        </div>
                                        <div class="text-left text-md-center">
                                            <h3><span class="d-inline-block d-md-none">2.</span> ໂອນເງິນ</h3>
                                            <hr class="hr-border-glow">
                                            <span class="d-none d-lg-block text-muted-lighter f-6">ກົດ " ຄັດລອກເລກບັນຊີ  " ຫຼື ສະແກນ QR CODE ເພື່ອໂອນເງິນ 	</span>
                                            <span class="d-block d-lg-none text-muted-lighter f-6">ກົດ " ຄັດລອກເລກບັນຊີ  " ຫຼື ສະແກນ QR CODE ເພື່ອໂອນເງິນ 	</span>
                                        </div>
                                    </div>
                                    <div
                                        class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                        <div class="ic-wrapper mb-2">
                                            <img data-src="{{secure_asset('img/ic_step_deposit_done.png')}}" alt="icon register"
                                                class="ic-register lazy">
                                        </div>
                                        <div class="text-left text-md-center">
                                            <h3><span class="d-inline-block d-md-none">3.</span>
                                                ກົດ " ໂອນແລ້ວ "</h3>
                                            <hr class="hr-border-glow">

                                            <span class="d-none d-lg-block text-muted-lighter f-6">ລະບົບ ຈະທຳການເຕິມຍອດເງິນໃຫ້ອັດຕະໂນມັດ ບໍ່ເກຶນ 30 ວິນາທີ			
                                                ຫາກຍອດເງິນຍັງບໍ່ເຂົ້າ ຕິດຕໍ່ໄດ້ທີ່ line id : @laocasino	</span>
                                            <span class="d-block d-lg-none text-muted-lighter f-6">ລະບົບ ຈະທຳການເຕິມຍອດເງິນໃຫ້ອັດຕະໂນມັດ ບໍ່ເກຶນ 30 ວິນາທີ			
                                                ຫາກຍອດເງິນຍັງບໍ່ເຂົ້າ ຕິດຕໍ່ໄດ້ທີ່ line id : @laocasino	</span>
                                        </div>
                                    </div>

                                    <div class="video-outer-wrapper js-video-loaded my-5"
                                    data-source="{{secure_asset('/build/web/LAOCASINO/videos/register.mp4')}}">
                                        <a id="deposit_video_btn" href="javascript:playVideo('deposit_video')">
                                            <img data-src="{{secure_asset('img/video_deposit_bg.jpg')}}" alt="register-background"
                                                class="img-fluid video-bg lazy">
                                        </a>
                                        <video id="deposit_video" class="img-fluid video-bg d-none" controls preload>
                                            <source src="{{ secure_asset('img/ฝาก LAOCASINO.mp4') }}" type='video/mp4' />
                                            Your browser does not support the video tag.
                                        </video>
                                        <div class="video-wrapper"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane video-container" id="tab-content-withdraw">
                            <div class="service-wrapper mt-2 pt-2">


                                <div class="row justify-content-center my-4">
                                    <div
                                        class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                        <a href="#0" class="d-flex flex-md-column flex-row" data-toggle="modal"
                                            data-target="#withdrawModal">
                                            <div class="ic-wrapper mb-2">
                                                <img data-src="{{secure_asset('img/ic_step_withdraw.png')}}" alt="icon register"
                                                    class="ic-register lazy">
                                            </div>
                                            <div class="text-left text-md-center">
                                                <h3><span class="d-inline-block d-md-none">1.</span> ກົດ " ຖອນເງິນ"</h3>
                                                <hr class="hr-border-glow">
                                                <span class="d-none d-lg-block text-muted-lighter f-6">ກົດ " ຖອນເງິນ"</span>
                                                <span class="d-block d-lg-none text-muted-lighter f-6">ກົດ " ຖອນເງິນ"</span>
                                            </div>
                                        </a>
                                    </div>

                                    <div
                                        class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                        <div class="ic-wrapper mb-2">
                                            <img data-src="{{secure_asset('img/ic_step_withdraw_add.png')}}" alt="icon register"
                                                class="ic-register lazy">
                                        </div>
                                        <div class="text-left text-md-center">
                                            <h3><span class="d-inline-block d-md-none">2.</span> ໃສ່່ຍອດເງິນ</h3>
                                            <hr class="hr-border-glow">
                                            <span class="d-none d-lg-block text-muted-lighter f-6">ໃສ່ຍອດເງິນທີ່ຕ້ອງການຖອນ</span>
                                            <span class="d-block d-lg-none text-muted-lighter f-6">ໃສ່ຍອດເງິນທີ່ຕ້ອງການຖອນ</span>
                                        </div>
                                    </div>

                                    <div
                                        class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                        <div class="ic-wrapper mb-2">
                                            <img data-src="{{secure_asset('img/ic_step_withdraw_done.png')}}" alt="icon register"
                                                class="ic-register lazy">
                                        </div>
                                        <div class="text-left text-md-center">
                                            <h3><span class="d-inline-block d-md-none">3.</span> ກົດ " ຍືນຍັນ "</h3>
                                            <hr class="hr-border-glow">
                                            <span class="d-none d-lg-block text-muted-lighter f-6">ກົດ " ຍືນຍັນ " ລະບັບອັດຕະໂນມັດ ຈະທຳການຖອນໃຫ້		
                                            </span>
                                            <span class="d-block d-lg-none text-muted-lighter f-6">ກົດ " ຍືນຍັນ "<br class="br-min-425"> ລະບັບອັດຕະໂນມັດ ຈະທຳການຖອນໃຫ້		
                                                
                                            </span>
                                        </div>
                                    </div>

                                    <div class="video-outer-wrapper js-video-loaded my-5"
                                    data-source="{{secure_asset('/build/web/LAOCASINO/videos/register.mp4')}}">
                                        <a id="withdraw_video_btn" href="javascript:playVideo('withdraw_video')">
                                            <img data-src="{{secure_asset('img/video_withdraw_bg.jpg')}}" alt="register-background"
                                                class="img-fluid video-bg lazy">
                                        </a>
                                        <video id="withdraw_video" class="img-fluid video-bg d-none" controls preload>
                                            <source src="{{ secure_asset('img/ถอน LAOCASINO.mp4') }}" type='video/mp4' />
                                            Your browser does not support the video tag.
                                        </video>
                                        <div class="video-wrapper"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane " id="tab-content-event">
            <div class="container text-center">
                <h3 class="text-white">ຕິດຕາມເບິ່ງກິດຈະກຳທີ່ກຳລັງຈະມາເຖິງ</h3>
                {{-- <span class="text-white">ยังไม่มีข้อมูลในส่วนนี้</span> --}}
            </div>
        </div>
    </div>






    </div><!-- index-tab-container -->
</section><!-- section-two -->

<style>
    .qr-lobby img {
        width: 180px;
        height: 180px;
    }
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/qrcode-generator/1.4.4/qrcode.min.js"
    integrity="sha512-ZDSPMa/JM1D+7kdg2x3BsruQ6T/JpJo3jWDWkCZsP+5yVyp1KfESqLI+7RqB5k24F7p2cV7i2YHh/890y6P6Sw=="
    crossorigin="anonymous"></script>
<script>
    function playVideo(video_name) {
        $('#' + video_name + '_btn').addClass('d-none')
        $('#' + video_name).removeClass('d-none')
        $('#' + video_name).get(0).play()
    }

    $(document).ready(() => {
        var qr = qrcode(0, 'L');
        // qr.addData('https://{{$_SERVER['SERVER_NAME'] . "/lobby"}}')
        qr.addData("https://lin.ee/3Px1baTSC");
        qr.make()
        $("#qr_lobby").html(qr.createImgTag())

        var promotion = null;
        var promotions_deposit = null;

        // $.ajax({
        //     url: "/get-contents",
        //     method: "GET",
        //     success: function(response) {
        //         $contents  = response;
        //         console.log(response);
        //     },error: function(response) {
        //         console.log(response);
        //     }
        // })

        $.ajax({
            url: "/api/get_promotions",
            type: "GET"
        }).done(function (data) {
            data = Object.filter(data, ele => !ele.name)
            if (Object.size(data) > 0) {
                var html_img_promotion = `<div class="container d-sm-block d-none" id="promotion_img">
                                            <div class="owl-carousel owl-theme" id="owl_promotion">`
                var html_img_promotion_mobile = `<div class="container d-block d-sm-none" id="promotion_img_mobile">
                                            <div class="owl-carousel owl-theme" id="owl_promotion_mobile">`
                // var html_img_promotion_mobile = '<div class="slider slick-promotion-img d-block d-sm-none" id="promotion_img_mobile">'
                // var html_text_promotion = '<div class="slider slick-promotion-text" id="promotion_text">'
         
                for (var i in data) {

                    var banner_img = (data[i].images.length > 0 ? data[i].images.find(ele => ele.type ==
                                'banner').url : "images/tab-1-img.png");
                    var banner_mobile_img = (data[i].images.length > 0 ? (data[i].images.find(ele => ele.type ==
                            'banner_mobile') ? data[i].images.find(ele => ele.type ==
                                'banner_mobile').url : data[i].images.find(ele => ele.type ==
                                    'banner').url) : "images/tab-1-img.png");

                    html_img_promotion +=
                        (`<div class="item">
                            <img src="` + banner_img + `" class="w-100">
                            <div class="carousel-caption- mt-4">
                                <h3 class="text-yellow">` + data[i].name + `</h3>`+ 
                                data[i].content +
                            `</div>`)

                        if (data[i].type == "deposit") {
                            html_img_promotion += `<div class="text-center justify-content-center mt-5">
                                                        <button onclick="open_deposit_modal('` + data[i].code + `','` + data[i].name + `')" class="btn btn-primary -pro">ຮັບສິດ</button>
                                                    </div>`
                        }
                        html_img_promotion += `</div>`

                    html_img_promotion_mobile +=
                        (`<div class="item">
                            <img src="` + banner_mobile_img + `" class="w-100">
                            <div class="carousel-caption- mt-4">
                                <h3 class="text-yellow">` + data[i].name + `</h3>`+ 
                                data[i].content +
                            `</div>`)

                        if (data[i].type == "deposit") {
                            html_img_promotion_mobile += `<div class="text-center justify-content-center mt-5">
                                                            <button onclick="open_deposit_modal('` + data[i].code + `','` + data[i].name + `')" class="btn btn-primary -pro">ຮັບສິດ</button>
                                                        </div>`
                        }
                        html_img_promotion_mobile += `</div>`
                }
                html_img_promotion += `</div></div>` 
                html_img_promotion_mobile += `</div></div>` 
                // html_img_promotion_mobile += '</div>'
                // html_text_promotion += '</div>'

                $("#tab-content-promotion").html(html_img_promotion + html_img_promotion_mobile)

                $('#owl_promotion').owlCarousel({
                    stopOnHover : true,
                    navigation:true,
                    paginationSpeed : 1000,
                    goToFirstSpeed : 2000,
                    singleItem : true,
                    autoHeight : true,
                    items: 1,
                    lazyLoad: true,
                    loop: true,
                    center: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                    dots: true,
                    nav: false,
                    animateIn: 'fadeIn',
                    animateOut: 'fadeOut'
                })

                $('#owl_promotion_mobile').owlCarousel({
                    stopOnHover : true,
                    navigation:true,
                    paginationSpeed : 1000,
                    goToFirstSpeed : 2000,
                    singleItem : true,
                    autoHeight : true,
                    items: 1,
                    lazyLoad: true,
                    loop: true,
                    center: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                    dots: true,
                    nav: false,
                    animateIn: 'fadeIn',
                    animateOut: 'fadeOut'
                })

                $("#tab-content-promotion").removeClass("d-none");

            } else {
                $("#owl_promotion").html(
                    '<div class="container text-center"><h3 class="text-white">โปรโมชั่น</h3><span class="text-white">ยังไม่มีข้อมูลในส่วนนี้</span></div>'
                )

                $("#tab-content-promotion").removeClass("d-none");
            }
            
        })
    })

$(window).on('load', function() {
    $.ajax({
        url: "/api/get-home-contents",
        method: "GET",
        success: function(response) {
            $("#page-1").html(response);
        },error: function(response) {
            console.log(response);
        }
    })
})
</script>

@endsection
