<div class="col-7 blog mb-5 mt-0">
    <div class="d-flex">
        <div>
            <img class="img-blog" src="{{ secure_asset('img/ic_content.png') }}" alt="">
        </div>
        <div class="text-content">
            <h2 class="text-white pt-3">บทความ</h2>
        </div>
    </div>
</div>
<div class="col-5 mt-2 pt-4">
    <div class="float-right">
        <a href="/blog" class="btn btn-warning px-4" style="text-decoration: underline">
            ดูทั้งหมด
        </a>
    </div>
</div>
@foreach (array_splice($content, 0, 6) as $item)
<div class="col-md-4 mb-5 h-100">
    <a href="/blog/{{ $item['slug'] }}">
        <div class="main-blog- ">
            <div class="blog-img">
                <img loading="lazy" src="{{$item['image']}}" alt="{{ $item['image_alt'] }}" class="img-blog-content">
            </div>
            <div class="bg-content text-left p-2">
                <h3 class="m-0 text-white -text-overflow">{!! $item['title'] !!}</h3>
                <p class="m-0 -font-normal -text-overflow-content">
                    {!! strip_tags($item['content']) !!}
                </p>
                <div class="box-date mt-2">
                    <span><i class="far fa-clock" style="font-size: 12px;"></i>
                        {{ date_format(date_create($item['date']), 'd/m/Y') }}</span>
                    <span><i class="fas fa-star" style="font-size: 12px;"></i>
                        {{ join(',', $item['categories']) }}</span>
                </div>
            </div>
        </div>
    </a>
</div>
@endforeach