<div class="modal-dialog modal-dialog-centered modal-size">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
        </button>
        <div class="modal-body">
            <form>
                <div class="text-center d-flex flex-column">
                    <h3 class="custom-modal-title mb-3" id="exampleModalLabel">ສະມາຊິກສົມບູນ</h3>
                    <div class="my-3 flex-column text-center">
                        <p>ຍິນ​ດີ​ຕ້ອນ​ຮັບ​ສູ່ LAOCASINO</p>
                    </div>
                    <div class="text-center mt-2">
                        <button type="button" data-toggle="modal" data-target="#DepositMoneyModal" data-dismiss="modal"
                            class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
                            ເລີ່ມຝາກເງິນເຂົ້າຮ່ວມ
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('#register_modal').on('hidden.bs.modal', function (e) {
        window.location.href = '/?new' + (window.location.pathname.split('/').length > 3 ? '&_p=' + window.location.pathname.split('/').slice(-1)[0] : '');
    });
</script>
