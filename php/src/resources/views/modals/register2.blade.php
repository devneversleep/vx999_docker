<!-- 2.registerModal -->
{{-- <div class="modal fade custom-modal" id="TworegisterModal" tabindex="-1" role="dialog"
    aria-labelledby="registerModalLabel" aria-hidden="true"> --}}
<div class="modal-dialog modal-dialog-centered modal-size">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
        </button>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg order-0">
                    <div class="text-center d-flex flex-column mb-lg-0 mb-5">
                        <div class="text-center d-flex flex-column">
                            <h3 class="custom-modal-title">ສະໜັກສະມາຊິກ
                                {{-- <hr class="hr-border-glow"> --}}
                            </h3>
                            <div class="modal-step-register">
                                <div class="d-block">
                                    <div class="col-7 m-auto row px-0">
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto ">
                                                <div class="modal--step-box-inner text-center">
                                                    1
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-border">
                                            <hr>
                                        </div>
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto step-active">
                                                <div class="modal--step-box-inner text-center">
                                                    2
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-border">
                                            <hr>
                                        </div>
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto ">
                                                <div class="modal--step-box-inner text-center">
                                                    3
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-border">
                                            <hr>
                                        </div>
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto ">
                                                <div class="modal--step-box-inner text-center">
                                                    4
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- modal-step-register -->
                            <form id="register2">
                                <div class="text-center">
                                    <img src="{{secure_asset('img/ic_step_otp.png')}}" alt="LAOCASINO ລົງທະບຽນ" class="img-fluid  my-3"
                                        width="100">
                                </div>
                                <div class="-x-input-icon mb-3 flex-column text-center">
                                    <img src="{{secure_asset('img/ic_mini_phone.png')}}" class="-icon" alt="login" width="12">
                                    <input type="number" id="register_otp" name="register[phoneNumber]" required
                                        pattern="[0-9]*" class="custom-form-control form-control mb-3"
                                        placeholder="ໃສ່ລະຫັດ OTP 4 ຕົວເລກ">
                                    <a href="javascript:cancel_register()" class="text-muted">ຍົກເລີກ</a>
                                </div>
                                <div class="text-center mt-4">
                                    <button type="submit" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
                                        ຢືນຢັນ
                                    </button>
                                </div>
                                <hr class="hr-border-glow w-100 mt-4">
                                <div class="text-center modal-contact no-fixed">
                                    <span>ພົບປັນຫາ</span>
                                    <a href="{{config('variable.line')}}" class="link-message " target="_blank">
                                        <span>ຕິດຕໍ່ບໍລິການລູກຄ້າ</span></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- </div> --}}
<script>
    function cancel_register()
    {
        $.ajax({
            type: "PUT",
            url: "/_ajax/cancel_register",
            success: function (response) {
                $("#register_modal").html(response);
            }
        });
    }

    $("#register2").off('submit')
    $("#register2").on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/_ajax/register_otp",
            data: {
                otp: $("#register_otp").val()
            },
            success: function (response) {
                $("#register_modal").html(response);
            },
            error: function (error) {
                error_alert(error.responseText);
            }
        });
    })
</script>
