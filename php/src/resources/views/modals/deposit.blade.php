<div class="row">
    <div class="col order-1 slide-left hide">
        <div class="wrapper-left">
            <h3 class="custom-modal-title mb-3">
                ການສົ່ງເສີມ
                <hr class="hr-border-glow" />
            </h3>
            <div class="game-styles">
                <div class="wrapper-inner scroll" style="text-align: center;" id="login-promotion"></div>
            </div>
            <!-- wrapper-inner -->
        </div>
    </div>
    <div class="col-lg order-lg-2 order-0">
        <div class="text-center d-flex flex-column mb-lg-0 mb-5">
            <h3 class="custom-modal-title mb-3">ຝາກເງິນ</h3>
            <div class="text-center">
                <img src="{{ secure_asset('img/ic_chip_deposit.png') }}" alt="ຝາກເງິນ" class="img-fluid my-3" width="120" />
            </div>
            <div id="promotion_name" class="mb-lg-0 mb-3 flex-column"></div>
            <div class="mb-3 flex-column">
                <a href="#" class="text-red btn-left f-6" id="deposite_lable">
                    <u id="text_active_show_promotion" onclick="setPromotion()">ຕ້ອງການຮັບໂປໂມຊັ່ນ</u>
                </a>
            </div>
            <div class="my-3 money-thb">
                <span class="text-white">1 THB = {{ $exchange_rate->thai_1_bath_to_laos }} LAK </span>
                <span class="text-secondary">( 1  ບາດ = {{ $exchange_rate->thai_1_bath_to_laos }}  ກີບ )</span>
            </div>
            <!-- <div class="my-3 money-lak d-none">
                <span class="text-white">1 LAK = 0.0032 THB</span>
                <span class="text-secondary">( 1 ກີບ = 0.0032  ບາດ )</span>
            </div> -->
            {{-- <hr class="w-100 my-4 hr-border-glow" /> --}}
            <p id="deposit_turnover" style="display: none;">
                turnover:
            </p>
            {{--
            <div class="my-3 flex-column text-center">
                <div class="input-container form-control">
                    <img
                        src="{{ secure_asset('img/ic_mini_withdraw.png') }}"
            alt="icon image"
            class="icon-modal"
            />
            <input type="number" step="0.01" required id="deposit_amount" name="deposit_amount"
                class="custom-form-control" placeholder="ເງິນຝາກຕ່ ຳ ສຸດ 100 ບາດ" />
        </div>
    </div>
    --}}
    <div class="-x-input-icon mb-3 text-center">
        {{-- <img src="{{ secure_asset('img/ic_mini_withdraw.png') }}" class="-icon" alt="login" width="12" /> --}}
        <div class="mr-3">
            <img class="img-mini-money" src="{{secure_asset('img/ic_mini_LAK.png') }}" alt="">
        </div>
        <div class="w-100 position-relative mr-2">
            <input type="number" id="deposit_amount_0" step="0.01" name="deposit_amount_0" required 
            class="custom-form-control form-control input-lak" placeholder="ໃສ່ຈຳນວນເງິນເປັນກີບ"
            onkeyup="calculate_amount(0)" />
            
        </div>
        <div class="text-money-lak">
            <span>₭ </span>
        </div>
    </div>
    <div class="-x-input-icon mb-3 text-center">
        {{-- <img src="{{ secure_asset('img/ic_mini_withdraw.png') }}" class="-icon" alt="login" width="12" /> --}}
        <div class="mr-3">
            <img class="img-mini-money" src="{{secure_asset('img/ic_mini_THB.png') }}" alt="">
        </div>
        <div class="w-100 position-relative mr-2">
            <input type="number" id="deposit_amount_1" step="0.01" name="deposit_amount_1" required 
            class="custom-form-control form-control input-thb" placeholder="ໃສ່ຈຳນວນເງິນເປັນບາດ"
            onkeyup="calculate_amount(1)" />
            
        </div>
        <div class="text-money-thb">
            <span>฿ </span>
        </div>
    </div>
    <div class="text-center mt-2">
        <button type="submit" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
            ຢືນຢັນ
        </button>
    </div>
    {{--
            <hr class="w-100 my-4 hr-border-glow" />
            --}}
    <hr class="hr-border-glow w-100 mt-4" />
    <div class="text-center modal-contact no-fixed ">
        <span>ພົບປັນຫາ</span>
        <a href="{{config('variable.line')}}" class="link-message" target="_blank">
            <span>ຕິດຕໍ່ບໍລິການລູກຄ້າ</span>
        </a>
    </div>
</div>
</div>
</div>

<!-- <script>
    $('.input-thb').click(function(){
        $('.money-thb').removeClass('d-none');
        $('.money-lak').addClass('d-none');
    });
    $('.input-lak').click(function(){
        $('.money-lak').removeClass('d-none');
        $('.money-thb').addClass('d-none');
    });
</script> -->

<script>
    var depNo = null;
    var deposit_page = 1;
    var id_depositModal = null;
    var launch_deposit = null;
    var promotion_code = null;
    var gold_eage_class = null;
    var deposit_amount = null;
    var deposit_turnover = null;
    var show_deposit_turnover = null;
    var exchange_rate = {{ $exchange_rate->thai_1_bath_to_laos }};

    $("#deposit_form").off("submit");
    $("#deposit_form").on("submit", function (e) {
        e.preventDefault();
        deposit_amount = document.getElementById("deposit_amount_1").value;
        //calculate_amount();
        //console.log(e, deposit_amount)
        $.ajax({
            url: "/api/deposit",
            type: "POST",
            data: {
                amount: deposit_amount || 0,
                promotion_code: promotion_code,
            },
        })
            .done(function (data) {
                if (data.bill.code == "200") {
                    gotoDeposit2(data);
                    // show_slide_left();
                } else {
                    var error_deposit = "";
                    if (typeof data.bill.error == "string") {
                        error_deposit += "<span>" + `${data.bill.error}` + "</span>";
                    } else {
                        for (const [key, value] of Object.entries(data.bill.error)) {
                            error_deposit += "<span>" + `${value}` + "</span>";
                        }
                    }
                    error_alert(error_deposit);
                }
            })
            .fail(function (error) {
                console.log(error);
            });
    });

    function setPromotion() {
        if (promotion_code != null) {
            promotion_code = null;
            validate_promotion = null;
            $("#show_promotion").html("");
            $("#deposit_turnover").hide();
            $("#promotion_name").html("");
            $("#text_active_show_promotion").html("ຕ້ອງການຮັບໂປໂມຊັ່ນ");
            gold_eage_class = document.getElementById("depositModal");
            gold_eage_class = gold_eage_class.getElementsByClassName("item");
            for (var i = 0; i < gold_eage_class.length; i++) {
                gold_eage_class[i].className = "item";
            }
            gold_eage_class = document.getElementsByClassName(
                "img-fluid img_promotion_click gold_eage"
            );
            for (var i = 0; i < gold_eage_class.length; i++) {
                gold_eage_class[i].className = "img-fluid img_promotion_click";
            }
        } else {
            id_depositModal = document.getElementById("depositModal");
            id_depositModal = id_depositModal.getElementsByClassName(
                "modal-dialog modal-dialog-centered modal-size"
            );
            id_depositModal[0].classList.add("modal-big");
        }
    }

    function close_deposit_modal() {
        if (deposit_page != 2) {
            $("#depositModal").modal("hide");
        } else {
            confirm_alert("ຕ້ອງການຍົກເລີກເງິນຝາກ ?");
        }
    }

    function cancel_deposit() {
        $.ajax({
            url: "/api/cancel_deposit",
            type: "PUT",
        })
            .done(function (data) {
                success_alert("ການໂອນຍ້າຍລົ້ມເຫລວ", 0);
                clearInterval(launch_deposit);
                deposit_again(data);
            })
            .fail(function (error) {
                console.log(error);
            });
    }

    function deposit_again(data) {
        $("#depositModal").modal("hide");
        $(".slide-left").addClass("hide");
        id_depositModal = document.getElementById("depositModal");
        id_depositModal = id_depositModal.getElementsByClassName(
            "modal-dialog modal-dialog-centered modal-size modal-big"
        );
        if (id_depositModal.length == 1) {
            id_depositModal[0].classList.remove("modal-big");
            $(".slide-left .wrapper-left").removeClass("wow fadeInUp animated");
            $(".slide-left .wrapper-left").removeAttr("style");
        }     
        $("#deposit_form").html(data.html);
    }

    function show_slide_left() {
        // $("#deposit_amount").attr('placeholder', $("#deposit_amount").attr('placeholder') + min_deposit);
        // $("#deposit_amount").attr('min', min_deposit);
        if (deposit_page != 1) {
            id_depositModal = document.getElementById("depositModal");
            id_depositModal = id_depositModal.getElementsByClassName(
                "modal-dialog modal-dialog-centered modal-size"
            );
            setTimeout(function () {
                $(".slide-left").removeClass("hide");
                $(".slide-left .wrapper-left").addClass(
                    "wow fadeInUp animated"
                );
                $(".slide-left .wrapper-left").attr(
                    "style",
                    "visibility: visible; animation-name: fadeInUp;"
                );
                id_depositModal[0].classList.add("modal-big");
            }, 500);
            //$("#tranfer_data").show();
        }
    }

    function gotoDeposit2(data) {

        $(".slide-left").addClass("hide");
        id_depositModal = document.getElementById("depositModal");
        id_depositModal = id_depositModal.getElementsByClassName(
            "modal-dialog modal-dialog-centered modal-size modal-big"
        );
        if (id_depositModal.length == 1) {
            id_depositModal[0].classList.remove("modal-big");
            $(".slide-left .wrapper-left").removeClass("wow fadeInUp animated");
            $(".slide-left .wrapper-left").removeAttr("style");
        }

        depNo = data.bill.number;
        deposit_page = 2;
        $("#deposit_form").html(data.html);

        document.getElementById("bank_number").innerHTML =
            data.bill.deposit_bank.number;
        document.getElementById("bank_name").innerHTML =
            data.bill.deposit_bank.name;

        // document.getElementById("bank_class").className = convertBankClass(
        //     data.bill.deposit_bank.bank_code
        // );

        var expired_at = new Date(data.bill.expired_at).getTime();
        //console.log(expired_at);

        launch_deposit = setInterval(function () {
            var now = new Date().getTime();
            var distance = expired_at - now;
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            var minutes = Math.floor(
                (distance % (1000 * 60 * 60)) / (1000 * 60)
            );
            console.log('eiei')
            $("#deposit_time").html(": " + minutes + " m " + seconds + " s ")
            if (distance <= 0) {
                cancel_deposit();
            }

            if (seconds % 5 == 0) {
                $.ajax({
                    url: "/api/check_pending_deposit",
                    type: "GET",
                })
                    .done(function (data) {
                        // console.log(data);
                        if (data.bill.code == "404") {
                            clearInterval(launch_deposit);
                        }
                    })
                    .fail(function (error) {
                        console.log(error);
                    });
            }
        }, 1000);

        $(".btn-copy.btn-bank-number").attr("data-clipboard-text", data.bill.deposit_bank.number)

        $(".btn-copy-").on('click', function (e) {
            $(e.target).text("ຄັດລອກແລ້ວ");
            setTimeout(function () {
                console.log(e);
                $(e.target).text("ສຳ ເນົາ");
                console.log("copy")
            }, 2000);
            copyToClipboard(e.currentTarget)
        })
    }

    function gotoDeposit3(data) {
        depNo = data.bill.number;
        deposit_page = 3;
        $("#deposit_form").html(data.html);

        document.getElementById("bank_number").innerHTML =
            data.bill.deposit_bank.number;
        document.getElementById("bank_name").innerHTML =
            data.bill.deposit_bank.name;

        // document.getElementById("bank_class").className = convertBankClass(
        //     data.bill.deposit_bank.bank_code
        // );
    }

    function convertBankClass(bank) {
        if (bank == "bank-bbl") return "bank bk3 color";
        else if (bank == "bank-bay") return "bank bk4 color";
        else if (bank == "bank-kbank") return "bank bk2 color";
        else if (bank == "bank-kk") return "bank bk5 color";
        else if (bank == "bank-cimb") return "bank bk11 color";
        else if (bank == "bank-tmb") return "bank bk6 color";
        else if (bank == "bank-scb") return "bank bk1 color";
        else if (bank == "bank-tbank") return "bank bk12 color";
        else if (bank == "bank-uob") return "bank bk7 color";
        else if (bank == "bank-ktb") return "bank bk8 color";
        else if (bank == "bank-gsb") return "bank bk9 color";
        else if (bank == "bank-sc") return "bank bk10 color";
    }

    $(".btn-left").on("click", function () {
        if ($(".slide-left").hasClass("hide")) {
            $(".slide-left").removeClass("hide");
            $(".slide-left .wrapper-left").addClass("wow fadeInUp animated");
            $(".slide-left .wrapper-left").attr(
                "style",
                "visibility: visible; animation-name: fadeInUp;"
            );
        } else {
            $(".slide-left").addClass("hide");
            id_depositModal = document.getElementById("depositModal");
            id_depositModal = id_depositModal.getElementsByClassName(
                "modal-dialog modal-dialog-centered modal-size modal-big"
            );
            id_depositModal[0].classList.remove("modal-big");
            $(".slide-left .wrapper-left").removeClass("wow fadeInUp animated");
            $(".slide-left .wrapper-left").removeAttr("style");
        }
    });

    $(document).ready(function () {
        // $("#deposit_amount").attr('placeholder', "ເພີ່ມເງີນຝາກຂັ້ນຕ່ ຳ " + min_deposit + " ບາດ");
        $("#deposit_amount").attr('min', min_deposit);

        $.ajax({
            url: "/api/check_pending_deposit",
            type: "GET",
        })
            .done(function (data) {
                // console.log(data);
                if (data.bill.code == "200" && data.bill.confirmed) {
                    gotoDeposit3(data);
                } else if (data.bill.code == "200") {
                    gotoDeposit2(data);
                } else if (data.bill.code == "404") {
                    getCodePromotion();
                }
            })
            .fail(function (error) {
                console.log(error);
            });
    });

    function GetPromotion(code, pro_name) {
        promotion_code = code;
        calculate_amount(0);
        $("#show_promotion").html(pro_name);
        gold_eage_class = document.getElementById("depositModal");
        gold_eage_class = gold_eage_class.getElementsByClassName(
            "img-fluid img_promotion_click"
        );
        for (var i = 0; i < gold_eage_class.length; i++) {
            if (gold_eage_class[i].id != promotion_code) {
                gold_eage_class[i].className = "img-fluid img_promotion_click";
            } else {
                $("#promotion_name").html(pro_name);
                gold_eage_class[i].className =
                    "img-fluid img_promotion_click gold_eage";
            }
        }
        $("#text_active_show_promotion").html("ຍົກເລີກການຍອມຮັບການສົ່ງເສີມ");
    }

    function getCodePromotion() {
        $.ajax({
            url: "/api/get_promotions",
            type: "GET",
            data: {
                type: "deposit",
            },
        })
            .done(function (data) {
                // console.log(data);
                get_data = data;
                if (data.length == 0) {
                    document.getElementById("login-promotion").innerHTML +=
                        "ບໍ່ພົບໂປຼໂມຊັນ";
                } else {
                    promotion_page = "";
                    for (j = 0; j < Object.keys(data).length; j++) {
                        number = "" + Object.keys(data)[j];
                        var image_promotion_deposit = "images/tab-1-img.png";
                        if (data[number]["images"].length != 0) {
                            image_promotion_deposit =
                                data[number]["images"][0]["url"];
                        }
                        var promotion_text_to_shop_after_click =
                            "<p" +
                            data[number]["name"] +
                            "p>";
                        promotion_page +=
                            '<div class="item -m-auto mb-2 pr-lg-3" onclick="GetPromotion(' +
                            "'" +
                            data[number]["code"] +
                            "'" +
                            ",'" +
                            data[number]["name"] +
                            "'" +
                            ')">' +
                            '<img id="' +
                            data[number]["code"] +
                            '" src="' +
                            image_promotion_deposit +
                            '" style="width:320px;height:106.5px;border-radius: 25px;" class="img-fluid img_promotion_click">' +
                            "</div>" +
                            "";
                        var promotion_text_to_shop_after_click =
                            "<p" +
                            data[number]["name"] +
                            "p>";
                    }
                    document.getElementById(
                        "login-promotion"
                    ).innerHTML = promotion_page;
                }
                var _p = new URLSearchParams(window.location.search).get("_p");
                if (_p) {
                    promotion_code = _p;
                    $("#depositModal").modal("show");

                    $.ajax({
                        url: "/promotion/" + _p,
                        type: "GET",
                    }).done(function (data) {
                        id_depositModal = document.getElementById("depositModal");
                        id_depositModal = id_depositModal.getElementsByClassName("modal-dialog modal-dialog-centered modal-size");
                        setTimeout(function () {
                            $(".slide-left").removeClass("hide");
                            $(".slide-left .wrapper-left").addClass("wow fadeInUp animated");
                            $(".slide-left .wrapper-left").attr("style", "visibility: visible; animation-name: fadeInUp;");
                            id_depositModal[0].classList.add("modal-big");
                            GetPromotion(data[0].code, data[0].name);
                        }, 500);
                    }).fail(function (error) {
                        console.log(error);
                    });
                }
            })
            .fail(function (error) {
                console.log(error);
            });
    }

    function calculate_amount(key) {
        deposit_amount = document.getElementById("deposit_amount_" + key).value;
        // console.log(deposit_amount)
        // document.getElementById("deposit_amount_" + Math.abs(key - 1).toString() ).value = exchange_rate
        if (key == 0) {
            $('#deposit_amount_1').val((deposit_amount / exchange_rate).toFixed(2))
            deposit_amount = (deposit_amount / exchange_rate).toFixed(2)
        } else if (key == 1) {
            $('#deposit_amount_0').val((deposit_amount * exchange_rate).toFixed(2))
        }
        // console.log(deposit_amount)
        if (deposit_amount > 0 && promotion_code) {
            $.ajax({
                url: encodeURI("/api/calculate_turnover/" + promotion_code),
                type: "POST",
                data: {
                    amount: deposit_amount,
                },
            })
                .done(function (data) {
                    // console.log(data)
                    deposit_turnover = data.turnover_details.must_do.toFixed(2);
                    $("#deposit_turnover").html(
                        "Turnover: " +
                        new Intl.NumberFormat("en-US", {
                            // maximumSignificantDigits: 2,
                            maximumFractionDigits: 2,
                        })
                            .format(deposit_turnover)
                            .toString()
                    );
                    $("#deposit_turnover").show();
                    // document.getElementById("deposit_turnover").innerHTML = "turnover: " + deposit_turnover;
                })
                .fail(function (error) {
                    console.log(error);
                });
        } else {
            deposit_turnover = deposit_amount;
            $("#deposit_turnover").hide();
        }
    }
    function open_deposit_modal(code, pro_name) {
        GetPromotion(code, pro_name);
        $("#depositModal").modal("show");
        // $("#deposit_turnover").hide();
        $(".slide-left").removeClass("hide");
        $("#depositModal .modal-dialog").addClass("modal-big");
    }
</script>
