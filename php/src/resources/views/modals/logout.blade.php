<!-- Logout Modal -->
<div class="modal fade custom-modal" id="LogoutModal" tabindex="-1" role="dialog" aria-labelledby="LogoutModal"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-size">
        <div class="modal-content">
            <div class="modal-body">
                <form action="/api/logout">
                    <div class="text-center d-flex flex-column">
                        <div class="my-3 flex-column text-center">
                            <p class="f-5 text-yellow">ຕ້ອງການອອກຈາກລະບົບ ?</p>
                        </div>
                        <div class="row">
                            <div class="col-6 d-flex align-items-stretch">
                                <button type="submit" class="btn btn-primary-dark-logout w-100">ຕົກ​ລົງ</button>
                            </div>
                            <div class="col-6 d-flex align-items-stretch">
                                <button type="button" class="btn btn-primary-dark-cancel w-100" data-dismiss="modal"
                                    aria-label="Close">ຍົກເລີກ</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
