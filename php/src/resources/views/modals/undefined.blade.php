<div class="modal fade custom-modal" id="undefinedModal" tabindex="-1" role="dialog" aria-labelledby="undefinedModal" aria-modal="true">
    <div class="modal-dialog modal-dialog-centered modal-size">
        <div class="modal-content">
            <div class="modal-body">
                <form action="/api/logout">
                    <div class="text-center d-flex flex-column">
                        <div class="my-3 flex-column text-center">
                            <p class="f-5">ທ່ານມີບັນຊີນີ້ບໍ?</p>
                        </div>
                        <div class="row">
                            <div class="col-6 d-flex align-items-stretch">
                                <button type="button" class="btn btn-primary-dark-logout w-100" onclick="Fx_call_login_modal()">ມີແລ້ວ</button>
                            </div>
                            <div class="col-6 d-flex align-items-stretch">
                                <button type="button" class="btn btn-dark w-100" data-dismiss="modal" aria-label="Close" onclick="Fx_call_regid_modal()">ຍັງ​ເທື່ອ</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function Fx_call_login_modal() {
        $('#undefinedModal').modal('hide');
        $('#loginModal').modal('show');
    }
    function Fx_call_regid_modal() {
        $('#undefinedModal').modal('hide');
        $('#register_modal').modal('show');
    }
</script>
