<!-- 4.registerModal -->
{{-- <div class="modal fade custom-modal" id="FourregisterModal" tabindex="-1" role="dialog"
    aria-labelledby="registerModalLabel" aria-hidden="true"> --}}
<div class="modal-dialog modal-dialog-centered modal-size">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
        </button>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg order-0">
                    <div class="text-center d-flex flex-column mb-lg-0 mb-5">
                        <div class="text-center d-flex flex-column">
                            <h3 class="custom-modal-title" id="exampleModalLabel">ເລືອກທະນາຄານ
                                {{-- <hr class="hr-border-glow"> --}}
                            </h3>
                            <div class="modal-step-register">
                                <div class="d-block">
                                    <div class="col-7 m-auto row px-0">
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto ">
                                                <div class="modal--step-box-inner text-center">
                                                    1
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-border">
                                            <hr>
                                        </div>
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto ">
                                                <div class="modal--step-box-inner text-center">
                                                    2
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-border">
                                            <hr>
                                        </div>
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto ">
                                                <div class="modal--step-box-inner text-center">
                                                    3
                                                </div>
                                            </div>
                                        </div>
                                        @if (!session('site_info')['otp_disabled'])
                                        <div class="modal-border">
                                            <hr>
                                        </div>
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto step-active">
                                                <div class="modal--step-box-inner text-center">
                                                    4
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div><!-- modal-step-register -->
                            <form id="register5">
                                {{-- <div class="text-center">
                                    <div class="list-bank d-flex flex-wrap justify-content-center px-4 pb-3">
                                        <div class="bank bk1 active" data-bank="bank-scb"></div>
                                        <div class="bank bk2" data-bank="bank-kbank"></div>
                                        <div class="bank bk3" data-bank="bank-bbl"></div>
                                        <div class="bank bk4" data-bank="bank-bay"></div>
                                        <div class="bank bk5" data-bank="bank-kk"></div>
                                        <div class="bank bk6" data-bank="bank-tmb"></div>
                                        <div class="bank bk7" data-bank="bank-uob"></div>
                                        <div class="bank bk8" data-bank="bank-ktb"></div>
                                        <div class="bank bk9" data-bank="bank-gsb"></div>
                                        <div class="bank bk10" data-bank="bank-sc"></div>
                                        <div class="bank bk11" data-bank="bank-cimb"></div>
                                        <div class="bank bk12" data-bank="bank-tbank"></div>
                                    </div>
                                </div> --}}
                                <div class="my-5">
                                    <img class="ic_bank-" src="{{secure_asset('img/ic_bank.png')}}" alt="">
                                </div>
                                <div class="mb-3 flex-column text-center position-relative regis_4">
                                    <i class="far fa-credit-card text-secondary"></i>
                                    <input type="text" id="AddBankInput" name="AddBankInput" pattern="^[0-9]{18}"
                                        class="custom-form-control form-control pl-38" placeholder="ໃສ່ເລກບັນຊີ" required>
                                </div>
                                <div class="mb-3 flex-column text-center position-relative regis_4">
                                    <i class="fas fa-user-circle text-secondary"></i>
                                    <input type="text" id="AddNamebank" name="AddNamebank"
                                        class="custom-form-control form-control pl-38" placeholder="ໃສ່ຊື່ - ນາມສະກຸນ"
                                        required>
                                </div>
                                <div class="text-center mt-4">
                                    <button type="submit" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
                                        ຢືນຢັນ
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- modal-content -->
</div>
{{-- </div> --}}

<script>
    $(".bank").on('click', function (e) {
        e.preventDefault();

        $(".bank.active").toggleClass("active");
        $(e.target).toggleClass("active");
    });

    $("#register5").off('submit')
    $("#register5").on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "/_ajax/register_user",
            data: {
                bank: {
                    code: $(".bank.active").attr('data-bank'),
                    name: $("#AddNamebank").val(),
                    number: $("#AddBankInput").val()
                }
            },
            success: function (response) {
                    $("#register_modal").html(response);
            },
            error: function (error) {
                error_alert(error.responseText);
            }
        });
    })
</script>
