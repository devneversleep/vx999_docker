<div class="text-center d-flex flex-column">
    <h3 class="custom-modal-title mb-4">
        SMS OTP ຢືນຢັນ
    </h3>
    <div class="-x-input-icon mb-3 flex-column text-center">
        <img src="img/ic_mini_password.png" class="-icon" alt="login" width="12">
        <input type="text" id="reset_password_otp" name="reset_password_otp" required
            pattern="^[0-9]{4}" class="custom-form-control form-control" placeholder="ໃສ່ລະຫັດ OTP">
    </div>
    <div class="text-center mt-2">
        <button id="btnSubmit" type="submit" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
            ຢືນຢັນ
        </button>
    </div>
    <hr class="hr-border-glow w-100 my-4" />
    <div class="text-center modal-contact">
        <span>ພົບປັນຫາ</span>
        <a href="{{config('variable.line')}}" class="link-message" target="_blank">
            <span>ຕິດຕໍ່ບໍລິການລູກຄ້າ</span>
        </a>
    </div>
</div>

<script>
    $('#reset_password_form').off('submit');
    $("#reset_password_form").on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: 'api/verify_otp',
            type: 'POST',
            datatype: 'html',
            data: {
                otp: $(
                    "input[name='reset_password_otp']"
                ).val(),
            },
            success: function (response) {
                console.log(response);
                $('#resetpassword').modal('hide');
                $('#ThreeregisterModal').modal('show');
            },
            error: function (error){
                error_alert(error.responseText);
            }
        });
    });
</script>
