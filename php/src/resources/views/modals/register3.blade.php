<!-- 3.registerModal -->
{{-- <div class="modal fade custom-modal" id="ThreeregisterModal" tabindex="-1" role="dialog"
    aria-labelledby="registerModalLabel" aria-hidden="true"> --}}
<div class="modal-dialog modal-dialog-centered modal-size">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
        </button>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg order-0">
                    <div class="text-center d-flex flex-column mb-lg-0 mb-5">
                        <div class="text-center d-flex flex-column">
                            <h3 class="custom-modal-title" id="exampleModalLabel">ສະໜັກສະມາຊິກ
                                {{-- <hr class="hr-border-glow"> --}}
                            </h3>
                            <div class="modal-step-register">
                                <div class="d-block">
                                    <div class="col-7 m-auto row px-0">
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto ">
                                                <div class="modal--step-box-inner text-center">
                                                    1
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-border">
                                            <hr>
                                        </div>
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto ">
                                                <div class="modal--step-box-inner text-center">
                                                    2
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-border">
                                            <hr>
                                        </div>
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto @if (!session('site_info')['otp_disabled']){{'step-active'}}@endif">
                                                <div class="modal--step-box-inner text-center">
                                                    3
                                                </div>
                                            </div>
                                        </div>
                                        @if (!session('site_info')['otp_disabled'])
                                        <div class="modal-border">
                                            <hr>
                                        </div>
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto ">
                                                <div class="modal--step-box-inner text-center">
                                                    4
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div><!-- modal-step-register -->
                            <form id="register3">
                                <div class="text-center">
                                    <img src="{{secure_asset('img/ic_set_password.png')}}" alt="LAOCASINO ລົງທະບຽນ" class="img-fluid  my-3"
                                        width="100">
                                </div>
                                <div class="-x-input-icon mb-3 flex-column text-center">
                                    <img src="{{secure_asset('img/ic_mini_password.png')}}" class="-icon" alt="login" width="12">
                                    <input type="password" id="register_password" name="registerPassword_one"
                                        class="custom-form-control form-control" placeholder="ຕັ້ງລະຫັດຜ່ານ"
                                        pattern=".{6,}" required>
                                    <div class="invalid-feedback ">ຢ່າງໜ້ອຍ 6  ໂຕອັກສອນ (a-z, A-Z, 0-9)</div>
                                </div>
                                <div class="-x-input-icon mb-3 flex-column text-center">
                                    <img src="{{secure_asset('img/ic_mini_password.png')}}" class="-icon" alt="login" width="12">
                                    <input type="password" id="register_confirm_password" name="registerPassword_two"
                                        class="custom-form-control form-control bg-white" placeholder="ພິມລະຫັດຜ່ານອີກເທື່ອ"
                                        pattern=".{6,}" required>
                                    <div class="invalid-feedback mt-2">ລະຫັດຜ່ານບໍ່ກົງກັນ</div>
                                </div>
                                <div class="mb-3">
                                    <span class=" text-yellow">ຢ່າງໜ້ອຍ 6  ໂຕອັກສອນ (a-z, A-Z, 0-9)</span>
                                </div>
                                <button type="submit" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
                                    ຢືນຢັນ
                                </button>
                            </form>
                            <hr class="hr-border-glow w-100 mt-4">
                            <div class="text-center modal-contact no-fixed">
                                <span>ພົບປັນຫາ</span>
                                <a href="{{config('variable.line')}}" class="link-message " target="_blank">
                                    <span>ຕິດຕໍ່ບໍລິການລູກຄ້າ</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- </div> --}}

<script>
    $("#register_confirm_password").keypress(function (e) {
        if ($(e.target).hasClass('is-invalid')) $(e.target).removeClass('is-invalid')
    })
    $("#register3").off('submit')
    $("#register3").on('submit', function (e) {
        e.preventDefault();

        if ($("#register_password").val() == $("#register_confirm_password").val())
        {
            $.ajax({
                type: "post",
                url: "/_ajax/register_password",
                data: {
                    register_password: $("#register_password").val()
                },
                success: function (response) {
                    $("#register_modal").html(response);
                },
                error: function (error) {
                    error_alert(error.responseText);
                }
            });
        } else {
            $("#register_confirm_password").addClass('is-invalid')
        }
    })
</script>
