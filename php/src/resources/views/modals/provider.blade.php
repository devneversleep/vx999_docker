<!-- account provider Money Modal -->
<div class="modal fade custom-modal" id="AccountProviderMoneyModal" tabindex="-1" role="dialog"
    aria-labelledby="AccountProviderMoneyModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-size modal-big -new">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            <div class="modal-body">
                <div class="row account-profile">
                    <div class="col order-1">
                        <div>
                            <h3 class="custom-modal-title mb-3">
                                ລະຫັດເຂົ້າຫຼີ້ນ
                                <hr class="hr-border-glow" />
                            </h3>
                            {{-- ตรงนี้ครับ --}}
                            <div class="wrapper-inner scroll">
                                <div class="m-auto text-center">
                                    <div class="d-flex row mb-5 fix-row">
                                        @foreach ($accountinfo["provider_users"]
                                        as $obj)
                                        {{-- <div>
                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/{{
                                                $obj['provide_code']
                                            }}-logo-square.png" alt="{{ $obj['provide_code'] }}" /> 
                                        </div> --}}
                                        <div
                                            class="-info-wrapper d-flex justify-content-between mb-2 col-12 fix-padding">
                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/{{
                                                    $obj['provide_code']
                                                }}-logo-square.png" alt="{{ $obj['provide_code'] }}" />
                                            <div class="d-flex flex-column mt-sm-1 mt-2 ml-2 ml-md-3">
                                                <div class="d-flex">
                                                    <div class="text-muted">
                                                        Username :
                                                        
                                                    </div>
                                                </div>
                                                <div class="d-flex">
                                                    <div class="text-muted">
                                                        Password :
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="d-flex flex-column mt-1 ml-4">
                                                <div class="text-muted" style="text-align: left;">
                                                    {{ $obj["username"] }}
                                                </div>
                                                <div class="text-muted" style="text-align: left;">
                                                    {{ $obj["password"] }}
                                                </div>
                                            </div>
                                            <div class="d-flex flex-column mt-1 mr-2 ml-auto">
                                                <div class="-copy-btn-wrapper text-right">
                                                    <a href="#" class="btn-copy text-yellow" style="
                                                            white-space: nowrap;
                                                            font-size: 0.8rem;
                                                        " data-clipboard-text="{{
                                                            $obj['username']
                                                        }}">
                                                        <i class="far fa-copy"></i>
                                                    </a>
                                                </div>
                                                <div class="-copy-btn-wrapper">
                                                    <a href="#" class="btn-copy text-yellow" style="
                                                            white-space: nowrap;
                                                            font-size: 0.8rem;
                                                        " data-clipboard-text="{{
                                                            $obj['password']
                                                        }}">
                                                        <i class="far fa-copy"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <!-- wrapper-inner -->
                        </div>
                    </div>
                    <div class="col-lg order-lg-2 order-0">
                        <div class="text-center d-flex flex-column mb-lg-0 mb-5">
                            <h3 class="custom-modal-title mb-3">
                                ຂໍ້ມູນສ່ວນໂຕ
                                <hr class="hr-border-glow" />
                            </h3>
                            <div class="text-center">
                                <img src="{{
                                        asset('img/ic_profile.png')
                                    }}" alt="LAOCASINO icon-profile" class="img-fluid my-3" width="100" />
                            </div>
                            <div class="mb-3 flex-column text-center">
                                <div class="text-yellow">
                                    {{ Session::get('phone_number') }}
                                </div>
                                <a href="#0" class="text-white-50" data-toggle="collapse"
                                    data-target="#change-password-collapse"><u>ປ່ຽນ​ລະ​ຫັດ​ຜ່ານ</u></a>
                            </div>
                            <div class="collapse" id="change-password-collapse">
                                <form action="javascript:Fx_changpassword()">
                                    <div class="mb-3 flex-column text-center">
                                        <input type="password" id="request_Password1" name="request_Password1"
                                            class="custom-form-control form-control" placeholder="ລະຫັດຜ່ານປະຈຸບັນ"
                                            required />
                                    </div>
                                    <div class="mb-3 flex-column text-center">
                                        <input type="password" id="request_Password2" name="request_Password2"
                                            class="custom-form-control form-control" placeholder="ລະຫັດຜ່ານໃຫມ່"
                                            required />
                                    </div>
                                    <div class="mb-3 flex-column text-center">
                                        <input type="password" id="request_Password3" name="request_Password3"
                                            class="custom-form-control form-control" placeholder="ພິມລະຫັດຜ່ານໃຫມ່ອີກເທື່ອ"
                                            required />
                                    </div>
                                    <div class="text-center mb-3">
                                        <button type="submit" class="btn btn-primary-modal d-block w-100 btn-lg">
                                            ຢືນຢັນ
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="mb-2 py-2 d-flex justify-content-center bank-info">
                                <div class="media pr-2">
                                    <div class="footer-bank-logo m-auto">
                                        <div class="container p-0 pr-1">
                                            {{-- <div class="wrapper">
                                                @switch($accountinfo['bank_account']['bank_code'])
                                                @case("bank-bbl")
                                                <div class="ic ic-3"
                                                    style="filter: grayscale(0%); background: url({{secure_asset('img/ic-bank-logo.png')}})">
                                                </div>
                                                @break
                                                @case("bank-bay")
                                                <div class="ic ic-4"
                                                    style="filter: grayscale(0%); background: url({{secure_asset('img/ic-bank-logo.png')}})">
                                                </div>
                                                @break
                                                @case("bank-kbank")
                                                <div class="ic ic-2"
                                                    style="filter: grayscale(0%); background: url({{secure_asset('img/ic-bank-logo.png')}})">
                                                </div>
                                                @break
                                                @case("bank-kk")
                                                <div class="ic ic-5"
                                                    style="filter: grayscale(0%); background: url({{secure_asset('img/ic-bank-logo.png')}})">
                                                </div>
                                                @break
                                                @case("bank-cimb")
                                                <div class="ic ic-11"
                                                    style="filter: grayscale(0%); background: url({{secure_asset('img/ic-bank-logo.png')}})">
                                                </div>
                                                @break
                                                @case("bank-tmb")
                                                <div class="ic ic-6"
                                                    style="filter: grayscale(0%); background: url({{secure_asset('img/ic-bank-logo.png')}})">
                                                </div>
                                                @break
                                                @case("bank-scb")
                                                <div class="ic ic-1"
                                                    style="filter: grayscale(0%); background: url({{secure_asset('img/ic-bank-logo.png')}})">
                                                </div>
                                                @break
                                                @case("bank-tbank")
                                                <div class="ic ic-12"
                                                    style="filter: grayscale(0%); background: url({{secure_asset('img/ic-bank-logo.png')}})">
                                                </div>
                                                @break
                                                @case("bank-uob")
                                                <div class="ic ic-7"
                                                    style="filter: grayscale(0%); background: url({{secure_asset('img/ic-bank-logo.png')}})">
                                                </div>
                                                @break
                                                @case("bank-ktb")
                                                <div class="ic ic-8"
                                                    style="filter: grayscale(0%); background: url({{secure_asset('img/ic-bank-logo.png')}})">
                                                </div>
                                                @break
                                                @case("bank-gsb")
                                                <div class="ic ic-9"
                                                    style="filter: grayscale(0%); background: url({{secure_asset('img/ic-bank-logo.png')}})">
                                                </div>
                                                @break
                                                @case("bank-sc")
                                                <div class="ic ic-10"
                                                    style="filter: grayscale(0%); background: url({{secure_asset('img/ic-bank-logo.png')}})">
                                                </div>
                                                @break
                                                @case("bank-lh")
                                                @break
                                                @case("bank-i")
                                                @break
                                                @endswitch
                                            </div> --}}
                                            <div class="">
                                                <img class="ic_bank_deposit" src="{{secure_asset('img/ic_bank.png')}}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    {{--
                                    <img
                                        src="images/bank/kbank.svg"
                                        class="align-self-start mr-3"
                                        alt="..."
                                        width="60"
                                    />
                                    --}}
                                    <div class="media-body pt-1">
                                        <h5 class="my-0 text-yellow">
                                            @php $new_number = ""; 
                                            if (substr($accountinfo['bank_account']['number'], 0, 3) == '888') {
                                                $bank_number = substr($accountinfo['bank_account']['number'], 3);
                                            } else {
                                                $bank_number = $accountinfo['bank_account']['number'];
                                            }
                                            $new_number = substr($bank_number, 0, 3); 
                                            if (strlen($bank_number) > 3) { 
                                                $new_number .= "-" . substr($bank_number, 3, 1); 
                                            } 
                                            if (strlen($bank_number)
                                            > 4) { $new_number .= "-" .
                                            substr($bank_number,
                                            4, 5); } if
                                            (strlen($bank_number)
                                            > 9) { $new_number .= "-" .
                                            substr($bank_number,
                                            9); } echo $new_number; @endphp
                                            {{-- {{$bank_number}}
                                            --}}
                                        </h5>
                                        <p class="mb-0">
                                            @if (substr($accountinfo['bank_account']['number'], 0, 3) == '888')
                                                {{ substr($accountinfo['bank_account']['name'], 1) }}
                                            @else
                                                {{ $accountinfo["bank_account"]["name"] }}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <hr class="hr-border-glow mt-4" />
                            <div class="text-center modal-contact no-fixed">
                                {{-- <span>*ต้องการเปลี่ยนบัญชี กรุณา </span> --}}
                                <span>ພົບປັນຫາ  </span>
                                <a href="{{ config('variable.line') }}" class="link-message" target="_blank">
                                    <span>ຕິດຕໍ່ບໍລິການລູກຄ້າ</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(".btn-copy").on("click", function (e) {
        $(this).html("<i class='fas fa-copy'></i>");
        setTimeout(function () {
            console.log(e);
            $(e.currentTarget).html("<i class='far fa-copy'></i>");
            console.log("copy");
        }, 2000);
        copyToClipboard(e.currentTarget);
    });

    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(element.getAttribute("data-clipboard-text")).select();
        document.execCommand("copy");
        navigator.clipboard.writeText($temp.val());
        $temp.remove();
    }

    function Fx_changpassword() {
        var old_pass = $("#request_Password1").val();
        var new_pass = $("#request_Password2").val();
        var confirem_pass = $("#request_Password3").val();

        if (new_pass != confirem_pass) {
            Swal.fire({
                icon: "warning",
                title: "ລະຫັດ ໃໝ່ ບໍ່ກົງກັນ",
                // confirmButtonText: "ຕົກ​ລົງ"
                customClass: {
                    container: "custom-modal",
                    popup: "modal-content-alert",
                    content: "swal-text-content",
                    
                },
            });
        } else {
            $.ajax({
                url: "/api/change_password",
                type: "PUT",
                data: {
                    old_password: old_pass,
                    new_password: new_pass,
                },
                success: function (response) {
                    if (response["code"] == 200) {
                        Swal.fire({
                            icon: "success",
                            title: "ລະຫັດຜ່ານຖືກປ່ຽນຢ່າງ ສຳ ເລັດຜົນ",
                            showConfirmButton: false,
                            customClass: {
                                container: "custom-modal",
                                popup: "modal-content-alert",
                                content: "swal-text-content",
                            },
                            timer: 2000,
                        });
                    } else {
                        Swal.fire({
                            icon: "error",
                            title: "ລະຫັດຜ່ານຂອງທ່ານບໍ່ຖືກຕ້ອງ",
                            customClass: {
                                container: "custom-modal",
                                popup: "modal-content-alert",
                                content: "swal-text-content",
                            },
                        });
                    }
                    console.log(response);
                    $("#request_Password1").val("");
                    $("#request_Password2").val("");
                    $("#request_Password3").val("");
                    $("#change-password-collapse").collapse("hide");
                },
                error: function (error) {
                    Swal.fire({
                        icon: "error",
                        title: error.responseText,
                        customClass: {
                            container: "custom-modal",
                            popup: "modal-content-alert",
                            content: "swal-text-content",
                        },
                    });
                },
            });
            // }
            // })
        }
    }
</script>
