<div class="modal-dialog modal-dialog-centered modal-size">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
        </button>
        <div class="modal-body">
            <div class="row">
                <div class="col order-1 slide-left hide">
                    <div class="wrapper-left">
                        <h3 class="custom-modal-title">Term and condition
                            <hr class="hr-border-glow">
                        </h3>
                        <div class="wrapper-inner scroll">
                        </div>
                    </div>
                </div>
                <div class="col-lg order-lg-2 order-0">
                    <div class="text-center d-flex flex-column mb-lg-0 mb-5">
                        <div class="text-center d-flex flex-column">
                            <h3 class="custom-modal-title" id="exampleModalLabel">ສະໜັກສະມາຊິກ
                                {{-- <hr class="hr-border-glow"> --}}
                            </h3>
                            <div class="modal-step-register">
                                <div class="d-block">
                                    <div class="col-7 m-auto row px-0">
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto step-active">
                                                <div class="modal--step-box-inner text-center">
                                                    1
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-border">
                                            <hr>
                                        </div>
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto ">
                                                <div class="modal--step-box-inner text-center">
                                                    2
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-border">
                                            <hr>
                                        </div>
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto ">
                                                <div class="modal--step-box-inner text-center">
                                                    3
                                                </div>
                                            </div>
                                        </div>
                                        @if (!session('site_info')['otp_disabled'])
                                        <div class="modal-border">
                                            <hr>
                                        </div>
                                        <div class="col-auto px-0 my-auto">
                                            <div class="modal-step-box-outer m-auto ">
                                                <div class="modal--step-box-inner text-center">
                                                    4
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div><!-- modal-step-register -->
                            <form id="register1">
                                <div class="text-center">
                                    <img src="{{secure_asset('img/ic_phone.png')}}" alt="LAOCASINO ລົງທະບຽນ"
                                        class="img-fluid  my-3" width="100">
                                </div>
                                {{-- <div class="-x-input-icon mb-3 flex-column text-center">
                                    

                                    <div class="input-group -new ">
                                        <div class="input-group-text">
                                            <img src="{{secure_asset('img/ic_mini_LAK.png')}}" class="ic-lak" alt="login" >
                                            <span class="f-5 text-black ml-1">+856</span>
                                        </div>
                                        <input type="tel" id="register_phoneNumber" name="register[phoneNumber]"
                                        required="required" class="form-control -new" placeholder="ເບີ​ໂທລະ​ສັບ" >
                                    </div>
                                </div> --}}
                                <div class="input-group mb-3">
                                    
                                    <div class="dropdown"> 
                                        <button class="btn btn-success-  
                                                dropdown-toggle h-100 position-relative" type="button" 
                                                data-toggle="dropdown"
                                                aria-haspopup="true" 
                                                aria-expanded="false"> 
                                                <img class="flag-thai d-none" src="{{secure_asset('img/ธง/1x/ic_mini_thai.png')}}" width="35" > 
                                                <img class="flag-lao" src="{{secure_asset('img/ธง/1x/ic_mini_lao.png')}}" width="35" > 
                                        </button> 
                              
                                        <ul class="dropdown-menu" 
                                            aria-labelledby="dropdownMenuButton"> 
                                            <li class="dropdown-item select-lao"> 
                                                <img class="mr-2" src="{{secure_asset('img/ธง/1x/ic_mini_lao.png')}}" width="35" >        
                                                laos (ປະເທດລາວ)   
                                                <span class="float-right">+856</span>                                     
                                            </li>
                                            <li class="dropdown-item select-thai"> 
                                                <img class="mr-2" src="{{secure_asset('img/ธง/1x/ic_mini_thai.png')}}" width="35" > 
                                                Thailand (ປະເທດໄທ) 
                                                <span class="float-right">+66</span>                                     
                                            </li>        
                                        </ul> 
                                    </div>
                                    <input type="hidden" id="register_phone_country" name="register_phone_country" value="11">
                                    <input type="tel" id="register_phoneNumber" name="register[phoneNumber]" required="required" class="custom-form-control form-control" placeholder="ເບີ​ໂທລະ​ສັບ" >
                                    {{-- <input type="tel" id="login_phone_number" name="phone_number" required class="custom-form-control form-control" placeholder="ເບີ​ໂທລະ​ສັບ" > --}}
                                </div>
                                <div class="d-flex flex-column m-auto -term-and-condition-check-box ">
                                    <div class="x-checkbox-primary d-flex justify-content-center ">
                                        <div class="form-check">
                                            <input type="checkbox" id="register_termAndCondition"
                                                name="register[termAndCondition]"
                                                class="x-form-control js-term-check-box form-check-input">
                                            <label class="form-check-label" for="register_termAndCondition"></label>
                                        </div>
                                        <span class="x-text-with-link-component">
                                            <label class="-text-message mt-1"
                                                for="register_termAndCondition">ຍອມຮັບເງື່ອນໄຂ </label>
                                            <a href="#" class="link-message btn-left">
                                                <span>Term and condition</span>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                                <div class="text-center mt-3">
                                    <button id="register1submit" type="submit"
                                        class="btn btn-primary-modal d-block w-100 btn-lg btn-submit" disabled>
                                        ຢືນຢັນ
                                    </button>
                                </div>
                            </form>
                            <hr class="hr-border-glow w-100 mt-4">
                            <div class="text-center modal-contact no-fixed">
                                <span>ພົບປັນຫາ </span>
                                <a href="{{config('variable.line')}}" class="link-message " target="_blank">
                                    <span> ຕິດຕໍ່ບໍລິການລູກຄ້າ</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#register_termAndCondition").click(function (e) {
        switch (e.target.checked) {
            case true:
                $("#register1submit").prop('disabled', false)
                break
            case false:
                $("#register1submit").prop('disabled', true)
                break
        }
    })

    $("#register1").off('submit')
    $("#register1").on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            type: "post",
            url: "/_ajax/register_phone",
            data: {
                register_phone_number: $("#register_phoneNumber").val(),
                phone_country: $("#register_phone_country").val(),
            },
            success: function (response) {
                $("#register_modal").html(response);
            },
            error: function (error) {
                console.log(error);
                error_alert(error.responseText);
            }
        });
    })
    $('.select-thai').click(function(){
        $('.flag-lao').addClass('d-none');
        $('.flag-thai').removeClass('d-none');
        $('#register_phone_country').val(10)
        $('#phone_country').val(10)
    });
    $('.select-lao').click(function(){
        $('.flag-lao').removeClass('d-none');
        $('.flag-thai').addClass('d-none');
        $('#register_phone_country').val(11)
        $('#phone_country').val(11)
    });
</script>
