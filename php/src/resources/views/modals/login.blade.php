<div class="modal fade custom-modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-size">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            <div class="modal-body">
                <form action="/api/login" method="POST">
                    <div class="text-center d-flex flex-column">
                        <h3 class="custom-modal-title mb-4">ເຂົ້າ​ສູ່​ລະ​ບົບ
                            {{-- <hr class="hr-border-glow"> --}}
                        </h3>
                        {{-- <div class="-x-input-icon mb-3 flex-column text-center">
                            <img src="{{secure_asset('img/ic_mini_phone.png')}}" class="-icon" alt="login" width="12">
                            <input type="tel" id="login_phone_number" name="phone_number" required
                                class="custom-form-control form-control" placeholder="ເບີ​ໂທລະ​ສັບ">
                        </div> --}}


                        {{-- <div class="input-group -new mb-3">
                            <div class="input-group-text">
                                <img src="{{secure_asset('img/ic_mini_LAK.png')}}" class="ic-lak" alt="login" >
                                <span class="f-5 text-black ml-1">+856</span>
                            </div>
                            <input type="tel" id="login_phone_number" name="phone_number"
                            required class="form-control -new" placeholder="ເບີ​ໂທລະ​ສັບ" >
                        </div> --}}

                        <div class="input-group mb-3">
                            {{-- <div class="input-group-prepend">
                                <select class="custom-select btn btn-success h-auto" id="inputGroupSelect01">
                                    <option selected style="background-image:url('img/ธง/1x/ic_mini_lao.png');">1111</option>
                                    <option value="1" style="background-image:url('img/ธง/1x/ic_mini_thai.png');">2222</option>
                                    <option value="2" style="background-image:url('img/ธง/1x/ic_mini_lao.png');">3333</option>
                                </select>                          
                            </div> --}}
                            <div class="dropdown"> 
                                <button class="btn btn-success-  
                                        dropdown-toggle h-100 position-relative" type="button" 
                                        data-toggle="dropdown"
                                        aria-haspopup="true" 
                                        aria-expanded="false"> 
                                        <img class="flag-thai d-none" src="{{secure_asset('img/ธง/1x/ic_mini_thai.png')}}" width="35" > 
                                        <img class="flag-lao" src="{{secure_asset('img/ธง/1x/ic_mini_lao.png')}}" width="35" > 
                                </button> 
                      
                                <ul class="dropdown-menu"
                                    aria-labelledby="dropdownMenuButton"> 
                                    <li class="dropdown-item select-lao"> 
                                        <img class="mr-2" src="{{secure_asset('img/ธง/1x/ic_mini_lao.png')}}" width="35" >        
                                        laos (ປະເທດລາວ)   
                                        <span class="float-right">+856</span>                                     
                                    </li> 
                                    <li class="dropdown-item select-thai"> 
                                        <img class="mr-2" src="{{secure_asset('img/ธง/1x/ic_mini_thai.png')}}" width="35" > 
                                        Thailand (ປະເທດໄທ) 
                                        <span class="float-right">+66</span>                                     
                                    </li>                                   
                                </ul> 

                                <input type="hidden" id="phone_country" name="phone_country" value="11">
                            </div>

                            <input type="tel" id="login_phone_number" name="phone_number" required class="custom-form-control form-control" placeholder="ເບີ​ໂທລະ​ສັບ" >
                            {{-- <input type="text" class="form-control" aria-label="Text input with dropdown button"> --}}
                        </div>


                        <div class="-x-input-icon mb-3 flex-column text-center">
                            {{-- <img src="{{secure_asset('img/ic_mini_password.png')}}" class="-icon" alt="login" width="12"> --}}
                            <i class="fas fa-key"></i>
                            <input type="password" id="login_password" required name="password"
                                class="custom-form-control form-control" placeholder="ລະຫັດຜ່ານ">
                        </div>
                        <div class="text-right">
                            <a href="{{config('variable.line')}}" target="_blank"
                                class="text-red"><u>ລືມລະຫັດຜ່ານ</u></a>
                        </div>
                        @if (Request::get('_p')) <input type="hidden" id="_p" name="_p" value="{{ Request::get('_p') }}"> @endif
                        <div class="text-center mt-3">
                            <button type="submit" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
                                ເຂົ້າ​ສູ່​ລະ​ບົບ
                            </button>
                        </div>
                        <hr class="hr-border-glow w-100 my-4">
                        <div class="text-center modal-contact">
                            <span>ພົບປັນຫາ</span>
                            <a href="{{config('variable.line')}}" class="link-message " target="_blank">
                                <span> ຕິດຕໍ່ບໍລິການລູກຄ້າ</span></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('.select-thai').click(function(){
        $('.flag-lao').addClass('d-none');
        $('.flag-thai').removeClass('d-none');
        $('#phone_country').val(10)
        $('#register_phone_country').val(10)
    });
    $('.select-lao').click(function(){
        $('.flag-lao').removeClass('d-none');
        $('.flag-thai').addClass('d-none');
        $('#phone_country').val(11)
        $('#register_phone_country').val(11)
    });
</script>