<div class="modal fade custom-modal" id="forgotModal" tabindex="-1" role="dialog" aria-labelledby="forgotModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-size">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            <div class="modal-body">
                <form action="javascript:Fx_forgotpassword()">
                    <div class="text-center d-flex flex-column mb-lg-0 mb-5">
                        <div class="text-center d-flex flex-column">
                            <h3 class="custom-modal-title" id="exampleModalLabel">ລືມລະຫັດຜ່ານ
                                {{-- <hr class="hr-border-glow"> --}}
                            </h3>
                            <div class="text-center">
                                <img src="{{secure_asset('img/ic_forget_password.png')}}" alt="LAOCASINO ລືມລະຫັດຜ່ານ" class="img-fluid  my-3"
                                    width="100">
                            </div>
                            @csrf
                            {{-- <div class="-x-input-icon mb-3 flex-row text-center">
                                <img src="{{secure_asset('img/ic_mini_phone.png')}}" class="-icon" alt="login"
                                    width="12">
                                <input type="tel" id="request_phoneNumber" name="request_[phoneNumber]"
                                    required="required" pattern="[0-9]*" class="custom-form-control form-control"
                                    placeholder="ເບີ​ໂທລະ​ສັບທີ່ເຄີຍລົງທະບຽນ">
                            </div> --}}
                            <div class="input-group mb-3">
                                <div class="dropdown"> 
                                    <button class="btn btn-success-  
                                            dropdown-toggle h-100 position-relative" type="button" 
                                            data-toggle="dropdown"
                                            aria-haspopup="true" 
                                            aria-expanded="false"> 
                                            <img class="flag-thai d-none" src="{{secure_asset('img/ธง/1x/ic_mini_thai.png')}}" width="35" > 
                                            <img class="flag-lao" src="{{secure_asset('img/ธง/1x/ic_mini_lao.png')}}" width="35" > 
                                    </button> 
                                    <ul class="dropdown-menu"
                                        aria-labelledby="dropdownMenuButton"> 
                                        <li class="dropdown-item select-lao"> 
                                            <img class="mr-2" src="{{secure_asset('img/ธง/1x/ic_mini_lao.png')}}" width="35" >        
                                            laos (ປະເທດລາວ)   
                                            <span class="float-right">+856</span>                                     
                                        </li> 
                                        <li class="dropdown-item select-thai"> 
                                            <img class="mr-2" src="{{secure_asset('img/ธง/1x/ic_mini_thai.png')}}" width="35" > 
                                            Thailand (ປະເທດໄທ) 
                                            <span class="float-right">+66</span>                                     
                                        </li>                                   
                                    </ul> 
                                    {{-- <input type="hidden" id="phone_country" name="phone_country" value="11"> --}}
                                </div>
                                <input type="tel" id="request_phoneNumber" name="request_[phoneNumber]"
                                    required="required" pattern="[0-9]*" class="custom-form-control form-control"
                                    placeholder="ເບີ​ໂທລະ​ສັບທີ່ເຄີຍລົງທະບຽນ">
                            </div>
                            <div class="text-center mt-3">
                                <button type="submit" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
                                    ຢືນຢັນ
                                </button>
                            </div>
                            <hr class="hr-border-glow w-100 my-4">
                            <div class="text-center modal-contact no-fixed">
                                <span>ພົບປັນຫາ </span>
                                <a href="{{config('variable.line')}}" class="link-message " target="_blank">
                                    <span>ຕິດຕໍ່ບໍລິການລູກຄ້າ</span></a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-content -->
    </div>
</div>


<div class="modal fade custom-modal" id="resetpassword" tabindex="-1" role="dialog"
    aria-labelledby="resetpasswordModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-size" id="modals_deposit">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            <div class="modal-body" id="registerModalBody">
                <form id="reset_password_form">
                    @include('modals.reset_password_otp')
                </form>
            </div>
        </div>
    </div>
</div>

<!-- setNewPasswordModal -->
<div class="modal fade custom-modal" id="ThreeregisterModal" tabindex="-1" role="dialog"
    aria-labelledby="registerModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-size">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            <form id="set_new_password">
                @include('modals.set_new_password')
            </form>
        </div><!-- modal-content -->
    </div>
</div>
