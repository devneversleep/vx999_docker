<!-- with draw Modal -->
<div class="modal fade custom-modal" id="withdrawModal" tabindex="-1" role="dialog" aria-labelledby="withdrawModal"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-size">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            <div class="modal-body">
                <form id="withdraw_form">
                    <div class="text-center d-flex flex-column">
                        <h3 class="custom-modal-title " id="exampleModalLabel">ຖອນເງິນ
                        </h3>
                        <div class="text-center">
                            <img src="{{secure_asset('img/ic_chip_withdraw.png')}}" alt="ຖອນເງິນ"
                                class="img-fluid my-3" width="120">
                        </div>
                        <div class="my-3 money-lak ">
                            <span class="text-white">1 THB = {{ $exchange_rate->thai_1_bath_to_laos }} LAK </span>
                            <span class="text-secondary">( 1  ບາດ = {{ $exchange_rate->thai_1_bath_to_laos }}  ກີບ )</span>
                        </div>
                        {{-- <div class="my-3 flex-column text-center" >
                            <div class="input-container form-control" id="withdraw_amount_field">
                                <img src="{{secure_asset('img/ic_mini_withdraw.png')}}" alt="icon image" class="icon-modal">
                        <input type="number" step="0.01" required id="withdraw_amount" name="withdraw_amount"
                            class="custom-form-control" placeholder="ການຖອນຂັ້ນຕ່ ຳ ສຸດ 100">
                    </div>
            </div> --}}
            <div class="-x-input-icon mb-3  text-center" id="withdraw_amount_field">
                {{-- <img src="{{secure_asset('img/ic_mini_withdraw.png')}}" class="-icon" alt="login" width="12">
                <input type="number" step="0.01" name="withdraw_amount" required id="withdraw_amount"
                    class="custom-form-control form-control" placeholder="ຕື່ມຂໍ້ມູນໃສ່ ຈຳ ນວນເງິນຖອນຂັ້ນຕ່  1 ບາດ"> --}}

                <div class="mr-3">
                    <img class="img-mini-money" src="{{secure_asset('img/ic_mini_THB.png') }}" alt="">
                </div>
                <div class="w-100 position-relative mr-2">
                    <input type="number" id="withdraw_amount_1" step="0.01" name="withdraw_amount_1" required 
                    class="custom-form-control form-control input-thb" placeholder="ໃສ່ຈຳນວນເງິນເປັນບາດ"
                    onkeyup="calculate_amount_withdraw(1)" />
                    
                </div>
                <div class="text-money-thb">
                    <span>฿ </span>
                </div>
            </div>
            <div class="-x-input-icon mb-3 text-center">
                {{-- <img src="{{ secure_asset('img/ic_mini_withdraw.png') }}" class="-icon" alt="login" width="12" /> --}}
                <div class="mr-3">
                    <img class="img-mini-money" src="{{secure_asset('img/ic_mini_LAK.png') }}" alt="">
                </div>
                <div class="w-100 position-relative mr-2">
                    {{-- <input type="number"  step="0.01" name="withdraw_amount_0" required 
                    class="custom-form-control form-control input-lak" placeholder="ໃສ່ຈຳນວນເງິນເປັນກີບ"
                    onkeyup="" /> --}}
                    <div class="box-lak text-right px-3 ">
                        <span class="text-white" id="withdraw_amount_0">0.00</span>
                    </div>
                    
                </div>
                <div class="text-money-lak">
                    <span>₭ </span>
                </div>
            </div>
            
            <div class="text-center mt-2">
                <button type="submit" id="btn_withdraw" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
                    ຢືນຢັນ
                </button>
            </div>
        </div>
        </form>
        <hr class="hr-border-glow w-100 mt-4" />
        <div class="text-center modal-contact no-fixed ">
            <span>ພົບປັນຫາ</span>
            <a href="{{config('variable.line')}}" class="link-message" target="_blank">
                <span>ຕິດຕໍ່ບໍລິການລູກຄ້າ</span>
            </a>
        </div>
    </div>
</div>
</div>
</div>

<script>
    var withNo;
    var btn_withdraw;

    $(document).ready(function () {
        $("#withdraw_amount").attr('placeholder', "ຕື່ມຂໍ້ມູນໃສ່ ຈຳ ນວນເງິນຖອນຂັ້ນຕ່ ຳ " + min_withdraw + " ບາດ");
        $("#withdraw_amount").attr('min', min_withdraw);

        $.ajax({
            url: "/api/check_pending_withdraw",
            type: "GET"
        }).done(function (data) {
            console.log(data);
            if (data.bill.code == "200") {
                wait_withdraw()
            }
        }).fail(function (error) {
            console.log(error);
        });
    });

    function withdraw_again() {
        $("#withdrawModal").modal("hide");
        $("#withdraw_amount_field").show();
        btn_withdraw = document.getElementById("btn_withdraw");
        btn_withdraw.innerHTML = 'ຢືນຢັນ';
        btn_withdraw.disabled = false;
        document.getElementById("withdraw_amount").value = null
    }

    // function aa() {
    //     console.log(document.getElementById("withdraw_amount").value)
    //     document.getElementById("withdraw_amount").value = null
    // }

    function wait_withdraw() {
        $("#withdraw_amount_field").hide();
        btn_withdraw = document.getElementById("btn_withdraw");
        btn_withdraw.innerHTML =
            '<div class="lds-ring"><div></div><div></div><div></div><div></div></div> ການກວດກາ...';
        btn_withdraw.disabled = true;
    }

    $("#withdraw_form").off("submit");
    $("#withdraw_form").on("submit", function (e) {
        e.preventDefault();
        withdraw_amount = document.getElementById("withdraw_amount_1").value;
        //calculate_amount();
        //console.log(e, deposit_amount)
        $.ajax({
                url: "/api/withdraw",
                type: "POST",
                data: {
                    amount: withdraw_amount
                }
            })
            .done(function (data) {
                // console.log(data);
                if (data.code == "200") {
                    getBalance();
                    wait_withdraw();
                    withNo = data.number;
                } else {
                    var error_withdraw = "";
                    for (const [key, value] of Object.entries(data.error)) {
                            error_withdraw += '<div>' + `${value}` + '</div>';
                    }
                    if (error_withdraw == '<div>Not Sufficient</div>') {
                        error_alert('ຍອດເງິນຂອງທ່ານແມ່ນບໍ່ພຽງພໍ')
                    } else {
                        error_alert(error_withdraw)
                    }
                }
            })
            .fail(function (error) {
                console.log(error);
            });
    });

    function calculate_amount_withdraw(key)
    {
        var withdraw_amount = $('#withdraw_amount_1').val()
        // console.log(withdraw_amount, exchange_rate)
        if (key == 1) {
            $('#withdraw_amount_0').text((withdraw_amount * exchange_rate))
        }
    }

</script>
