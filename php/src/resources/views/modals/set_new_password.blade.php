<div class="modal-body">
    <div class="row">
        <div class="col-lg order-0">
            <div class="text-center d-flex flex-column mb-lg-0 mb-5">
                <div class="text-center d-flex flex-column">
                    <h3 class="custom-modal-title" id="exampleModalLabel">ໃສ່ລະຫັດຜ່ານຂອງທ່ານ
                        <hr class="hr-border-glow">
                    </h3>
                    <div class="text-center">
                        <img src="{{secure_asset('img/ic_step_phone.png')}}" alt="LAOCASINO ລະຫັດຜ່ານ" class="img-fluid  my-3"
                            width="100">
                    </div>
                    <div class="-x-input-icon mb-3 flex-column text-center">
                        <img src="{{secure_asset('img/ic_mini_password.png')}}" class="-icon" alt="login" width="12">
                        <input type="password" id="register_Password_one" name="registerPassword_one"
                            class="custom-form-control form-control" placeholder="ຕັ້ງລະຫັດຜ່ານ" pattern=".{6,}"
                            required>
                        <div class="invalid-feedback">ລະຫັດຜ່ານທີ່ນ້ອຍກວ່າ 6 ໂຕອັກສອນ</div>
                    </div>
                    <div class="-x-input-icon mb-3 flex-column text-center">
                        <img src="{{secure_asset('img/ic_mini_password.png')}}" class="-icon" alt="login" width="12">
                        <input type="password" id="register_Password_two" name="registerPassword_two"
                            class="custom-form-control form-control bg-white" placeholder="ພິມລະຫັດຜ່ານອີກຄັ້ງ" pattern=".{6,}"
                            required>
                        <div class="invalid-feedback mt-2">ລະຫັດຜ່ານບໍ່ກົງກັນ</div>
                    </div>
                    <button type="submit" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
                        ຢືນຢັນ
                    </button>
                    <hr class="hr-border-glow w-100 my-4">
                    <div class="text-center modal-contact no-fixed">
                        <span>ພົບປັນຫາ</span>
                        <a href="{{config('variable.line')}}" class="link-message " target="_blank">
                            <span>ຕິດຕໍ່ບໍລິການລູກຄ້າ</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $("#register_Password_two").keypress(function (e) {
        if ($(e.target).hasClass('is-invalid')) $(e.target).removeClass('is-invalid')
    })
    $("#ThreeregisterModal").off('submit')
    $("#ThreeregisterModal").on('submit', function (e) {
        e.preventDefault();
        if ($("#register_Password_one").val() == $("#register_Password_two").val()) {
            $.ajax({
                type: "PUT",
                url: "/api/reset_password",
                data: {
                    register_password: $("#register_Password_one").val()
                },
                success: function (response) {
                    error_alert('ລະຫັດຜ່ານຖືກປ່ຽນຢ່າງ ສຳ ເລັດຜົນ, ກະລຸນາເຂົ້າສູ່ລະບົບອີກຄັ້ງ.');
                    $('#ThreeregisterModal').modal('hide');
                },
                error: function (error) {
                    error_alert(error.responseText);
                }
            });
        } else {
            $("#register_Password_two").addClass('is-invalid')
        }
    })
</script>
