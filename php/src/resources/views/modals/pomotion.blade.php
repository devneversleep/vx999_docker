<!-- Pomotion Modal -->
<div class="modal fade custom-modal" id="PomotionModal" tabindex="-1" role="dialog" aria-labelledby="PomotionModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-size">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
          <div class="modal-body">
              <form>
                  <div class="text-center d-flex flex-column">
                      <h3 class="custom-modal-title mb-3">ການສົ່ງເສີມການເຂົ້າຮ່ວມ<hr class="hr-border-glow"></h3>
                        <div class="my-3 flex-column text-center" id="Acc_promotion">
                            <p>ທ່ານບໍ່ມີການສົ່ງເສີມການເຂົ້າຮ່ວມ</p>
                        </div>
                        <hr class="hr-border-glow">
                        <div class="text-center modal-contact no-fixed">
                            <span>*ເພື່ອປ່ຽນບັນຊີກະລຸນາ</span>
                            <a href="{{config('variable.line')}}" class="link-message " target="_blank">
                                 <span>ຕິດຕໍ່ບໍລິການລູກຄ້າ</span>
                            </a>
                        </div>
                  </div>
              </form>
          </div>
        </div>
      </div>
    </div>
    <script>
      var acc_promotion = {!! json_encode($accountinfo)!!};
      // console.log("acc_promotion : ",acc_promotion);
      if (acc_promotion['active_promotion']) {
          var percent = Math.floor((acc_promotion['active_promotion']['turnover_details']['current'] / acc_promotion['active_promotion']['turnover_details']['must_do']) * 100);
          document.getElementById("Acc_promotion").innerHTML = "<H4 style='color:#B39700;'>" + acc_promotion['active_promotion']['promotion_name'] + " </H4>( ເຂົ້າຮ່ວມ )<br><br>turnOver<br>" +  acc_promotion['active_promotion']['turnover_details']['current'] + " / " + acc_promotion['active_promotion']['turnover_details']['must_do'] + "<div class='progress'><div class='progress-bar' role='progressbar' style='width: " + percent.toString() + "%;' aria-valuenow='" + percent.toString() + "' aria-valuemin='0' aria-valuemax='100'>" + percent.toString() + "%</div></div><br> <p class='text-yellow'>ສະພາບ</p><br>" + acc_promotion['active_promotion']['promotion_content'];
      } else {
          document.getElementById("Acc_promotion").innerHTML = "<p>ບໍ່ພົບການສົ່ງເສີມການມີສ່ວນຮ່ວມ</p>";
      }
  </script>
