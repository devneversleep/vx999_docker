<!-- ยื่นยันการถอนเงิน -->
<div class="modal fade custom-modal" id="withdraw-confirm-Modal" tabindex="-1" role="dialog"
    aria-labelledby="withdraw-confirm-Modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-size">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            <div class="modal-body">
                <form>
                    <div class="text-center d-flex flex-column">
                        <h3 class="custom-modal-title mb-4" id="exampleModalLabel">ຖອນເງິນ
                            <hr class="hr-border-glow">
                        </h3>
                        <div class="text-center">
                            <img src="{{secure_asset('img/ic_chip_withdraw.png')}}" alt="ຖອນເງິນ" class="img-fluid  my-3"
                                width="100">
                        </div>
                        <div class="mb-2 py-2 d-flex justify-content-center">
                            <div class="media">
                                <img src="{{secure_asset('images/bank/kbank.svg')}}" class="align-self-start mr-3" alt="..."
                                    width="60">
                                <div class="media-body pt-1 text-left">
                                    <p class="mb-0">ສົມໄຊ ນາມສະກຸນ</p>
                                    <h5 class="my-0 text-yellow">123-4-5678</h5>
                                </div>
                            </div>
                        </div>
                        <div class="mb-2 py-2 f-2 text-center bank-info text-yellow">
                            100.00
                        </div>
                        <p>ລະບົບດັ່ງກ່າວຈະຖອນເງິນພາຍໃນ 15 ນາທີ</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
