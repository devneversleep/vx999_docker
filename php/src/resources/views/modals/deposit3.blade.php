<div class="row">
    <div id="tranfer_data" class="col order-0 slide-left hide">
        <div class="wrapper-left">
            <h3 class="custom-modal-title mb-3">ການໂອນຂໍ້ມູນ
                <hr class="hr-border-glow">
            </h3>
            <div class="zone-copy-bank">
                <div class="mb-3 d-flex align-items-center box-copy-bank">
                    <div class="media">
                        <div class="list-bank align-self-start mr-2">
                            {{-- <div id="bank_class" class="bank bk2" style="filter: grayscale(0%); -webkit-filter: grayscale(0%);"></div> --}}
                            <div class="">
                                <img class="ic_bank_deposit" src="{{secure_asset('img/ic_bank.png')}}" alt="">
                            </div>
                        </div>
                        <div class="media-body -new">
                            <div>
                                <div class="d-flex">
                                    <h5 id="bank_number" class="my-0 text-yellow-2">123-4-5678</h5>
                                    <div class="text_copy- ml-2">
                                        <button class="btn-copy-" type="button">
                                            <i class="fas fa-copy text-red"></i> <span class="text-yellow-2"></span>
                                        </button>
                                    </div>
                                </div>
                                <p id="bank_name" class="mb-0">ສົມໄຊ ນາມສະກຸນ</p>
                            </div>
                            
                        </div>
                    </div>
                    
                    
                </div>
                <div>
                    <hr class="w-100 my-4 border-dark">
                </div>
            </div>
            
        </div>
        
    </div>
    <div class="col-lg order-lg-2 order-1">
        <div class="text-center d-flex flex-column mb-lg-0 mb-5">
            <h3 class="custom-modal-title mb-3 d-none d-lg-block">ຝາກເງິນ
                <hr class="hr-border-glow w-100 my-4">
            </h3>
            {{-- <div class="text-center d-none d-lg-block">
                <img src="{{secure_asset('img/ic_chip_deposit.png')}}" alt="ຝາກເງິນ" class="img-fluid  my-3"
                    width="100">
            </div> --}}
            <div class="zone-copy-bank">
                <div class=" d-flex align-items-center box-copy-bank">
                    <div class="media">
                        <div class="list-bank align-self-start mr-2">
                            {{-- <div id="bank_class" class="bank bk2" style="filter: grayscale(0%); -webkit-filter: grayscale(0%);"></div> --}}
                            <div class="">
                                <img class="ic_bank_deposit" src="{{secure_asset('img/ic_bank.png')}}" alt="">
                            </div>
                        </div>
                        <div class="media-body -new">
                            <div class="d-flex">
                                <h5 id="bank_number" class="my-0 text-yellow-2">{{$bank['number']}}</h5>
                                <div class="text_copy- ml-2">
                                    <button type="button" class="btn-copy-" data-clipboard-text="{{$bank['number']}}">
                                        <i class="fas fa-copy text-red"></i> <span class="text-yellow-2"></span>
                                    </button>
                                </div>
                            </div>
                            <p id="bank_name" class="mb-0 f-8 text-left">{{$bank['name']}}</p>
                            
                        </div>
                    </div>
                    
                    
                </div>
            </div>
            <hr class="w-100 my-4 border-dark d-none d-lg-block">
            <div class="my-3 flex-column text-center">
                <div class="f-5 text-gray-lighter">ເມື່ອຖືກໂອນແລ້ວ ກະລຸນາຢືນຢັນຢູ່ດ້ານລຸ່ມ</div>
            </div>
            <div class="text-center mt-2">
                <button class="btn btn-primary-modal d-block w-100 btn-lg btn-submit" disabled style="opacity: 0.65 !important;">
                    <div class="lds-ring">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div> ການກວດກາ...
                </button>
            </div>
            <hr class="hr-border-glow w-100 my-4">
            <div class="text-center modal-contact no-fixed text-white">
                <span>ພົບປັນຫາ</span>
                <a href="{{config('variable.line')}}" class="link-message " target="_blank">
                    <span>ຕິດຕໍ່ບໍລິການລູກຄ້າ</span>
                </a>
            </div>
        </div>
    </div>
</div>
