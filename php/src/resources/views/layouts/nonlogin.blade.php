<header class="header">
    <nav class="navbar">
        <div class="container  d-flex justify-content-between">
            <a class="navbar-brand" href="/"><img src="{{ secure_asset('img/logo.png') }}" class="img-fluid logo -size"
                    alt="LAOCASINO logo-navbar"></a>
            <div class="navbar-right navbar-login">
                <div class="d-flex">
                    <a href="/lobby" class="btn btn-primary-trytoplay d-lg-block d-none mx-2 px-lg-4 px-4">
                        <i class="fas fa-play mr-2" style="font-size:18px"></i> ທົດລອງຫຼີ້ນ</a>
                    <a href="#loginModal" data-toggle="modal" class="btn btn-primary-modal ">
                        <i class="fas fa-sign-in-alt mr-2"></i>
                        ເຂົ້າສູ່ລະບົບ</a>
                </div>
            </div><!-- navbar-login -->
        </div>
    </nav>
</header>

<section class="section-one">
    <div class="bg-container">
        {{-- <img src="{{secure_asset('img/bg_card.png')}}" alt="icon chip" class="bg-dice"> --}}
    </div><!-- bg-container -->
    <div class="contact-us d-flex flex-column">
        {{-- <hr class="hr-border-glow left m-0"> --}}
        {{-- <hr class="hr-border-glow top m-0 d-none d-sm-block"> --}}
        {{-- <a href="{{config('variable.line')}}" target="_blank" class="live-chat-container">
        <div class="d-flex flex-column">
            <div class="text-center m-auto">
                <i class="fas fa-comment-dots f-sm-4 f-5"></i>
            </div>
            <div class="text-center m-auto f-sm-7 f-8">
                <div class="mt-1">Live Chat</div>
            </div>
        </div>
        </a> --}}
        {{-- <a href="{{config('variable.line')}}" target="_blank" class="contact-us-container">
        <div class="d-flex flex-column">
            <div class="text-center m-auto">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="11 4 10 26" xml:space="preserve" style="" class="line-icon ">
                    <g>
                        <path d="M27 14.926C27 10.006 22.065 6 16 6S5 10.005 5 14.926c0 4.413 3.913 8.11 9.2
                                8.808.358.077.845.236.968.542.112.278.073.713.036.995l-.157.942c-.048.28-.22 1.088.953.593
                                1.174-.494 6.334-3.73 8.642-6.386C26.236 18.67 27 16.896 27 14.925zm-4.247-.41a.577.577 0
                                0 1 0 1.153h-1.61v1.03h1.61a.578.578 0 0 1 0 1.155h-2.186a.578.578 0 0 1-.577-.577v-4.37c0-.32.26-.578.577-.578h2.186a.578.578
                                0 0 1 0 1.153h-1.61v1.033h1.61zm-3.537 2.762a.575.575 0 0 1-.578.577.58.58 0 0 1-.46-.23l-2.24-3.05v2.703a.577.577
                                0 0 1-1.154 0v-4.37a.577.577 0 0 1 1.038-.347l2.24 3.05v-2.703a.578.578 0 0 1 1.154 0v4.37zm-5.26 0a.577.577
                                0 0 1-1.154 0v-4.37a.577.577 0 0 1 1.153 0v4.37zm-2.262.577H9.508a.577.577 0 0 1-.576-.577v-4.37a.577.577
                                0 0 1 1.153 0V16.7h1.61a.577.577 0 0 1 0 1.155z" fill-rule="evenodd"></path>
                    </g>
                </svg>
            </div>
            <div class="text-center m-auto f-sm-7 f-8">
                <div class="mt-1">แอดไลน์</div>
            </div>
        </div>
        </a> --}}
        <a href="{{config('variable.line')}}" target="_blank" class="contact-us-container">
            <div class="box-line">
                <div class="text-center">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="11 4 10 26" xml:space="preserve" style="" class="line-icon ">
                        <g>
                            <path d="M27 14.926C27 10.006 22.065 6 16 6S5 10.005 5 14.926c0 4.413 3.913 8.11 9.2
                                8.808.358.077.845.236.968.542.112.278.073.713.036.995l-.157.942c-.048.28-.22 1.088.953.593
                                1.174-.494 6.334-3.73 8.642-6.386C26.236 18.67 27 16.896 27 14.925zm-4.247-.41a.577.577 0
                                0 1 0 1.153h-1.61v1.03h1.61a.578.578 0 0 1 0 1.155h-2.186a.578.578 0 0 1-.577-.577v-4.37c0-.32.26-.578.577-.578h2.186a.578.578
                                0 0 1 0 1.153h-1.61v1.033h1.61zm-3.537 2.762a.575.575 0 0 1-.578.577.58.58 0 0 1-.46-.23l-2.24-3.05v2.703a.577.577
                                0 0 1-1.154 0v-4.37a.577.577 0 0 1 1.038-.347l2.24 3.05v-2.703a.578.578 0 0 1 1.154 0v4.37zm-5.26 0a.577.577
                                0 0 1-1.154 0v-4.37a.577.577 0 0 1 1.153 0v4.37zm-2.262.577H9.508a.577.577 0 0 1-.576-.577v-4.37a.577.577
                                0 0 1 1.153 0V16.7h1.61a.577.577 0 0 1 0 1.155z" fill-rule="evenodd"></path>
                        </g>
                    </svg>
                </div>
                <div class="text-center pl-2  f-sm-7 f-8">
                    <div class="text-line-">Add Line</div>
                </div>
            </div>
        </a>
        {{-- <hr class="hr-border-glow bottom m-0 d-none d-sm-block"> --}}
    </div><!-- contact-us -->

    {{-- <div class="contact-us-mobile">
        <div class="contact-">
            <a href="{{config('variable.line')}}" target="_blank" class="contact-us-container contact-mo">

                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="11 4 10 26" xml:space="preserve" style="" class="line-icon-mobile ">
                    <g>
                        <path d="M27 14.926C27 10.006 22.065 6 16 6S5 10.005 5 14.926c0 4.413 3.913 8.11 9.2
                    8.808.358.077.845.236.968.542.112.278.073.713.036.995l-.157.942c-.048.28-.22 1.088.953.593
                    1.174-.494 6.334-3.73 8.642-6.386C26.236 18.67 27 16.896 27 14.925zm-4.247-.41a.577.577 0
                    0 1 0 1.153h-1.61v1.03h1.61a.578.578 0 0 1 0 1.155h-2.186a.578.578 0 0 1-.577-.577v-4.37c0-.32.26-.578.577-.578h2.186a.578.578
                    0 0 1 0 1.153h-1.61v1.033h1.61zm-3.537 2.762a.575.575 0 0 1-.578.577.58.58 0 0 1-.46-.23l-2.24-3.05v2.703a.577.577
                    0 0 1-1.154 0v-4.37a.577.577 0 0 1 1.038-.347l2.24 3.05v-2.703a.578.578 0 0 1 1.154 0v4.37zm-5.26 0a.577.577
                    0 0 1-1.154 0v-4.37a.577.577 0 0 1 1.153 0v4.37zm-2.262.577H9.508a.577.577 0 0 1-.576-.577v-4.37a.577.577
                    0 0 1 1.153 0V16.7h1.61a.577.577 0 0 1 0 1.155z" fill-rule="evenodd"></path>
                    </g>
                </svg>

            </a>
        </div>
    </div> --}}

    <div class="inner-wrapper">
        

        <div class="box-float">
            <img class="img-light-new w-100" src="{{secure_asset('img/bg_float.png')}}" alt="">
        </div>


        <div class="container position-relative">
            <div class="box-chip-1">
                <img class="img-chip-1" src="{{secure_asset('img/bg_extra8.png')}}" alt="">
            </div>
            <div class="box-dice">
                <img class="img-dice-" src="{{secure_asset('img/bg_extra6.png')}}" alt="">
            </div>
            <div class="row ">
                <div class="col-lg-8 col-md-7 col-sm-12 box-girl-extra">
                    <div class="box-girl">
                        <img class="img-girl w-100" src="{{secure_asset('img/bg_extra7.png')}}" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-12 d-flex align-items-center box-logo-">
                    <div class="text-center box-index-">
                        <div class="">
                            <img class="img-logo-index " src="{{secure_asset('img/logo_main.png')}}" alt="">
                        </div>
                        <div>
                            <img class="img-text-index " src="{{secure_asset('img/bg_extra4.png')}}" alt="">
                        </div>
                        <hr class="hr-border-glow w-100 mt-4">
                        <p class="text-white">
                            ເວັບດຽວທີ່ຮວມຄາສິໂນຊັ້ນນຳໄວ້ແລ້ວ ບໍລິການດ້ວຍລະບົບຝາກ-ຖອນ ອັດຕະໂນມັດ ຜ່ານທີມງານມືອາຊີບດ້ວຍປະສົບການກວ່າ 5 ປີ			
                        </p>
                        <a  data-target="#register_modal" data-toggle="modal"  class="btn btn-warning -nonlogin"> ສະໜັກສະມາຊິກ</a>
                    </div>
                </div>

                <div class="col-lg-12">



                    {{-- <div class="intro-promotion pb-1">
                        <div class="slider slick-promotion-header d-none d-sm-block">
                            <div class="d-flex mx-3 flex-column">
                                <div class="slider-cover align-self-center">
                                    <img src="{{secure_asset('img/2x/banner_main@2x.png')}}" class="mb-3">
                                </div>
                            </div>
                            <div class="d-flex mx-3 flex-column">
                                <div class="slider-cover align-self-center">
                                    <img src="{{secure_asset('img/2x/banner_main@2x.png')}}" class="mb-3">
                                </div>
                            </div>
                            <div class="d-flex mx-3 flex-column">
                                <div class="slider-cover align-self-center">
                                    <img src="{{secure_asset('img/2x/banner_main@2x.png')}}" class="mb-3">
                                </div>
                            </div>
                        </div><!-- slick desktop --->
                        <div class="slider slick-promotion-header d-block d-sm-none">
                            <div class="d-flex mx-3 flex-column">
                                <div class="slider-cover align-self-center">
                                    <img src="{{secure_asset('img/banner_main_mobile.png')}}" class="mb-3">
                                </div>
                            </div>
                            <div class="d-flex mx-3 flex-column">
                                <div class="slider-cover align-self-center">
                                    <img src="{{secure_asset('img/banner_main_mobile.png')}}" class="mb-3">
                                </div>
                            </div>
                            <div class="d-flex mx-3 flex-column">
                                <div class="slider-cover align-self-center">
                                    <img src="{{secure_asset('img/banner_main_mobile.png')}}" class="mb-3">
                                </div>
                            </div>
                        </div><!-- slick moibile --->
                    </div> --}}

                    {{-- <div class="col-lg-12 ">
                        <div class="box-brand text-center">
                            <h1 class="text-yellow text-brand-header">
                                SEXY GAMING
                            </h1>

                        </div>

                    </div> --}}

                    {{-- <div class="row mt-5  btn-actions wow fadeInUp" data-wow-delay="0.12s"
                        style="visibility: visible; animation-delay: 0.12s; animation-name: fadeInUp;">
                        <div class="d-flex  m-auto">
                            <a data-target="#register_modal" data-toggle="modal"
                                class="btn-image hoverable btn-glow btn-register d-lg-block mr-3 -btn-img-outer-glow">
                                <img class="default" src="{{secure_asset('img/2x/btn_register@2x.png')}}"
                                    alt="LAOCASINO register">
                                <img class="hover" src="{{secure_asset('img/2x/btn_register_hover@2x.png')}}"
                                    alt="LAOCASINO register hover">
                            </a>
                            <a href="/lobby"
                                class="btn-image hoverable btn-glow btn-register d-lg-block mr-3 register- ">
                                <img class="default" src="{{secure_asset('img/2x/btn_demo@2x.png')}}" alt="LAOCASINO register">
                                <img class="hover" src="{{secure_asset('img/2x/btn_demo_hover@2x.png')}}"
                                    alt="LAOCASINO register hover">
                            </a>
                            <a href="/lobby" class="btn btn-primary-trytoplay ml-3">
                                    <i class="fas fa-play mr-2" style="font-size:18px"></i>
                                    ทดลองเล่น
                                </a>

                        </div>
                    </div> --}}

                </div>

                {{-- <div class="col-lg-5 -header-index">
                    <div class="d-flex align-items-end justify-content-center wow fadeInUp d-block d-lg-none" data-wow-duration="0.5s">
                        <div class="logo-wrapper text-center">
                            <img src="{{secure_asset('img/bg_extra_one.png')}}" alt="LAOCASINO pretty"
                class="logo-title-second">
            </div>
        </div>
        <h2 data-wow-duration="0.9s"
            class="order-1 -title text-white text-lg-left text-center font-weight-normal wow fadeInUp">
            LAOCASINO
        </h2>
        <h3 data-wow-duration="0.9s"
            class="-sub-title text-white text-lg-left text-center font-weight-normal wow fadeInUp">

            LAOCASINO กีฬา คาสิโน บาคาร่า สล็อต <br class="d-none d-lg-block">เว็บเดียวจบครบทุกการเดิมพัน มั่นคง รวดเร็ว
            ปลอดภัย
        </h3>
        <div class="row mt-5 mr-lg-5 btn-actions wow fadeInUp" data-wow-delay="0.12s"
            style="visibility: visible; animation-delay: 0.12s; animation-name: fadeInUp;">
            <div class="d-flex flex-column m-auto">
                <a data-target="#register_modal" data-toggle="modal"
                    class="btn-image hoverable btn-glow btn-register d-lg-block">
                    <img class="default" src="{{secure_asset('img/2x/btn_register@2x.png')}}" alt="LAOCASINO register">
                    <img class="hover" src="{{secure_asset('img/2x/btn_register_hover@2x.png')}}" alt="LAOCASINO register hover">
                </a>
            </div>
        </div>
    </div> --}}
    {{-- <div class="col-lg-7 order-1 -pretty-right">

                    <img class="image wow fadeInUp" src="{{secure_asset('img/2x/bg_extra_one@2x.png')}}" alt="LAOCASINO pretty"
    style="visibility: hidden; animation-duration: 1.5s; animation-name: none;">
    </div> --}}
    </div>
    </div>
    </div><!-- inner-wrapper -->
    {{-- <div class="service-wrapper mt-3">
        <div class="row container m-auto">
            <div class="col-md-4 text-center d-flex align-items-start d-md-block box">
                <a class="d-flex flex-md-column flex-row" data-toggle="modal" data-target="#registerModal">
                    <div class="ic-wrapper mb-2">
                        <img src="{{secure_asset('img/ic_step_30s.png')}}" alt="icon register" class="ic">
    </div>
    <div class="text-left text-md-center">
        <h3><span class="d-inline-block d-md-none">1.</span> ฝาก-ถอน 30 วิ</h3>
        <hr class="hr-border-glow">

        <span class="d-none d-lg-block text-muted-lighter f-5">รวดเร็วด้วยระบบอัตโนมัติ</span>
        <span class="d-block d-lg-none text-muted-lighter f-5">รวดเร็วด้วยระบบอัตโนมัติ</span>
    </div>
    </a>
    </div>
    <div class="col-md-4 text-center d-flex align-items-start d-md-block box">
        <div class="ic-wrapper mb-2">
            <img src="{{secure_asset('img/ic_step_service.png')}}" alt="icon register" class="ic">
        </div>
        <div class="text-left text-md-center">
            <h3><span class="d-inline-block d-md-none">2.</span> บริการลูกค้า</h3>
            <hr class="hr-border-glow">
            <span class="d-none d-lg-block text-muted-lighter f-5">รับประกันงานบริการ</span>
            <span class="d-block d-lg-none text-muted-lighter f-5">รับประกันงานบริการ</span>
        </div>
    </div>
    <div class="col-md-4 text-center d-flex align-items-start d-md-block box">
        <div class="ic-wrapper mb-2">
            <img src="{{secure_asset('img/ic_step_secure.png')}}" alt="icon register" class="ic">
        </div>
        <div class="text-left text-md-center">
            <h3><span class="d-inline-block d-md-none">3.</span> ความมั่นคง</h3>
            <hr class="hr-border-glow">
            <span class="d-none d-lg-block text-muted-lighter f-5">ถอนสูงสุดวันละ ล้าน/บัญชี</span>
            <span class="d-block d-lg-none text-muted-lighter f-5">ถอนสูงสุดวันละ ล้าน/บัญชี</span>
        </div>
    </div>
    </div>
    </div> --}}
    <!-- service-wrapper -->
</section><!-- section-one -->

<section>
    <div class="button-actions">
        <div class="d-flex align-items-end">
            <a href="/lobby" target="_blank" class="btn btn-lg btn-primary flex-grow-1 pb-0">
                <i class="fas fa-play mr-2" style="font-size: 18px;"></i>  ທົດລອງຫຼີ້ນ
            </a>
        </div>
    </div>
</section>

@include('modals.login')
@include('modals.forgetpass')

@include('modals.undefined')


<!-- 1.registerModal -->
<div class="modal fade custom-modal" id="register_modal" tabindex="-1" role="dialog"
    aria-labelledby="registerModalLabel" aria-hidden="true">
    @include('modals.register1')
</div>



<script>
    function open_deposit_modal(code = null, name = null) {
        $("#undefinedModal").modal('show');
    }

    var _p = new URLSearchParams(window.location.search).get("_p");
    if (_p) {
        open_deposit_modal()
    }
</script>
