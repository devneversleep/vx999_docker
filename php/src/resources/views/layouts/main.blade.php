<!doctype html>
<html class="no-js" lang="th" class="x-windows-os">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    {{-- @if (!isset($seo_head))

    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="SEXY GAMING,SEXY GAME,SEXY BACCARAT">

    <title>@yield('title')</title>
    @else
    {!! $seo_head !!}
    @endif --}}

    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>LAOCASINO</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="canonical" href="{{ Request::url() }}">

    <link rel="manifest" href="{{secure_asset('site.webmanifest')}}">
    <!-- <link rel="apple-touch-icon" href="icon.png"> -->
    <link rel="icon" href="{{secure_asset('favicon-32x32.png')}}" type="image/x-icon">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{secure_asset('css/styles.css?v=' . time())}}">
    <link rel="stylesheet" href="{{secure_asset('css/animate.css')}}">

    <link rel="preload" as="font" href="/fonts/DB HelvethaicaMon X Med.f4018213.ttf" type="font/ttf"
        crossorigin="anonymous">
    <link rel="preload" as="font" href="/fonts/DB HelvethaicaMon X.cfd3be53.ttf" type="font/ttf"
        crossorigin="anonymous">
    <link rel="stylesheet" href="{{secure_asset('css/owl.carousel.min.css')}}">

    {{-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{secure_asset('css/slick.css')}}"/> --}}
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    />
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css" />
     
    <script>
        window.jQuery || document.write('<script src="{{secure_asset("js/vendor/jquery-3.3.1.min.js")}}"><script>')

    </script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{secure_asset('js/vendor/popper.min.js')}}"></script>
    <script type="text/javascript" src="{{secure_asset('js/vendor/bootstrap.min.js')}}"></script>

    <script type="text/javascript" src="{{secure_asset('js/plugins.js')}}"></script>
    <script type="text/javascript" src="{{secure_asset('js/vendor/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{secure_asset('js/main.js?v='.time())}}"></script>

    {{-- <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> --}}
    {{-- <script type="text/javascript" src="{{secure_asset('js/vendor/slick.min.js')}}"></script> --}}

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <link rel="stylesheet" href="{{secure_asset('css/font-awesome.min.css')}}" />

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
</head>

<body data-spy="scroll" data-target=".scrollspy" class="bg-img-">
    @yield('content')
    <footer class="footer py-4" style="background-color: #000;">
        <div class="inner-wrapper">
            <center class="container">

                {{-- <div class="row">
                <div class="col-3 -custom-col-3"">
                    <img src="{{secure_asset('img/logo_nav.png')}}" class="footer-img-logo">
        </div>
        <div class="col-3 text-left -custom-col-3">
            <h3 class="foorter-head-titlie">LAOCASINO</h3>
            <h6>
                <p>เป็นทางเลือกแรกของทุกคน ด้วยบริการที่ดีทุกระดับ
                    เราคือผู้ให้บริการ CASINO เจ้าแรกในประเทศไทยใน
                    การเงินมั่นคงฝากถอน รวดเร็วทันใจ</p>
            </h6>
        </div>
        <div class="col-2 text-left -custom-col-2">
            <h3 class="foorter-head-titlie">ข้อมูล</h3>
            <h6>
                <a>เกี่ยวกับเรา</a>
                <a>กติกาข้อตกลง</a>
                <a>ความเป็นส่วนตัว</a>
                <a>ศูนย์ช่วยเหลือ</a>
            </h6>
        </div>
        <div class="col-2 text-left -custom-col-2">
            <h3 class="foorter-head-titlie">กิจกรรม</h3>
            <h6>
                <a>โปรโมชั่นอัพเดท</a>
                <a>แนะนำเพื่อน</a>
                <a>รับเงินคืน</a>
                <a>เครดิตฟรี</a>
            </h6>
        </div>
        <div class="col-2 text-left -custom-col-2">
            <h3 class="foorter-head-titlie">ติดต่อเรา</h3>
            <h6>
                <a>บริการ</a>
                <a>เกี่ยวกับเรา</a>
                <a>วิธีการเล่น</a>
            </h6>
        </div>
        </div> --}}

        <div class="row">
            <div class="col pb-0 pb-3 ">
                <img data-src=" {{secure_asset('img/logo.png')}}" class="footer-img-logo lazy" alt="LAOCASINO logo-footer">
            </div>
        </div>

        {{-- <div class="col-12 -footer-tags">
            <div class="btn-tag">
                <a href="/" class="btn btn-sm">sexy</a>
                <a href="/" class="btn btn-sm">sexy gaming</a>
                <a href="/" class="btn btn-sm">sexy game</a>
                <a href="/" class="btn btn-sm">sexy casino</a>
                <a href="/" class="btn btn-sm">บาคาร่าออนไลน์ sexy gaming</a>
                <a href="/" class="btn btn-sm">คาสิโน่ออนไลน์ sexy gaming</a>
            </div>

            <div class="btn-tag">
                <a href="/" class="btn btn-sm">บาคาร่า sexy game</a>
                <a href="/" class="btn btn-sm">คาสิโน่ sexy game</a>
                <a href="/" class="btn btn-sm">sexy barccarat</a>
            </div>

        </div> --}}

        </center>
        </div>
        <div class="inner-wrapper">
            <div
                class="container image-wrapper d-none d-lg-block justify-content-center align-items-center px-3 px-lg-0 flex-wrap">
                <div class="footer-bank-logo">
                    <div class="container">
                        <div class="wrapper">
                            <div class="ic lazy ic-1" data-bg="{{secure_asset('img/ic-bank-logo.png')}}"></div>
                            <div class="ic lazy ic-2" data-bg="{{secure_asset('img/ic-bank-logo.png')}}"></div>
                            <div class="ic lazy ic-3" data-bg="{{secure_asset('img/ic-bank-logo.png')}}"></div>
                            <div class="ic lazy ic-4" data-bg="{{secure_asset('img/ic-bank-logo.png')}}"></div>
                            <div class="ic lazy ic-5" data-bg="{{secure_asset('img/ic-bank-logo.png')}}"></div>
                            <div class="ic lazy ic-6" data-bg="{{secure_asset('img/ic-bank-logo.png')}}"></div>
                            <div class="ic lazy ic-7" data-bg="{{secure_asset('img/ic-bank-logo.png')}}"></div>
                            <div class="ic lazy ic-8" data-bg="{{secure_asset('img/ic-bank-logo.png')}}"></div>
                            <div class="ic lazy ic-9" data-bg="{{secure_asset('img/ic-bank-logo.png')}}"></div>
                            <div class="ic lazy ic-10" data-bg="{{secure_asset('img/ic-bank-logo.png')}}"></div>
                            <div class="ic lazy ic-11" data-bg="{{secure_asset('img/ic-bank-logo.png')}}"></div>
                            <div class="ic lazy ic-12" data-bg="{{secure_asset('img/ic-bank-logo.png')}}"></div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <hr class="hr-border-glow-footer"> --}}
            <div class="text-center mt-4">
                <!-- <div>
                    <a href="/term-and-condition" target="_blank" class="text-white-50">Term and condition</a>
                </div> -->
                <p class="mb-0">
                    Term and condition<br>Copyright © 2020 LAOCASINO All Rights Reserved.
                </p>
            </div>
        </div>
    </footer>


    @if($errors->any())
    <script>
        error_alert('{{$errors->first()}}')

    </script>
    @endif
    <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
    <script>
        window.ga = function () {
            ga.q.push(arguments)
        };
        ga.q = [];
        ga.l = +new Date;
        ga('create', 'UA-XXXXX-Y', 'auto');
        ga('send', 'pageview')
    </script>

    <script type="text/javascript">
        // $(document).ready(function(){
        //     $('.your-class').slick({
        //         setting-name: setting-value
        //     });
        // });

    </script>
    <script src="https://www.google-analytics.com/analytics.js" async defer></script>
    <script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.3.0/dist/lazyload.min.js"></script>
    <script>
        var lazyLoadInstance = new LazyLoad({

        });
        lazyLoadInstance.update();
    </script>
</body>

</html>
