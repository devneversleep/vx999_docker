<header class="header">
    <nav class="navbar">
        <div class="container position-relative d-flex justify-content-between">
            <a class="navbar-brand" href="/"><img src="{{secure_asset('img/logo.png')}}" class="img-fluid logo" alt="LAOCASINO logo-navbar"></a>
            <div class="navbar-right navbar-login logged-box">
                <div class="d-flex logged-container">
                    <div class="py-3 px-3 px-lg-4 profile">
                        <div class="d-flex">
                            <div class="mr-2 d-flex logged-sidebar" id="showRight">
                                <div class="profile-image">
                                    <img class="img-fluid mb-0" src="{{secure_asset('img/ic_balance.png')}}">
                                </div>
                            </div>
                            <div class="d-flex flex-column m-auto profile-detail f-7">
                                <div class="text-lg-right  text-white mb-1" id="showRight1">
                                    @if ( strlen(Session::get('phone_number')) > 11)
                                        {{ substr(Session::get('phone_number'), 3) }}
                                    @else 
                                        {{ Session::get('phone_number') }}
                                    @endif
                                </div>
                                <div class="text-primary user-balance text-nowrap">
                                    <span id="customer-balance">
                                        <span id="user_credits"
                                            class="text-yellow">{{number_format($accountinfo['current_balance'], 2,'.',',')}}</span>
                                    </span>
                                    <button type="button" class="btn btn-link p-0" onclick="Fx_refresh_credit()">
                                        <img class="animated" id="imgrefresh" src="{{secure_asset('img/refresh-button.png')}}"
                                            width="16px">
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="dropdown-content">
                            <div class="profile-dropdown-menu px-3">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a href="#account" class="nav-link" data-toggle="modal"
                                            data-target="#AccountProviderMoneyModal" onclick="show_slide_provider()">
                                            <img class="mr-3" src="{{secure_asset('img/ic_mini_account.png')}}"
                                                alt="icon account" width="20">
                                            <span class="text-white">ຂໍ້ມູນສ່ວນໂຕ</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#promotion-active" class="nav-link" data-toggle="modal"
                                            data-target="#PomotionModal">
                                            <img class="mr-3" src="{{secure_asset('img/ic_mini_promotion.png')}}"
                                                alt="icon promotion" width="20">
                                            <span class="text-white">ໂປໂມຊັ່ນທີ່ເຂົ້າຮ່ວມ</span>
                                        </a>
                                    </li>
                                    <li class="nav-item js-close-sidebar">
                                        <a href="#LogoutModal" class="nav-link" data-toggle="modal"
                                            data-target="#LogoutModal" data-title="ต้องการออกจากระบบ ?">
                                            <img class="mr-3" src="{{secure_asset('img/ic_mini_logout.png')}}"
                                                alt="icon logout" width="20">
                                            <span class="text-white">ອອກ​ຈາກ​ລະ​ບົບ</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div><!-- dropdown-content -->
                    </div><!-- profile -->
                    <div class="deposit d-none d-lg-block">
                        <a href="#deposit" {{--onclick="show_slide_left()"--}} class="text-white" data-toggle="modal"
                            data-target="#depositModal">
                            <div class="d-flex flex-column justify-content-center h-100 px-3 -border-left">
                                <img class="img-fluid mt-2" src="{{secure_asset('img/ic_deposit.png')}}"
                                    alt="icon deposit white" width="40">
                                <div class="f-7 mt-1 text-white">ຝາກ</div>
                            </div>
                        </a>
                    </div><!-- deposit -->
                    <div class="withdraw d-none d-lg-block">
                        <a href="#withdraw" class="text-white" data-toggle="modal" data-target="#withdrawModal">
                            <div class="d-flex flex-column justify-content-center h-100 px-3 -border-left">
                                <img class="img-fluid mt-2" src="{{secure_asset('img/ic_withdraw.png')}}"
                                    alt="icon deposit white" width="40">
                                <div class="f-7 mt-1 text-white">ຖອນ</div>
                            </div>
                        </a>
                    </div><!-- withdraw -->
                </div><!-- logged-container -->
            </div><!-- navbar-login -->
        </div>
    </nav>
</header>

<section class="section-one">
    <div class="bg-container">
        {{-- <img src="{{secure_asset('img/bg_card.png')}}" alt="icon chip" class="bg-dice"> --}}
    </div>
    <!-- bg-container -->
    <div class="contact-us d-flex flex-column ">
        {{-- <hr class="hr-border-glow left m-0"> --}}
        {{-- <hr class="hr-border-glow top m-0"> --}}
        {{-- <a href="{{config('variable.line')}}" target="_blank" class="live-chat-container">
            <div class="d-flex flex-column">
                <div class="text-center m-auto">
                    <i class="fas fa-comment-dots f-sm-4 f-5"></i>
                </div>
                <div class="text-center m-auto f-sm-7 f-8">
                    <div class="mt-1">Live Chat</div>
                </div>
            </div>
        </a>
        <a href="{{config('variable.line')}}" target="_blank" class="contact-us-container">
            <div class="d-flex flex-column">
                <div class="text-center m-auto">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="11 4 10 26" xml:space="preserve" style="" class="line-icon ">
                        <g>
                            <path d="M27 14.926C27 10.006 22.065 6 16 6S5 10.005 5 14.926c0 4.413 3.913 8.11 9.2
                            8.808.358.077.845.236.968.542.112.278.073.713.036.995l-.157.942c-.048.28-.22 1.088.953.593
                            1.174-.494 6.334-3.73 8.642-6.386C26.236 18.67 27 16.896 27 14.925zm-4.247-.41a.577.577 0
                            0 1 0 1.153h-1.61v1.03h1.61a.578.578 0 0 1 0 1.155h-2.186a.578.578 0 0 1-.577-.577v-4.37c0-.32.26-.578.577-.578h2.186a.578.578
                            0 0 1 0 1.153h-1.61v1.033h1.61zm-3.537 2.762a.575.575 0 0 1-.578.577.58.58 0 0 1-.46-.23l-2.24-3.05v2.703a.577.577
                            0 0 1-1.154 0v-4.37a.577.577 0 0 1 1.038-.347l2.24 3.05v-2.703a.578.578 0 0 1 1.154 0v4.37zm-5.26 0a.577.577
                            0 0 1-1.154 0v-4.37a.577.577 0 0 1 1.153 0v4.37zm-2.262.577H9.508a.577.577 0 0 1-.576-.577v-4.37a.577.577
                            0 0 1 1.153 0V16.7h1.61a.577.577 0 0 1 0 1.155z" fill-rule="evenodd"></path>
                        </g>
                    </svg>
                </div>
                <div class="text-center m-auto f-sm-7 f-8">
                    <div class="mt-1">แอดไลน์</div>
                </div>
            </div>
        </a> --}}
        <a href="{{config('variable.line')}}" target="_blank" class="contact-us-container">
            <div class="box-line">
                <div class="text-center">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="11 4 10 26" xml:space="preserve" style="" class="line-icon ">
                        <g>
                            <path d="M27 14.926C27 10.006 22.065 6 16 6S5 10.005 5 14.926c0 4.413 3.913 8.11 9.2
                                8.808.358.077.845.236.968.542.112.278.073.713.036.995l-.157.942c-.048.28-.22 1.088.953.593
                                1.174-.494 6.334-3.73 8.642-6.386C26.236 18.67 27 16.896 27 14.925zm-4.247-.41a.577.577 0
                                0 1 0 1.153h-1.61v1.03h1.61a.578.578 0 0 1 0 1.155h-2.186a.578.578 0 0 1-.577-.577v-4.37c0-.32.26-.578.577-.578h2.186a.578.578
                                0 0 1 0 1.153h-1.61v1.033h1.61zm-3.537 2.762a.575.575 0 0 1-.578.577.58.58 0 0 1-.46-.23l-2.24-3.05v2.703a.577.577
                                0 0 1-1.154 0v-4.37a.577.577 0 0 1 1.038-.347l2.24 3.05v-2.703a.578.578 0 0 1 1.154 0v4.37zm-5.26 0a.577.577
                                0 0 1-1.154 0v-4.37a.577.577 0 0 1 1.153 0v4.37zm-2.262.577H9.508a.577.577 0 0 1-.576-.577v-4.37a.577.577
                                0 0 1 1.153 0V16.7h1.61a.577.577 0 0 1 0 1.155z" fill-rule="evenodd"></path>
                        </g>
                    </svg>
                </div>
                <div class="text-center pl-2  f-sm-7 f-8">
                    <div class="text-line-">Add Line</div>
                </div>
            </div>
        </a>
        {{-- <hr class="hr-border-glow bottom m-0"> --}}
    </div>
    <!-- contact-us -->

    {{-- <div class="contact-us-mobile">
        <div class="contact-">
            <a href="{{config('variable.line')}}" target="_blank" class="contact-us-container contact-mo">

                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="11 4 10 26" xml:space="preserve" style="" class="line-icon-mobile ">
                <g>
                    <path d="M27 14.926C27 10.006 22.065 6 16 6S5 10.005 5 14.926c0 4.413 3.913 8.11 9.2
                    8.808.358.077.845.236.968.542.112.278.073.713.036.995l-.157.942c-.048.28-.22 1.088.953.593
                    1.174-.494 6.334-3.73 8.642-6.386C26.236 18.67 27 16.896 27 14.925zm-4.247-.41a.577.577 0
                    0 1 0 1.153h-1.61v1.03h1.61a.578.578 0 0 1 0 1.155h-2.186a.578.578 0 0 1-.577-.577v-4.37c0-.32.26-.578.577-.578h2.186a.578.578
                    0 0 1 0 1.153h-1.61v1.033h1.61zm-3.537 2.762a.575.575 0 0 1-.578.577.58.58 0 0 1-.46-.23l-2.24-3.05v2.703a.577.577
                    0 0 1-1.154 0v-4.37a.577.577 0 0 1 1.038-.347l2.24 3.05v-2.703a.578.578 0 0 1 1.154 0v4.37zm-5.26 0a.577.577
                    0 0 1-1.154 0v-4.37a.577.577 0 0 1 1.153 0v4.37zm-2.262.577H9.508a.577.577 0 0 1-.576-.577v-4.37a.577.577
                    0 0 1 1.153 0V16.7h1.61a.577.577 0 0 1 0 1.155z" fill-rule="evenodd"></path>
                </g>
                </svg>

            </a>
        </div>
    </div> --}}
    <div class="inner-wrapper">
        <div class="box-float">
            <img class="img-light-new w-100" src="{{secure_asset('img/bg_float.png')}}" alt="">
        </div>


        <div class="container position-relative">
            <div class="box-chip-1">
                <img class="img-chip-1" src="{{secure_asset('img/bg_extra8.png')}}" alt="">
            </div>
            <div class="box-dice">
                <img class="img-dice-" src="{{secure_asset('img/bg_extra6.png')}}" alt="">
            </div>
            <div class="row">

                <div class="col-lg-8 col-md-7 col-sm-12 box-girl-extra">
                    <div class="box-girl">
                        <img class="img-girl w-100" src="{{secure_asset('img/bg_extra7.png')}}" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-12 d-flex align-items-center box-logo-">
                    <div class="text-center box-index-">
                        <div class="">
                            <img class="img-logo-index " src="{{secure_asset('img/logo_main.png')}}" alt="">
                        </div>
                        <div>
                            <img class="img-text-index " src="{{secure_asset('img/bg_extra4.png')}}" alt="">
                        </div>
                        <hr class="hr-border-glow w-100 mt-4">
                        <p class="text-white">
                            ເວັບດຽວທີ່ຮວມຄາສິໂນຊັ້ນນຳໄວ້ແລ້ວ ບໍລິການດ້ວຍລະບົບຝາກ-ຖອນ ອັດຕະໂນມັດ ຜ່ານທີມງານມືອາຊີບດ້ວຍປະສົບການກວ່າ 5 ປີ
                        </p>
                        <a  href="/lobby"  class="btn btn-warning width-btn"> ເຂົ້າຫລິ້ນເກມ</a>
                    </div>
                </div>
                {{-- <div class="col-lg-12">
                    <div class="intro-promotion">
                        <div class="slider slick-promotion-header d-none d-sm-block">
                            <div class="d-flex mx-3 flex-column">
                                <div class="slider-cover align-self-center">
                                    <img src="{{secure_asset('img/2x/banner_main@2x.png')}}" class="mb-3">
                                </div>
                            </div>
                            <div class="d-flex mx-3 flex-column">
                                <div class="slider-cover align-self-center">
                                    <img src="{{secure_asset('img/2x/banner_main@2x.png')}}" class="mb-3">
                                </div>
                            </div>
                            <div class="d-flex mx-3 flex-column">
                                <div class="slider-cover align-self-center">
                                    <img src="{{secure_asset('img/2x/banner_main@2x.png')}}" class="mb-3">
                                </div>
                            </div>
                        </div><!-- slick desktop --->
                        <div class="slider slick-promotion-header d-block d-sm-none">
                            <div class="d-flex mx-3 flex-column">
                                <div class="slider-cover align-self-center">
                                    <img src="{{secure_asset('img/banner_main_mobile.png')}}" class="mb-3">
                                </div>
                            </div>
                            <div class="d-flex mx-3 flex-column">
                                <div class="slider-cover align-self-center">
                                    <img src="{{secure_asset('img/banner_main_mobile.png')}}" class="mb-3">
                                </div>
                            </div>
                            <div class="d-flex mx-3 flex-column">
                                <div class="slider-cover align-self-center">
                                    <img src="{{secure_asset('img/banner_main_mobile.png')}}" class="mb-3">
                                </div>
                            </div>
                        </div>
                        <!-- slick moibile --->
                    </div>
                    <div class="col-lg-12 ">
                        <div class="box-brand text-center" >
                            <h1 class="text-yellow text-brand-header">
                                SEXY GAMING
                            </h1>

                        </div>

                    </div>
                    <div class="row mt-5  btn-actions wow fadeInUp" data-wow-delay="0.12s"
                    style="visibility: visible; animation-delay: 0.12s; animation-name: fadeInUp;">
                        <div class="d-flex  m-auto">
                                <a href="/lobby" target="_blank" class="btn-image hoverable btn-glow btn-register d-lg-block -btn-img-outer-glow">
                                    <img class="default" src= "{{secure_asset('img/2x/btn_play@2x.png')}}" alt="LAOCASINO register">
                                    <img class="hover" src="{{secure_asset('img/2x/btn_play_hover@2x.png')}}" alt="LAOCASINO register hover">
                                </a>
                        </div>
                    </div>

                </div> --}}

            </div>
        </div>
    </div><!-- inner-wrapper -->
    <!-- inner-wrapper -->
    {{-- <div class="service-wrapper mt-3">
        <div class="row container m-auto">
            <div class="col-md-4 text-center d-flex align-items-start d-md-block box">
                <a class="d-flex flex-md-column flex-row" data-toggle="modal" data-target="#registerModal">
                    <div class="ic-wrapper mb-2">
                        <img src="{{secure_asset('LAOCASINO/All New/Asset 13.png')}}" alt="icon register" class="ic">
    </div>
    <div class="text-left text-md-center">
        <h3><span class="d-inline-block d-md-none">1.</span> ฝาก-ถอน 30 วิ</h3>
        <hr class="hr-border-glow">

        <span class="d-none d-lg-block text-muted-lighter f-5">รวดเร็วด้วยระบบอัตโนมัติ</span>
        <span class="d-block d-lg-none text-muted-lighter f-5">รวดเร็วด้วยระบบอัตโนมัติ</span>
    </div>
    </a>
    </div>
    <div class="col-md-4 text-center d-flex align-items-start d-md-block box">
        <div class="ic-wrapper mb-2">
            <img src="{{secure_asset('LAOCASINO/All New/Asset 12.png')}}" alt="icon register" class="ic">
        </div>
        <div class="text-left text-md-center">
            <h3><span class="d-inline-block d-md-none">2.</span> บริการลูกค้า</h3>
            <hr class="hr-border-glow">
            <span class="d-none d-lg-block text-muted-lighter f-5">รับประกันงานบริการ</span>
            <span class="d-block d-lg-none text-muted-lighter f-5">รับประกันงานบริการ</span>
        </div>
    </div>
    <div class="col-md-4 text-center d-flex align-items-start d-md-block box">
        <div class="ic-wrapper mb-2">
            <img src="{{secure_asset('LAOCASINO/All New/Asset 11.png')}}" alt="icon register" class="ic">
        </div>
        <div class="text-left text-md-center">
            <h3><span class="d-inline-block d-md-none">3.</span> ความมั่นคง</h3>
            <hr class="hr-border-glow">
            <span class="d-none d-lg-block text-muted-lighter f-5">ถอนสูงสุดวันละ ล้าน/บัญชี</span>
            <span class="d-block d-lg-none text-muted-lighter f-5">ถอนสูงสุดวันละ ล้าน/บัญชี</span>
        </div>
    </div>
    </div>
    </div> --}}
    <!-- service-wrapper -->
</section>
<div class="fix-mobile d-lg-none d-block button-actions">
    <div class="d-flex">
        <a href="javascript:void(0);" data-toggle="modal" data-target="#depositModal"
            class="btn btn-lg btn-plain-primary">
            <img src="{{secure_asset('img/ic_deposit.png')}}" alt="LAOCASINO ฝากเงิน" class="icon img-fluid mr-2">
            <span class="typo">ຝາກ</span>
        </a>
        <a href="javascript:void(0);" data-toggle="modal" data-target="#withdrawModal"
            class="btn btn-lg btn-plain-secondary">
            <img src="{{secure_asset('img/ic_withdraw.png')}}" alt="LAOCASINO ถอนเงิน" class="icon img-fluid mr-2">
            <span class="typo text-white">ຖອນ</span>
        </a>
    </div>
</div>
<nav class="menu menu-right">
    <div class="mt-4 d-flex justify-content-between">
        <img src="{{secure_asset('img/logo_main.png')}}" style="width: 150px!important; height:90px;"><a href="#"
            class="backBtn f-1 d-inline-block">x</a>
    </div>

    <ul class="navbar-nav mt-5">
        <li class="nav-item">
            <a href="#account" class="nav-link" data-toggle="modal" data-target="#AccountProviderMoneyModal">
                <img class="mr-3" src="{{secure_asset('img/ic_mini_account.png')}}" alt="icon account" width="20">
                <span class="text-white">ຂໍ້​ມູນ​ບັນ​ຊີ</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#promotion-active" class="nav-link" data-toggle="modal" data-target="#PomotionModal">
                <img class="mr-3" src="{{secure_asset('img/ic_mini_promotion.png')}}" alt="icon promotion" width="20">
                <span class="text-white">ການສົ່ງເສີມການເຂົ້າຮ່ວມ</span>
            </a>
        </li>
        <li class="nav-item js-close-sidebar">
            <a href="#LogoutModal" data-toggle="modal" data-target="#LogoutModal" class="nav-link"
                data-title="ต้องการออกจากระบบ ?">
                <img class="mr-3" src="{{secure_asset('img/ic_mini_logout.png')}}" alt="icon logout" width="20">
                <span class="text-white">ອອກ​ຈາກ​ລະ​ບົບ</span>
            </a>
        </li>
    </ul>
</nav>

<!-- /slide menu right -->

@include('modals.logout')

<!-- Deposit Modal -->
<div class="modal fade custom-modal" id="depositModal" tabindex="-1" role="dialog" aria-labelledby="depositMoneyModal"
    aria-hidden="true">
    <div class="Modal-Deposit modal-dialog modal-dialog-centered modal-size">
        <div class="modal-content">
            <button type="button" class="close" onclick="close_deposit_modal()" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            <div class="modal-body">
                <form id="deposit_form">
                    @include('modals.deposit')
                </form>
            </div>
        </div>
    </div>
</div>

@include('modals.provider')
@include('modals.withdraw')
@include('modals.pomotion')

<script>
    // var removeMBig = document.getElementByClass("Modal-Deposit");
    // removeMBig.classList.remove("modal-big");

    $('.Modal-Deposit').removeClass('modal-big');
</script>

<script>
    var min_deposit = 1;
    var min_withdraw = 1;

    $('#contact').on('click', function () {
        window.open("{{config('variable.line')}}");
    });

    @if(!is_null($affliate) && !session('token'))
    $("#registerModal").modal();
    @endif

    @if($new)
    $("#depositModal").modal();
    @endif

    @if (isset($accountinfo['group']))
            min_deposit = {{ $accountinfo['group']['min_deposit'] ?? 1 }};
            min_withdraw = {{ $accountinfo['group']['min_withdraw'] ?? 1 }};
    @endif

    function Fx_refresh_credit() {
        $('#imgrefresh').addClass('rotateIn');
        setTimeout(() => {
            $('#imgrefresh').removeClass('rotateIn');
        }, 1000);
        $.ajax({
                url: "/api/get_balance",
                type: "GET",
                datatype: "call_credit",
            })
            .done(function (data) {
                var balance = data['current_balance'].toFixed(2)
                $('#user_credits').html(
                    new Intl.NumberFormat('en-US', {
                        minimumFractionDigits: 2,
                    }).format(
                        balance
                    ).toString()
                );
            })
            .fail(function (error) {
                console.log(error);
            });
    }

</script>

<script src="https://cdn.jsdelivr.net/npm/socket.io-client@2/dist/socket.io.js"></script>
<script>
    var socket = io.connect('https://www.LAOCASINO.xyz/', {
        secure: true
    });
    socket.on('connect', (data) => {
        socket.emit('join', '{{session("phone_number")}}')
    })

    socket.on('posting_result', (data) => {
        // deposit
        if (depNo == data['metadata']['number']) {
            data['metadata']['state'] == 'success' ? success_alert('ฝากเงินสำเร็จ', 1) : success_alert(
                'ฝากเงินไม่สำเร็จ', 0)
            getBalance();

            $.ajax({
                url: "/api/deposit_again",
                type: "GET"
            }).done(function (data) {
                console.log(data)
                clearInterval(launch_deposit);
                deposit_again(data)
            }).fail(function (error) {
                console.log(error);
            });
        }

        // withdraw
        if (withNo == data['metadata']['number']) {
            data['metadata']['state'] == 'success' ? success_alert('ถอนเงินสำเร็จ', 1) : success_alert(
                'ถอนเงินไม่สำเร็จ', 0)
            getBalance();
            withdraw_again();
        }
    })

</script>
