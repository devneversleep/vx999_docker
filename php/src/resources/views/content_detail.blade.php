@extends('layouts.main')

@section('content')

@if (Session::has('token'))
{{-- @include('layouts.logon') --}}
<header class="header">
    <nav class="navbar">
        <div class="container position-relative d-flex justify-content-between">
            <a class="navbar-brand" href="/"><img src="{{secure_asset('img/logo.png')}}" class="img-fluid logo"
                    alt="LAOCASINO logo-navbar"></a>
            <div class="navbar-right navbar-login logged-box">
                <div class="d-flex logged-container">
                    <div class="py-3 px-3 px-lg-4 profile">
                        <div class="d-flex">
                            <div class="mr-2 d-flex logged-sidebar" id="showRight">
                                <div class="profile-image">
                                    <img class="img-fluid mb-0" src="{{secure_asset('img/ic_balance.png')}}">
                                </div>
                            </div>
                            <div class="d-flex flex-column m-auto profile-detail f-7">
                                <div class="text-lg-right  text-white mb-1" id="showRight1">
                                    {{Session::get('phone_number')}}</div>
                                <div class="text-primary user-balance text-nowrap">
                                    <span id="customer-balance">
                                        <span id="user_credits"
                                            class="text-yellow">{{number_format($accountinfo['current_balance'], 2,'.',',')}}</span>
                                    </span>
                                    <button type="button" class="btn btn-link p-0" onclick="Fx_refresh_credit()">
                                        <img class="animated" id="imgrefresh" src="{{secure_asset('img/refresh-button.png')}}"
                                            width="16px">
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="dropdown-content">
                            <div class="profile-dropdown-menu px-3">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a href="#account" class="nav-link" data-toggle="modal"
                                            data-target="#AccountProviderMoneyModal" onclick="show_slide_provider()">
                                            <img class="mr-3" src="{{secure_asset('img/ic_mini_account.png')}}"
                                                alt="icon account" width="20">
                                            <span class="text-white">ຂໍ້​ມູນ​ບັນ​ຊີ</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#promotion-active" class="nav-link" data-toggle="modal"
                                            data-target="#PomotionModal">
                                            <img class="mr-3" src="{{secure_asset('img/ic_mini_promotion.png')}}"
                                                alt="icon promotion" width="20">
                                            <span class="text-white">ການສົ່ງເສີມການເຂົ້າຮ່ວມ</span>
                                        </a>
                                    </li>
                                    <li class="nav-item js-close-sidebar">
                                        <a href="#LogoutModal" class="nav-link" data-toggle="modal"
                                            data-target="#LogoutModal" data-title="ต้องการออกจากระบบ ?">
                                            <img class="mr-3" src="{{secure_asset('img/ic_mini_logout.png')}}"
                                                alt="icon logout" width="20">
                                            <span class="text-white">ອອກ​ຈາກ​ລະ​ບົບ</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div><!-- dropdown-content -->
                    </div><!-- profile -->
                    <div class="deposit d-none d-lg-block">
                        <a href="#deposit" class="text-white" data-toggle="modal"
                            data-target="#depositModal">
                            <div class="d-flex flex-column justify-content-center h-100 px-3 -border-left">
                                <img class="img-fluid mt-2" src="{{secure_asset('img/ic_deposit.png')}}"
                                    alt="icon deposit white" width="40">
                                <div class="f-7 mt-1 text-white">ຝາກເງິນ</div>
                            </div>
                        </a>
                    </div><!-- deposit -->
                    <div class="withdraw d-none d-lg-block">
                        <a href="#withdraw" class="text-white" data-toggle="modal" data-target="#withdrawModal">
                            <div class="d-flex flex-column justify-content-center h-100 px-3 -border-left">
                                <img class="img-fluid mt-2" src="{{secure_asset('img/ic_withdraw.png')}}"
                                    alt="icon deposit white" width="40">
                                <div class="f-7 mt-1 text-white">ຖອນເງິນ</div>
                            </div>
                        </a>
                    </div><!-- withdraw -->
                </div><!-- logged-container -->
            </div><!-- navbar-login -->
        </div>
    </nav>
</header>

<div class="fix-mobile d-lg-none d-block button-actions">
    <div class="d-flex">
        <a href="javascript:void(0);" data-toggle="modal" data-target="#depositModal"
            class="btn btn-lg btn-plain-primary">
            <img src="{{secure_asset('img/ic_deposit.png')}}" alt="LAOCASINO ฝากเงิน" class="icon img-fluid mr-2">
            <span class="typo">ຝາກເງິນ</span>
        </a>
        <a href="javascript:void(0);" data-toggle="modal" data-target="#withdrawModal"
            class="btn btn-lg btn-plain-secondary">
            <img src="{{secure_asset('img/ic_withdraw.png')}}" alt="LAOCASINO ถอนเงิน" class="icon img-fluid mr-2">
            <span class="typo text-white">ຖອນເງິນ</span>
        </a>
    </div>
</div>
<nav class="menu menu-right">
    <div class="mt-4 d-flex justify-content-between">
        <img src="{{secure_asset('img/logo_main.png')}}" style="width: 150px!important; height:90px;"><a href="#"
            class="backBtn f-1 d-inline-block">x</a>
    </div>

    <ul class="navbar-nav mt-5">
        <li class="nav-item">
            <a href="#account" class="nav-link" data-toggle="modal" data-target="#AccountProviderMoneyModal">
                <img class="mr-3" src="{{secure_asset('img/ic_mini_account.png')}}" alt="icon account" width="20">
                <span class="text-white">ຂໍ້​ມູນ​ບັນ​ຊີ</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#promotion-active" class="nav-link" data-toggle="modal" data-target="#PomotionModal">
                <img class="mr-3" src="{{secure_asset('img/ic_mini_promotion.png')}}" alt="icon promotion" width="20">
                <span class="text-white">ການສົ່ງເສີມການເຂົ້າຮ່ວມ</span>
            </a>
        </li>
        <li class="nav-item js-close-sidebar">
            <a href="#LogoutModal" data-toggle="modal" data-target="#LogoutModal" class="nav-link"
                data-title="ต้องการออกจากระบบ ?">
                <img class="mr-3" src="{{secure_asset('img/ic_mini_logout.png')}}" alt="icon logout" width="20">
                <span class="text-white">ອອກ​ຈາກ​ລະ​ບົບ</span>
            </a>
        </li>
    </ul>
</nav>

<!-- /slide menu right -->

@include('modals.logout')

<!-- Deposit Modal -->
<div class="modal fade custom-modal" id="depositModal" tabindex="-1" role="dialog" aria-labelledby="depositMoneyModal"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-size">
        <div class="modal-content">
            <button type="button" class="close" onclick="close_deposit_modal()" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            <div class="modal-body">
                <form id="deposit_form">
                    @include('modals.deposit')
                </form>
            </div>
        </div>
    </div>
</div>

@include('modals.provider')
@include('modals.withdraw')
@include('modals.pomotion')

<script>
    var min_deposit = 1;
    var min_withdraw = 1;

    $('#contact').on('click', function () {
        window.open("{{config('variable.line')}}");
    });

    @if (isset($accountinfo['group']))
            min_deposit = {{ $accountinfo['group']['min_deposit'] ?? 1 }};
            min_withdraw = {{ $accountinfo['group']['min_withdraw'] ?? 1 }};
    @endif

    function Fx_refresh_credit() {
        $('#imgrefresh').addClass('rotateIn');
        setTimeout(() => {
            $('#imgrefresh').removeClass('rotateIn');
        }, 1000);
        $.ajax({
                url: "/api/get_balance",
                type: "GET",
                datatype: "call_credit",
            })
            .done(function (data) {
                var balance = data['current_balance'].toFixed(2)
                $('#user_credits').html(
                    new Intl.NumberFormat('en-US', {
                        minimumFractionDigits: 2,
                    }).format(
                        balance
                    ).toString()
                );
            })
            .fail(function (error) {
                console.log(error);
            });
    }

</script>

<script src="https://cdn.jsdelivr.net/npm/socket.io-client@2/dist/socket.io.js"></script>
<script>
    var socket = io.connect('https://www.icasino.bet/', {
        secure: true
    });
    socket.on('connect', (data) => {
        socket.emit('join', '{{session("phone_number")}}')
    })

    socket.on('posting_result', (data) => {
        // deposit
        if (depNo == data['metadata']['number']) {
            data['metadata']['state'] == 'success' ? success_alert('ฝากเงินสำเร็จ', 1) : success_alert(
                'ฝากเงินไม่สำเร็จ', 0)
            getBalance();

            $.ajax({
                url: "/api/deposit_again",
                type: "GET"
            }).done(function (data) {
                console.log(data)
                clearInterval(launch_deposit);
                deposit_again(data)
            }).fail(function (error) {
                console.log(error);
            });
        }

        // withdraw
        if (withNo == data['metadata']['number']) {
            data['metadata']['state'] == 'success' ? success_alert('ถอนเงินสำเร็จ', 1) : success_alert(
                'ถอนเงินไม่สำเร็จ', 0)
            getBalance();
            withdraw_again();
        }
    })

</script>
@else
{{-- @include('layouts.nonlogin') --}}
<header class="header">
    <nav class="navbar">
        <div class="container  d-flex justify-content-between">
            <a class="navbar-brand" href="/"><img src="{{ secure_asset('img/logo.png') }}" class="img-fluid logo"
                    alt="LAOCASINO logo-navbar"></a>
            <div class="navbar-right navbar-login">
                <div class="d-flex">
                    {{-- <a href="/lobby" class="btn btn-primary-trytoplay d-lg-block d-none mx-2 px-lg-4 px-4">
                        <i class="fas fa-play mr-2" style="font-size:18px"></i> ทดลองเล่น</a> --}}
                    <a data-target="#register_modal" data-toggle="modal" class="btn-image hoverable btn-glow btn-register blog- d-lg-block mr-3 -btn-img-outer-glow">
                        <img class="default blog" src= "{{secure_asset('img/btn_register.png')}}" alt="LAOCASINO register">
                        <img class="hover blog" src="{{secure_asset('img/btn_register_hover.png')}}" alt="LAOCASINO register hover">
                    </a>
                    <a href="#loginModal" data-toggle="modal" class="btn btn-primary-modal mx-2 px-lg-5 px-4 d-none d-lg-block">
                        {{-- <i class="fas fa-sign-in-alt mr-2"></i>  --}}
                        เข้าสู่ระบบ</a>
                </div>
            </div><!-- navbar-login -->
        </div>
    </nav>
</header>

<section>
    <div class="button-actions">
        <div class="d-flex align-items-end">
            <a href="#loginModal" data-toggle="modal" class="btn btn-lg btn-primary flex-grow-1 pb-0">
                <i class="fas fa-sign-in-alt mr-2"></i> เข้าสู่ระบบ
            </a>
        </div>
    </div>
</section>

@include('modals.login')
@include('modals.forgetpass')
@endif

<div class="modal fade custom-modal" id="register_modal" tabindex="-1" role="dialog"
    aria-labelledby="registerModalLabel" aria-hidden="true">
    @include('modals.register1')
</div>

<div class="container justify-content-center ">

    <div class="row justify-content-center">
        <div class="col-12 blog mb-5">
            <div>
                <h3>
                    <a href="/blog" class="-btn-back">
                        <i class="fas fa-chevron-circle-left pr-2" style="font-size: 20px;"></i>
                        ย้อนกลับ
                    </a>
                </h3>
            </div>
            <div class="d-flex justify-content-center">
                <div class="text-content">
                    <h1 class="text-white -header-content">{{ $content['title'] }}</h1>
                </div>
            </div>
            <hr style="border: 1px solid;">
            <div class="pt-4">
                <img src="{{ $content['image'] }}" class="-img-content-detail" alt="{{ $content['image_alt'] }}">
            </div>
            <div class="-text-contents-detail p-lg-5 p-4">
                {!! $content['content'] !!}
            </div>
        </div>
    </div>
</div>

@endsection
