<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>LAOCASINO</title>
        <link rel="canonical" href="{{ Request::url() }}">

        <link rel="manifest" href="{{secure_asset('site.webmanifest')}}">
        <!-- <link rel="apple-touch-icon" href="icon.png"> -->
        <link rel="icon" href="{{secure_asset('favicon-32x32.png')}}" type="image/x-icon">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="{{secure_asset('css/styles.css?v=' . time())}}">
        <link rel="stylesheet" href="{{secure_asset('css/animate.css')}}">

        <link rel="preload" as="font" href="/fonts/DB HelvethaicaMon X Med.f4018213.ttf" type="font/ttf"
            crossorigin="anonymous">
        <link rel="preload" as="font" href="/fonts/DB HelvethaicaMon X.cfd3be53.ttf" type="font/ttf"
            crossorigin="anonymous">
        <link rel="stylesheet" href="{{secure_asset('css/owl.carousel.min.css')}}">

        {{-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/> --}}
        {{-- <link rel="stylesheet" type="text/css" href="{{secure_asset('css/slick.css')}}"/> --}}

        <!-- <link rel="stylesheet" type="text/css" href="slick/slick-theme.css" /> -->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script>
            window.jQuery || document.write('<script src="{{secure_asset("js/vendor/jquery-3.3.1.min.js")}}"><script>')

        </script>

        <script type="text/javascript" src="{{secure_asset('js/vendor/popper.min.js')}}"></script>
        <script type="text/javascript" src="{{secure_asset('js/vendor/bootstrap.min.js')}}"></script>

        <script type="text/javascript" src="{{secure_asset('js/plugins.js')}}"></script>
        <script type="text/javascript" src="{{secure_asset('js/vendor/owl.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{secure_asset('js/main.js?v='.time())}}"></script>

        {{-- <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> --}}
        {{-- <script type="text/javascript" src="{{secure_asset('js/vendor/slick.min.js')}}"></script> --}}

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <link rel="stylesheet" href="{{secure_asset('css/font-awesome.min.css')}}" />

        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        </script>
    </head>
    <body class="bg-exchange">
        <div class="nav-admin">
            <nav class="navbar navbar-expand-lg navbar-light bg-light mt-0 nav-bg-exchange">
                <div class="container">
                    <a class="navbar-brand" href="/">
                        <img class="logo-exchange" src="{{secure_asset('img/logo.png')}}" alt="">
                    </a>
                    <button class="navbar-toggler -admin" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon">
                            <i class="fas fa-bars -admin"></i>
                        </span>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav w-100 -exchange">
                            <li class="nav-item active mx-2">
                                <a class="nav-link text-white" href="exchange-rate">ตั้งค่าเรทค่าเงิน </a>
                                <hr class="hr-active m-0">
                            </li>
                            <li class="nav-item mx-2">
                                <a class="nav-link text-white" href="select_bank">บัญชีขาเข้า</a>
                                <hr class="hr-active m-0">
                            </li>
                            <li class="nav-item  ml-2 logout-admin">
                                <a class="nav-link text-white"  data-toggle="modal" data-target="#LogoutModal-Admin">
                                    <i class="fas fa-power-off"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="zone-exchange">
                        <div class="justify-content-center in-exchange">
                            <div class="box-text-bath w-100 m-auto text-center">
                                <span id="text_th_exchange_rate" >1 THB = {{ $exchange_rate->thai_1_bath_to_laos }} LAK </span>
                                <!-- <span id="text_lao_exchange_rate" class="text-yellow">( 1 ບາດ = {{ $exchange_rate->thai_1_bath_to_laos }} ກີບ )</span> -->
                            </div>
                            <form action="">
                                <div class="d-flex justify-content-center mt-4 box-all-exchange">
                                    <div class="exhange-lao">
                                        <div class="flag-all-th-lao">
                                            <div class="text-center">
                                                <img src="{{secure_asset('img/ic_mini_LAK.png')}}" alt="">
                                            </div>
                                            <div class="ic_exchange_hide">
                                                <i class="fas fa-exchange-alt"></i>
                                            </div>
                                            <div class="text-center f-th">
                                                <img src="{{secure_asset('img/ic_mini_THB.png')}}" alt="">
                                            </div>
                                        </div>
                                        <div class="mt-4 position-relative">
                                            <input type="number" id="exchange_rate" value="{{ $exchange_rate->thai_1_bath_to_laos }}"
                                                class="custom-form-control form-control px-4" placeholder="ใส่จำนวนเงินเป็นกีบ">
                                            <span class="item-lak">₭</span>
                                        </div>
                                    </div>
                                    <div class="ic_exchange">
                                        <i class="fas fa-exchange-alt"></i>
                                    </div>
                                    <div class="exhange-thai">
                                        <div class="text-center">
                                            <img src="{{secure_asset('img/ic_mini_THB.png')}}" alt="">
                                        </div>
                                        <div class="mt-4 text-center">
                                            <span class="f-2">
                                                1 Baht
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center box_exchange_rate">
                                    <button type="button" id="btn_exchange_rate" class="btn btn-primary-modal d-block btn-lg btn-submit -exchange mx-auto mt-5">
                                        ยืนยัน
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {{-- <div class="modal custom-modal d-block" >
        <div class="modal-dialog modal-dialog-centered modal-size">
            <div class="modal-content">
                
                <div class="modal-body">
                    <form action="/api/login" method="POST">
                        <div class="text-center d-flex flex-column">
                            <h3 class="custom-modal-title mb-3 f-3">Exchange Rate </h3>
                            <div class="mb-3">
                                <span class="text-white">1 THB = {{ $exchange_rate->thai_1_bath_to_laos }} LAK</span>
                            </div>
                            <input class="custom-form-control form-control " type="number" id="exchange_rate" value="{{ $exchange_rate->thai_1_bath_to_laos }}">
                            
                            
                            <div class="text-center mt-3">
                                <button type="button" id="btn_exchange_rate" value="Submit" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
                                    ยืนยัน
                                </button>
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}


        {{-- <h1>The input element</h1>

        <form>
            <label>Exchange Rate:</label>
            <input type="number" id="exchange_rate" value="{{ $exchange_rate->thai_1_bath_to_laos }}"><br><br>
            <label for="lname">Secret Key:</label>
            <input type="text" id="key"><br><br>
            <input id="btn_exchange_rate" type="button" value="Submit">
        </form> --}}
        <div class="modal fade custom-modal" id="LogoutModal-Admin" tabindex="-1" role="dialog" aria-labelledby="LogoutModal-Admin"
        aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-size">
                <div class="modal-content">
                    <div class="modal-body">
                        <form action="/api/logout-admin" method="post">
                            <div class="text-center d-flex flex-column">
                                <div class="my-3 flex-column text-center">
                                    <p class="f-5 text-yellow">คุณต้องการออกจากระบบ ?</p>
                                </div>
                                <div class="row">
                                    <div class="col-6 d-flex align-items-stretch">
                                        <button type="summit" class="btn btn-primary-dark-cancel w-100">ตกลง</button>
                                    </div>
                                    <div class="col-6 d-flex align-items-stretch">
                                        <button type="button" class="btn btn-primary-dark-logout  w-100" data-dismiss="modal"
                                            aria-label="Close">ยกเลิก</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{secure_asset('js/exchange_rate.js?v=' . time())}}"></script>
    </body>
</html>
<script>
    var id = {!! $exchange_rate->id !!};
</script>