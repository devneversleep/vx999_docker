
<hr class="hr-border-glow my-0">
<section class="section-two">
    <div class="index-tab-container">
        <div class="container">
            <ul class="nav nav-tabs tab py-3">
                <li class="nav-item  active index js-tab-scrolled" id="tab-index">
                    <a data-toggle="tab" href="#tab-content-index" class="nav-link active">
                        <img src="{{secure_asset('img/tab_index.png')}}" alt="logo_index" class="ic"><br>
                        <span class="d-sm-none d-inline-block mt-2 text-gray-lighter">Goodgame</span>
                        <span class="d-sm-inline-block d-none mt-2 text-gray-lighter">Goodgame</span>
                        <hr class="hr-border-glow mb-2">
                    </a>
                </li>
                <li class="nav-item  promotion js-tab-scrolled" id="tab-promotion">
                    <a data-toggle="tab" href="#tab-content-promotion" class="nav-link ">
                        <img src="{{secure_asset('img/tab_promotion.png')}}" alt="logo_promotion" class="ic"><br>
                        <span class="d-sm-none d-inline-block mt-2 text-gray-lighter">โปรโมชั่น</span>
                        <span class="d-sm-inline-block d-none mt-2 text-gray-lighter">โปรโมชั่น</span>
                        <hr class="hr-border-glow mb-2">
                    </a>
                </li>
                <li class="nav-item manual js-tab-scrolled" id="tab-manual">
                    <a data-toggle="tab" href="#tab-content-manual" class="nav-link ">
                        <img src="{{secure_asset('img/tab_manual.png')}}" alt="logo_manual" class="ic"><br>
                        <span class="d-sm-none d-inline-block mt-2 text-gray-lighter">แนะนำ</span>
                        <span class="d-sm-inline-block d-none mt-2 text-gray-lighter">แนะนำการใช้งาน</span>
                        <hr class="hr-border-glow mb-2">
                    </a>
                </li>
                <li class="nav-item event js-tab-scrolled" id="tab-event">
                    <a data-toggle="tab" href="#tab-content-event" class="nav-link ">
                        <img src="{{secure_asset('img/tab_event.png')}}" alt="logo_event" class="ic"><br>
                        <span class="d-sm-none d-inline-block mt-2 text-gray-lighter">กิจกรรม</span>
                        <span class="d-sm-inline-block d-none mt-2 text-gray-lighter">กิจกรรม</span>
                        <hr class="hr-border-glow mb-2">
                    </a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="tab-content-index">
                <div class="tab-index">
                    <div class="container mt-5 mb-3">
                        <div class="notice-box top">
                            <div class="title">
                                <img src="{{secure_asset('LAOCASINO/logo_SA.png')}}" alt="LAOCASINO logo">
                            </div>
                            <div class="description">
                                <div class="row m-0 py-5">
                                    <div class="col-lg-12 text-white position-relative">
                                        <div class="footer-lobby-logo">
                                            <div class="container">
                                                <div class="casino-wrapper">
                                                    <ul class="nav-container">
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <a href="/lobby" target="_blank">
                                                                <img src="https://core.igame.bet/build/admin/img/lobby_main/sa-gaming-logo-circle.png"
                                                                    alt="" class="logo-circle">
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <a href="/lobby" target="_blank">
                                                                <img src="https://core.igame.bet/build/admin/img/lobby_main/wm-logo-circle.png"
                                                                    alt="" class="logo-circle">
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <a href="/lobby" target="_blank">
                                                                <img src="https://core.igame.bet/build/admin/img/lobby_main/bbin-logo-circle.png"
                                                                    alt="" class="logo-circle">
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <a href="/lobby" target="_blank">
                                                                <img src="https://core.igame.bet/build/admin/img/lobby_main/sexy-bac-logo-circle.png"
                                                                    alt="" class="logo-circle">
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <a href="/lobby" target="_blank">
                                                                <img src="https://core.igame.bet/build/admin/img/lobby_main/netent-live-logo-circle.png"
                                                                    alt="" class="logo-circle">
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <a href="/lobby" target="_blank">
                                                                <img src="https://core.igame.bet/build/admin/img/lobby_main/dream-gaming-logo-circle.png"
                                                                    alt="" class="logo-circle">
                                                            </a>
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <a href="/lobby" target="_blank">
                                                                <img src="https://core.igame.bet/build/admin/img/lobby_main/asia-gaming-logo-circle.png"
                                                                    alt="" class="logo-circle">
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- casino-wrapper -->
                                                <div class="slot-wrapper mt-3">
                                                    <ul class="nav-container">
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <div class="cv-box">
                                                                <a href="/lobby" target="_blank">
                                                                    <img src="https://core.igame.bet/build/admin/img/lobby_main/evo-play-cover-vertical.png"
                                                                        alt="evo-play cover vertical"
                                                                        class="img-fluid cover-vertical">
                                                                </a>
                                                            </div>
                                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/evo-play-logo-horizontal.png"
                                                                alt="evo-play logo horizontal"
                                                                class="img-fluid logo-horizontal">
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <div class="cv-box">
                                                                <a href="/lobby" target="_blank">
                                                                    <img src="https://core.igame.bet/build/admin/img/lobby_main/ps-cover-vertical.png"
                                                                        alt="ps cover vertical"
                                                                        class="img-fluid cover-vertical">
                                                                </a>
                                                            </div>
                                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/ps-logo-horizontal.png"
                                                                alt="ps logo horizontal"
                                                                class="img-fluid logo-horizontal">
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <div class="cv-box">
                                                                <a href="/lobby" target="_blank">
                                                                    <img src="https://core.igame.bet/build/admin/img/lobby_main/sp-cover-vertical.png"
                                                                        alt="sp cover vertical"
                                                                        class="img-fluid cover-vertical">
                                                                </a>
                                                            </div>
                                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/sp-logo-horizontal.png"
                                                                alt="sp logo horizontal"
                                                                class="img-fluid logo-horizontal">
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <div class="cv-box">
                                                                <a href="/lobby" target="_blank">
                                                                    <img src="https://core.igame.bet/build/admin/img/lobby_main/netent-slot-cover-vertical.png"
                                                                        alt="netent-slot cover vertical"
                                                                        class="img-fluid cover-vertical">
                                                                </a>
                                                            </div>
                                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/netent-slot-logo-horizontal.png"
                                                                alt="netent-slot logo horizontal"
                                                                class="img-fluid logo-horizontal">
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <div class="cv-box">
                                                                <a href="/lobby" target="_blank">
                                                                    <img src="https://core.igame.bet/build/admin/img/lobby_main/kingmaker-cover-vertical.png"
                                                                        alt="kingmaker cover vertical"
                                                                        class="img-fluid cover-vertical">
                                                                </a>
                                                            </div>
                                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/kingmaker-logo-horizontal.png"
                                                                alt="kingmaker logo horizontal"
                                                                class="img-fluid logo-horizontal">
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <div class="-cv-box">
                                                                <a href="/lobby" target="_blank">
                                                                    <img src="https://core.igame.bet/build/admin/img/lobby_main/joker-cover-vertical.png"
                                                                        alt="joker cover vertical"
                                                                        class="img-fluid cover-vertical">
                                                                </a>
                                                            </div>
                                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/joker-logo-horizontal.png"
                                                                alt="joker logo horizontal"
                                                                class="img-fluid logo-horizontal">
                                                        </li>
                                                        <li class="nav-item" style="width: calc(100% / 8 )">
                                                            <div class="cv-box">
                                                                <a href="/lobby" target="_blank">
                                                                    <img src="https://core.igame.bet/build/admin/img/lobby_main/habanero-cover-vertical.png"
                                                                        alt="habanero cover vertical"
                                                                        class="img-fluid cover-vertical">
                                                                </a>
                                                            </div>
                                                            <img src="https://core.igame.bet/build/admin/img/lobby_main/habanero-logo-horizontal.png"
                                                                alt="habanero logo horizontal"
                                                                class="img-fluid logo-horizontal">
                                                        </li>
                                                    </ul>
                                                </div><!-- slot-wrapper -->
                                            </div>
                                        </div><!-- footer-lobby-logo -->
                                    </div>
                                </div>
                            </div><!-- description -->
                        </div>
                    </div>
                    <div class="container mt-4">
                        <div class="notice-box">
                            <div class="description text-center">
                                <div class="row m-0 py-3">
                                    <div class="col-lg-12 text-white position-relative">
                                        <h3 class="text-primary mb-0">GoodGame 365</h3>
                                        <hr class="hr-border-glow">
                                        <p class="f-6 f-lg-5 px-4"><b>GoodGame 365</b>
                                            เราเป็นเว็บที่ให้เล่นคาสิโนออนไลน์ในรูปแบบมือถือ และบนคอมพิวเตอร์ มี
                                            บาคาร่า เกมส์สล็อต เสือ มังกร มีให้เล่นมากในเกมส์ <b>GoodGame 365</b>
                                            สามารถเลือกเล่นได้หลากหลายรูปแบบ มีระบบฝากถอน เงิน ออโต้
                                            สามารถทำรายการไม่เกิน 1 นาที ไม่ต้องมารอเติมเงินให้เสียเวลาสามารถ ฝาก
                                            ถอนได้ตลอดเวลา 24 ชั่วโมง เว็บ www.goodgame365.bet เล่นบาคาร่าออนไลน์
                                            เล่นคาสิโนออนไลน์ ผ่านเว็บง่ายๆ</p>
                                        <p class="f-6 f-lg-5 pt-3 px-4">วันนี้เข้ามาในไทยร่วม 10 ปี
                                            ที่ไม่หยุดพัฒนาเพื่อประสบการณ์ต่อผู้เล่น ที่สะดวก เสถียรที่สุด
                                            รองรับทั้งมือถือ และ คอมพิวเตอร์ โดยไม่ต้องดาวน์โหลดใดๆ
                                            อีกทั้งยังมีระบบฝากถอนออโต้ <span
                                                class="text-primary">เจ้าเดียวที่ใช้ได้จริง</span> ทำให้ GoodGame
                                            365 เป็นที่นิยมแพร่หลายจนถึงทุกวันนี้ ซึ่งผู้ใดอยากจะเดิมพันคาสิโน
                                            ทั้งเกม <i class="text-primary"><u>บาคาร่า ไฮโล ป็อคเด้ง เสือ-มังกร
                                                    หรือรูเล็ตแล้ว ต้องนึกถึง GoodGame 365 เป็นเว็บแรก</u></i></p>
                                    </div>
                                </div>
                            </div><!-- description -->
                        </div>
                    </div>
                    <div class="bottom py-3">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-lg-7 mt-2 order-1 d-lg-block d-none">
                                    <div class="scanner d-flex">
                                        <img src="{{secure_asset('img/lobby.png')}}" alt="สแกนเพื่อทดลองเล่น"
                                            class="logo mr-3">
                                        <div class="detail text-center px-2 w-100">
                                            <h3 class="f-xl-4 f-6 mb-0">สแกนเพื่อทดลองเล่น</h3>
                                            <span class="text-white">Compatible with iOS &amp; Android
                                                Devices</span>
                                            <div class="text-center">
                                                <i class="fab fa-apple mr-3"></i>
                                                <i class="fab fa-android"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-5 order-0 order-lg-1">
                                    <div class="streaming mt-lg-3 mt-xl-0">
                                        <img src="{{secure_asset('img/mobile_hd.png')}}" data-wow-duration="0.3s"
                                            alt="Mobile HD" class="image wow fadeInUp">
                                        <div>
                                            <div class="text-white text mt-1 wow fadeInUp" data-animatable="fadeInUp"
                                                data-wow-duration="0.5s">
                                                <span class="text-detail d-block f-6 f-sm-5">HD Live
                                                    streaming</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer-logo-lobby-in-used">
                                <!-- <ul class="navbar-nav">
                                    <li class="nav-item logo-white"
                                        style="background-image: url({{secure_asset('img/footer-all-netent.png')}})"></li>
                                    <li class="nav-item logo-white"
                                        style="background-image: url({{secure_asset('img/footer-all-wm.png')}})"></li>
                                    <li class="nav-item logo-white"
                                        style="background-image: url({{secure_asset('img/footer-all-dream-gaming.png')}})">
                                    </li>
                                    <li class="nav-item logo-white"
                                        style="background-image: url({{secure_asset('img/footer-all-pinnacle.png')}})"></li>
                                    <li class="nav-item logo-white"
                                        style="background-image: url({{secure_asset('img/footer-all-sp.png')}})"></li>
                                    <li class="nav-item logo-white"
                                        style="background-image: url({{secure_asset('img/footer-all-ps.png')}})"></li>
                                    <li class="nav-item logo-white"
                                        style="background-image: url({{secure_asset('img/footer-all-evo-play.png')}})"></li>
                                    <li class="nav-item logo-white"
                                        style="background-image: url({{secure_asset('img/footer-all-skill-game.png')}})"></li>
                                    <li class="nav-item logo-white"
                                        style="background-image: url({{secure_asset('img/footer-all-habanero.png')}})"></li>
                                    <li class="nav-item logo-white"
                                        style="background-image: url({{secure_asset('img/footer-all-joker.png')}})"></li>
                                    <li class="nav-item logo-white"
                                        style="background-image: url({{secure_asset('img/footer-all-sexy-bac.png')}})"></li>
                                </ul> -->
                            </div><!-- footer-logo-lobby-in-used -->
                        </div>
                    </div><!-- bottom -->
                </div>
            </div><!-- tab-content-index -->
            <div class="tab-pane " id="tab-content-promotion">
                <div class="intro-promotion">
                    <div class="slider slick-promotion-img">
                        <div class="d-flex mx-3 flex-column">
                            <div class="slider-cover">
                                <img src="{{secure_asset('images/slick-1.png')}}" class="mb-3">
                            </div>
                        </div>
                        <div class="d-flex mx-3 flex-column">
                            <div class="slider-cover">
                                <img src="{{secure_asset('images/slick-2.png')}}" class="mb-3">
                            </div>
                        </div>
                        <div class="d-flex mx-3 flex-column">
                            <div class="slider-cover">
                                <img src="{{secure_asset('images/slick-3.png')}}" class="mb-3">
                            </div>
                        </div>
                        <div class="d-flex mx-3 flex-column" class="mb-3">
                            <div class="slider-cover">
                                <img src="{{secure_asset('images/slick-1.png')}}" class="mb-3">
                            </div>
                        </div>
                        <div class="d-flex mx-3 flex-column">
                            <div class="slider-cover">
                                <img src="{{secure_asset('images/slick-1.png')}}" class="mb-3">
                            </div>
                        </div>
                    </div><!-- slick-promotion-img --->
                    <div class="slider slick-promotion-text">
                        <div class="d-flex mx-3 flex-column">
                            <div class="slick-content">
                                <div class="d-flex flex-column my-5">
                                    <div class="mb-3 promotion-content">
                                        <p>สมาชิกใหม่ฝากครั้งแรก รับโบนัสฟรี 20% สูงสุด 5,000 บาท&nbsp;<br>(
                                            ทำยอดเล่น 10 เท่า ของยอดฝาก+โบนัส )</p>
                                    </div>
                                </div>
                            </div>
                            <div class="m-auto btn-container">
                                <button class="btn btn-primary btn-block">รับสิทธิ์</button>
                            </div>
                        </div>
                        <div class="d-flex mx-3 flex-column">
                            <div class="slick-content">
                                <div class="d-flex flex-column my-5">
                                    <div class="mb-3 promotion-content">
                                        <p>22222222222222</p>
                                    </div>
                                </div>
                            </div>
                            <div class="m-auto btn-container">
                            </div>
                        </div>
                        <div class="d-flex mx-3 flex-column">
                            <div class="slick-content">
                                <div class="d-flex flex-column my-5">
                                    <div class="mb-3 promotion-content">
                                        <p>333333333333333</p>
                                    </div>
                                </div>
                            </div>
                            <div class="m-auto btn-container">
                            </div>
                        </div>
                        <div class="d-flex mx-3 flex-column">
                            <div class="slick-content">
                                <div class="d-flex flex-column my-5">
                                    <div class="mb-3 promotion-content">
                                        <p>4444444444</p>
                                    </div>
                                </div>
                            </div>
                            <div class="m-auto btn-container">
                            </div>
                        </div>
                        <div class="d-flex mx-3 flex-column">
                            <div class="slick-content">
                                <div class="d-flex flex-column my-5">
                                    <div class="mb-3 promotion-content">
                                        <p>5555555555555555</p>
                                    </div>
                                </div>
                            </div>
                            <div class="m-auto btn-container">
                            </div>
                        </div>
                    </div><!-- slick-promotion-text -->
                </div>
            </div><!-- tab-content-promotion -->
            <div class="tab-pane " id="tab-content-manual">
                <div class="tab-manual">
                    <div class="container container-wrapper pt-0">
                        <ul class="nav nav-tabs tabs d-flex justify-content-center video-tab-wrapper">
                            <li class="nav-item  active register" id="tab-register">
                                <a data-toggle="tab" href="#tab-content-register" class="nav-link active">
                                    วิธีการสมัคร
                                </a>
                            </li>
                            <li class="nav-item deposit" id="tab-deposit">
                                <a data-toggle="tab" href="#tab-content-deposit" class="nav-link ">
                                    วิธีการฝากเงิน
                                </a>
                            </li>
                            <li class="nav-item withdraw" id="tab-withdraw">
                                <a data-toggle="tab" href="#tab-content-withdraw" class="nav-link ">
                                    วิธีการถอนเงิน
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content text-center mt-2">
                            <div class="tab-pane active video-container" id="tab-content-register">
                                <div class="service-wrapper mt-2 pt-2">
                                    <div class="row justify-content-center mt-4">
                                        <div
                                            class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                            <a href="#0" class="d-flex flex-md-column flex-row" data-toggle="modal"
                                                data-target="#registerModal">
                                                <div class="ic-wrapper mb-2">
                                                    <img src="{{secure_asset('img/ic_register.png')}}" alt="icon register"
                                                        class="ic-register">
                                                </div>
                                                <div class="text-left text-md-center">
                                                    <h3><span class="d-inline-block d-md-none">1.</span> สมัครสมาชิก
                                                    </h3>
                                                    <hr class="hr-border-glow">
                                                    <span
                                                        class="d-none d-lg-block text-muted-lighter f-5">กรอกเบอร์โทรศัพท์มือถือ
                                                        ของคุณให้ถูกต้อง</span>
                                                    <span
                                                        class="d-block d-lg-none text-muted-lighter f-5">กรอกเบอร์โทรศัพท์มือถือ
                                                        ของคุณให้ถูกต้อง</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div
                                            class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                            <div class="ic-wrapper mb-2">
                                                <img src="{{secure_asset('img/ic_otp.png')}}" alt="icon register"
                                                    class="ic-otp">
                                            </div>
                                            <div class="text-left text-md-center">
                                                <h3><span class="d-inline-block d-md-none">2.</span> รอรับ SMS
                                                    ยืนยัน</h3>
                                                <hr class="hr-border-glow">

                                                <span class="d-none d-lg-block text-muted-lighter f-5">กรอกเลข OTP
                                                    ให้ถูกต้อง พร้อมตั้งรหัสเข้าเล่น</span>
                                                <span class="d-block d-lg-none text-muted-lighter f-5">กรอกเลข OTP
                                                    ให้ถูกต้อง พร้อมตั้งรหัสเข้าเล่น</span>
                                            </div>
                                        </div>
                                        <div
                                            class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                            <div class="ic-wrapper mb-2">
                                                <img src="{{secure_asset('img/ic_bank.png')}}" alt="icon register"
                                                    class="ic-bank">
                                            </div>
                                            <!--ic-wrapper -->
                                            <div class="text-left text-md-center">
                                                <h3><span class="d-inline-block d-md-none">3.</span>
                                                    ใส่เลขบัญชีและชื่อบัญชี</h3>
                                                <hr class="hr-border-glow">
                                                <span
                                                    class="d-none d-lg-block text-muted-lighter f-5">กรอกเลขบัญชีของคุณพร้อมชื่อให้ถูกต้อง
                                                    เข้าร่วมสนุกกับ GoodGame 365 ได้ทันที !</span>
                                                <span
                                                    class="d-block d-lg-none text-muted-lighter f-5">กรอกเลขบัญชีของคุณพร้อมชื่อให้ถูกต้อง
                                                    เข้าร่วมสนุกกับ GoodGame 365 ได้ทันที !</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="video-outer-wrapper js-video-loaded"
                                        data-source="/build/web/goodgame365/videos/register.mp4">
                                        <img src="{{secure_asset('img/video_register_bg.png')}}" alt="register-background"
                                            class="img-fluid video-bg">
                                        <div class="video-wrapper"></div>
                                    </div><!-- video-outer-wrapper -->
                                </div>
                                <!--service-wrapper-->
                            </div><!-- tab-content-register -->
                            <div class="tab-pane video-container" id="tab-content-deposit">
                                <div class="service-wrapper mt-2 pt-2">
                                    <div class="row justify-content-center mt-4">
                                        <div
                                            class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                            <a href="#0" class="d-flex flex-md-column flex-row" data-toggle="modal"
                                                data-target="#depositModal">
                                                <div class="ic-wrapper mb-2">
                                                    <img src="{{secure_asset('img/ic_chip_deposit.png')}}" alt="icon register"
                                                        class="ic">
                                                </div>
                                                <div class="text-left text-md-center">
                                                    <h3><span class="d-inline-block d-md-none">1.</span> กด
                                                        &quot;ฝากเงิน&quot;</h3>
                                                    <hr class="hr-border-glow">
                                                    <span class="d-none d-lg-block text-muted-lighter f-5">กรอกจำนวนเงิน
                                                        กด "ต้องการรับโปรโมชั่น"<br
                                                            class="d-none d-lg-block">เพื่อรับโบนัสที่ต้องการ กด
                                                        "ยืนยัน"</span>
                                                    <span class="d-block d-lg-none text-muted-lighter f-5">กรอกจำนวนเงิน
                                                        กด "ต้องการรับโปรโมชั่น"<br
                                                            class="d-none d-lg-block">เพื่อรับโบนัสที่ต้องการ กด
                                                        "ยืนยัน"</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div
                                            class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                            <div class="ic-wrapper mb-2">
                                                <img src="{{secure_asset('img/ic_step_deposit_add.png')}}" alt="icon register"
                                                    class="ic">
                                            </div>
                                            <div class="text-left text-md-center">
                                                <h3><span class="d-inline-block d-md-none">2.</span> โอนเงิน</h3>
                                                <hr class="hr-border-glow">
                                                <span class="d-none d-lg-block text-muted-lighter f-5">กด
                                                    "คัดลอกเลขบัญชี"<br class="d-none d-lg-block">หรือแสกน QR Code
                                                    เพื่อโอนเงิน</span>
                                                <span class="d-block d-lg-none text-muted-lighter f-5">กด
                                                    "คัดลอกเลขบัญชี"<br class="d-none d-lg-block">หรือแสกน QR Code
                                                    เพื่อโอนเงิน</span>
                                            </div>
                                        </div>
                                        <div
                                            class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                            <div class="ic-wrapper mb-2">
                                                <img src="{{secure_asset('img/ic_step_deposit_done.png')}}" alt="icon register"
                                                    class="ic">
                                            </div>
                                            <div class="text-left text-md-center">
                                                <h3><span class="d-inline-block d-md-none">3.</span>
                                                    กด&quot;โอนแล้ว&quot;</h3>
                                                <hr class="hr-border-glow">

                                                <span class="d-none d-lg-block text-muted-lighter f-5">ส่งรายการฝาก
                                                    ระบบจะทำการ<br class="d-none d-lg-block">ตรวจสอบเงิน
                                                    และเติมเงินให้ทันที</span>
                                                <span class="d-block d-lg-none text-muted-lighter f-5">ส่งรายการฝาก
                                                    ระบบจะทำการ<br class="d-none d-lg-block">ตรวจสอบเงิน
                                                    และเติมเงินให้ทันที</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="video-outer-wrapper js-video-loaded"
                                        data-source="{{secure_asset('/build/web/goodgame365/videos/register.mp4')}}">
                                        <img src="{{secure_asset('img/video_register_bg.png')}}" alt="register-background"
                                            class="img-fluid video-bg">
                                        <div class="video-wrapper"></div>
                                    </div><!-- video-outer-wrapper -->
                                </div>
                            </div><!-- tab-content-deposit -->
                            <div class="tab-pane video-container" id="tab-content-withdraw">
                                <div class="service-wrapper mt-2 pt-2">
                                    <div class="row justify-content-center mt-4">
                                        <div
                                            class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                            <a href="#0" class="d-flex flex-md-column flex-row" data-toggle="modal"
                                                data-target="#withdrawModal">
                                                <div class="ic-wrapper mb-2">
                                                    <img src="{{secure_asset('img/ic_step_withdraw.png')}}" alt="icon register"
                                                        class="ic">
                                                </div>
                                                <div class="text-left text-md-center">
                                                    <h3><span class="d-inline-block d-md-none">1.</span> กด
                                                        &quot;ถอนเงิน&quot;</h3>
                                                    <hr class="hr-border-glow">
                                                    <span class="d-none d-lg-block text-muted-lighter f-5">กด
                                                        "ถอนเงิน"</span>
                                                    <span class="d-block d-lg-none text-muted-lighter f-5">กด
                                                        "ถอนเงิน"</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div
                                            class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                            <div class="ic-wrapper mb-2">
                                                <img src="{{secure_asset('img/ic_step_withdraw_add.png')}}" alt="icon register"
                                                    class="ic">
                                            </div>
                                            <div class="text-left text-md-center">
                                                <h3><span class="d-inline-block d-md-none">2.</span> กรอกจำนวนเงิน
                                                </h3>
                                                <hr class="x-hr-border-glow">
                                                <span
                                                    class="d-none d-lg-block text-muted-lighter f-5">กรอกจำนวนเงินที่ต้องการถอน</span>
                                                <span
                                                    class="d-block d-lg-none text-muted-lighter f-5">กรอกจำนวนเงินที่ต้องการถอน</span>
                                            </div>
                                        </div>
                                        <div
                                            class="col-11 col-sm-9 col-md-4 text-center box d-flex align-items-start d-md-block">
                                            <div class="ic-wrapper mb-2">
                                                <img src="{{secure_asset('img/ic_step_withdraw_done.png')}}"
                                                    alt="icon register" class="ic">
                                            </div>
                                            <div class="text-left text-md-center">
                                                <h3><span class="d-inline-block d-md-none">3.</span> ยืนยัน</h3>
                                                <hr class="hr-border-glow">
                                                <span class="d-none d-lg-block text-muted-lighter f-5">กด "ยืนยัน"
                                                    ระบบจะทำการถอนเงินให้ทันที</span>
                                                <span class="d-block d-lg-none text-muted-lighter f-5">กด "ยืนยัน"
                                                    ระบบจะทำการถอนเงินให้ทันที</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="video-outer-wrapper js-video-loaded"
                                        data-source="{{secure_asset('/build/web/goodgame365/videos/register.mp4')}}">
                                        <img src="{{secure_asset('img/video_register_bg.png')}}" alt="register-background"
                                            class="img-fluid video-bg">
                                        <div class="video-wrapper"></div>
                                    </div><!-- video-outer-wrapper -->
                                </div>
                            </div><!-- tab-content-withdraw -->
                        </div>
                    </div>
                </div>
            </div><!-- tab-content-manual -->
            <div class="tab-pane " id="tab-content-event">
                <div class="container text-center">
                    <h3 class="text-white">กิจกรรม</h3>
                    <span class="text-white">ยังไม่มีข้อมูลในส่วนนี้</span>
                </div>
            </div><!-- tab-content-event -->
        </div>
    </div><!-- index-tab-container -->
</section><!-- section-two -->
