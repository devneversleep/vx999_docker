<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="canonical" href="{{ Request::url() }}">

        <link rel="manifest" href="{{secure_asset('site.webmanifest')}}">
        <!-- <link rel="apple-touch-icon" href="icon.png"> -->
        <link rel="icon" href="{{secure_asset('/img/favicon-32x32.png')}}" type="image/x-icon">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="{{secure_asset('css/styles.css?v=' . time())}}">
        <link rel="stylesheet" href="{{secure_asset('css/animate.css')}}">

        <link rel="preload" as="font" href="/fonts/DB HelvethaicaMon X Med.f4018213.ttf" type="font/ttf"
            crossorigin="anonymous">
        <link rel="preload" as="font" href="/fonts/DB HelvethaicaMon X.cfd3be53.ttf" type="font/ttf"
            crossorigin="anonymous">
        <link rel="stylesheet" href="{{secure_asset('css/owl.carousel.min.css')}}">

        {{-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/> --}}
        {{-- <link rel="stylesheet" type="text/css" href="{{secure_asset('css/slick.css')}}"/> --}}

        <!-- <link rel="stylesheet" type="text/css" href="slick/slick-theme.css" /> -->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script>
            window.jQuery || document.write('<script src="{{secure_asset("js/vendor/jquery-3.3.1.min.js")}}"><script>')

        </script>

        <script type="text/javascript" src="{{secure_asset('js/vendor/popper.min.js')}}"></script>
        <script type="text/javascript" src="{{secure_asset('js/vendor/bootstrap.min.js')}}"></script>

        <script type="text/javascript" src="{{secure_asset('js/plugins.js')}}"></script>
        <script type="text/javascript" src="{{secure_asset('js/vendor/owl.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{secure_asset('js/main.js?v='.time())}}"></script>

        {{-- <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> --}}
        {{-- <script type="text/javascript" src="{{secure_asset('js/vendor/slick.min.js')}}"></script> --}}

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <link rel="stylesheet" href="{{secure_asset('css/font-awesome.min.css')}}" />

        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        </script>
    </head>
    <body class="bg-exchange">

        <section class="section-pages">
            <div class="container -table-box mt-3">
                
                <table id="main_table" class="table table-hover">
                    <thead>
                        <tr class="bg-grey">                     
                            <th style="color:white;" scope="col">code</th>
                            <th style="color:white;" scope="col">name</th>
                            <th style="color:white;" scope="col">number</th> 
                            <th style="color:white;" scope="col">action</th>                      
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($banks as $bank)
                            <tr>                          
                                <td style="color:white;">{{ $bank->code }}</td>
                                <td style="color:white;">{{ $bank->name }}</td>
                                <td style="color:white;">{{ $bank->number }}</td>                          
                                <td>
                                <button type="button" onclick="changeActiveBanks({{ $bank->id }})" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
                                    add
                                </button>
                                </td>
                            </tr>
                        @endforeach      
                    </tbody>
                </table>
            </div>
            <!-- <div class="form-group">
                <label class="col-form-label">code</label>
                <input type="text" class="form-control" name="code" id="code" required>
            </div> -->
            <div class="form-group">
                <label class="col-form-label">name</label>
                <input type="text" class="form-control" name="name" id="name" required>
            </div>
            <div class="form-group">
                <label class="col-form-label">number</label>
                <input type="text" class="form-control" name="number" id="number" required>
            </div>
            <button type="button" id="btn_add_bank" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
                add
            </button>
        </section>

        <script src="{{secure_asset('js/banks.js?v=' . time())}}"></script>
    </body>
</html>