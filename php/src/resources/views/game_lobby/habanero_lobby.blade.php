<!DOCTYPE html>
<html lang="th" class="x-lobby-main-container-habanero">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>LAOCASINO</title>

    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-title" content="hanghangvip">
    <title>hanghangvip</title>
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{secure_asset('favicIcon/apple-touch-icon-57x57.png')}}" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
        href="{{secure_asset('favicIcon/apple-touch-icon-114x114.png')}}" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{secure_asset('favicIcon/apple-touch-icon-72x72.png')}}" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
        href="{{secure_asset('favicIcon/apple-touch-icon-144x144.png')}}" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{secure_asset('favicIcon/apple-touch-icon-60x60.png')}}" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120"
        href="{{secure_asset('favicIcon/apple-touch-icon-120x120.png')}}" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{secure_asset('favicIcon/apple-touch-icon-76x76.png')}}" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152"
        href="{{secure_asset('favicIcon/apple-touch-icon-152x152.png')}}" />
    <link rel="icon" type="image/png" href="{{secure_asset('favicIcon/favicon-196x196.png')}}" sizes="196x196" />
    <link rel="icon" type="image/png" href="{{secure_asset('favicIcon/favicon-96x96.png')}}" sizes="96x96" />
    <link rel="icon" type="image/png" href="{{secure_asset('favicIcon/favicon-32x32.png')}}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{secure_asset('favicIcon/favicon-16x16.png')}}" sizes="16x16" />
    <link rel="icon" type="image/png" href="{{secure_asset('favicIcon/favicon-128.png')}}" sizes="128x128" />
    <meta name="application-name" content="LAOCASINO" />
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="{{secure_asset('favicIconmstile-144x144.png')}}" />
    <meta name="msapplication-square70x70logo" content="{{secure_asset('favicIconmstile-70x70.png')}}" />
    <meta name="msapplication-square150x150logo" content="{{secure_asset('favicIconmstile-150x150.png')}}" />
    <meta name="msapplication-wide310x150logo" content="{{secure_asset('favicIconmstile-310x150.png')}}" />
    <meta name="msapplication-square310x310logo" content="{{secure_asset('favicIconmstile-310x310.png')}}" /> -->
    <link rel="manifest" href="{{secure_asset('site.webmanifest')}}">
    <meta name="application-name" content="LAOCASINO" />

    <link rel="icon" href="{{secure_asset('favicon-32x32.png')}}" type="image/x-icon">

    <link rel="stylesheet" href="{{ secure_asset('/css/all.css')}}"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="{{secure_asset('css/animate.css?v=' . time())}}" />

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="{{ secure_asset('/css/style.f800d0b0.css')}}">
    <script type="text/javascript">
        window['gif64'] = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
        window['Bonn'] = {
            boots: [],
            inits: []
        };

    </script>

    <style type="text/css" data-tippy-stylesheet="">
        @charset "UTF-8";

        @font-face {
            font-family: DB Helvethaica X;
            font-style: normal;
            font-weight: 300;
            font-display: fallback;
            src: url("{{secure_asset("fonts/DB HelvethaicaMon X.cfd3be53.ttf")}}") format("TrueType");
        }

        @font-face {
            font-family: DB Helvethaica X;
            font-style: normal;
            font-weight: 700;
            font-display: fallback;
            src: url("{{secure_asset("fonts/DB HelvethaicaMon X Med.f4018213.ttf")}}") format("TrueType");
        }

        .scrolling header .navbar {
            /*background-color: rgba(22,29,37,.8);*/
            padding-top: 0;
            padding-bottom: 0;
        }

        .scrolling header .navbar .navbar-brand img {
            opacity: 0;
        }

        .scrolling header .nav-welcome {
            background-color: rgba(22, 29, 37, 0.8) !important;
        }

        .tippy-iOS {
            cursor: pointer !important;
            -webkit-tap-highlight-color: transparent
        }

        .tippy-popper {
            transition-timing-function: cubic-bezier(.165, .84, .44, 1);
            max-width: calc(100% - 8px);
            pointer-events: none;
            outline: 0
        }

        .tippy-popper[x-placement^=top] .tippy-backdrop {
            border-radius: 40% 40% 0 0
        }

        .tippy-popper[x-placement^=top] .tippy-roundarrow {
            bottom: -7px;
            bottom: -6.5px;
            -webkit-transform-origin: 50% 0;
            transform-origin: 50% 0;
            margin: 0 3px
        }

        .tippy-popper[x-placement^=top] .tippy-roundarrow svg {
            position: absolute;
            left: 0;
            -webkit-transform: rotate(180deg);
            transform: rotate(180deg)
        }

        .tippy-popper[x-placement^=top] .tippy-arrow {
            border-top: 8px solid #333;
            border-right: 8px solid transparent;
            border-left: 8px solid transparent;
            bottom: -7px;
            margin: 0 3px;
            -webkit-transform-origin: 50% 0;
            transform-origin: 50% 0
        }

        .tippy-popper[x-placement^=top] .tippy-backdrop {
            -webkit-transform-origin: 0 25%;
            transform-origin: 0 25%
        }

        .tippy-popper[x-placement^=top] .tippy-backdrop[data-state=visible] {
            -webkit-transform: scale(1) translate(-50%, -55%);
            transform: scale(1) translate(-50%, -55%)
        }

        .tippy-popper[x-placement^=top] .tippy-backdrop[data-state=hidden] {
            -webkit-transform: scale(.2) translate(-50%, -45%);
            transform: scale(.2) translate(-50%, -45%);
            opacity: 0
        }

        .tippy-popper[x-placement^=top] [data-animation=shift-toward][data-state=visible] {
            -webkit-transform: translateY(-10px);
            transform: translateY(-10px)
        }

        .tippy-popper[x-placement^=top] [data-animation=shift-toward][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateY(-20px);
            transform: translateY(-20px)
        }

        .tippy-popper[x-placement^=top] [data-animation=perspective] {
            -webkit-transform-origin: bottom;
            transform-origin: bottom
        }

        .tippy-popper[x-placement^=top] [data-animation=perspective][data-state=visible] {
            -webkit-transform: perspective(700px) translateY(-10px) rotateX(0);
            transform: perspective(700px) translateY(-10px) rotateX(0)
        }

        .tippy-popper[x-placement^=top] [data-animation=perspective][data-state=hidden] {
            opacity: 0;
            -webkit-transform: perspective(700px) translateY(0) rotateX(60deg);
            transform: perspective(700px) translateY(0) rotateX(60deg)
        }

        .tippy-popper[x-placement^=top] [data-animation=fade][data-state=visible] {
            -webkit-transform: translateY(-10px);
            transform: translateY(-10px)
        }

        .tippy-popper[x-placement^=top] [data-animation=fade][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateY(-10px);
            transform: translateY(-10px)
        }

        .tippy-popper[x-placement^=top] [data-animation=shift-away][data-state=visible] {
            -webkit-transform: translateY(-10px);
            transform: translateY(-10px)
        }

        .tippy-popper[x-placement^=top] [data-animation=shift-away][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateY(0);
            transform: translateY(0)
        }

        .tippy-popper[x-placement^=top] [data-animation=scale] {
            -webkit-transform-origin: bottom;
            transform-origin: bottom
        }

        .tippy-popper[x-placement^=top] [data-animation=scale][data-state=visible] {
            -webkit-transform: translateY(-10px) scale(1);
            transform: translateY(-10px) scale(1)
        }

        .tippy-popper[x-placement^=top] [data-animation=scale][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateY(-10px) scale(.5);
            transform: translateY(-10px) scale(.5)
        }

        .tippy-popper[x-placement^=bottom] .tippy-backdrop {
            border-radius: 0 0 30% 30%
        }

        .tippy-popper[x-placement^=bottom] .tippy-roundarrow {
            top: -7px;
            -webkit-transform-origin: 50% 100%;
            transform-origin: 50% 100%;
            margin: 0 3px
        }

        .tippy-popper[x-placement^=bottom] .tippy-roundarrow svg {
            position: absolute;
            left: 0;
            -webkit-transform: rotate(0);
            transform: rotate(0)
        }

        .tippy-popper[x-placement^=bottom] .tippy-arrow {
            border-bottom: 8px solid #333;
            border-right: 8px solid transparent;
            border-left: 8px solid transparent;
            top: -7px;
            margin: 0 3px;
            -webkit-transform-origin: 50% 100%;
            transform-origin: 50% 100%
        }

        .tippy-popper[x-placement^=bottom] .tippy-backdrop {
            -webkit-transform-origin: 0 -50%;
            transform-origin: 0 -50%
        }

        .tippy-popper[x-placement^=bottom] .tippy-backdrop[data-state=visible] {
            -webkit-transform: scale(1) translate(-50%, -45%);
            transform: scale(1) translate(-50%, -45%)
        }

        .tippy-popper[x-placement^=bottom] .tippy-backdrop[data-state=hidden] {
            -webkit-transform: scale(.2) translate(-50%);
            transform: scale(.2) translate(-50%);
            opacity: 0
        }

        .tippy-popper[x-placement^=bottom] [data-animation=shift-toward][data-state=visible] {
            -webkit-transform: translateY(10px);
            transform: translateY(10px)
        }

        .tippy-popper[x-placement^=bottom] [data-animation=shift-toward][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateY(20px);
            transform: translateY(20px)
        }

        .tippy-popper[x-placement^=bottom] [data-animation=perspective] {
            -webkit-transform-origin: top;
            transform-origin: top
        }

        .tippy-popper[x-placement^=bottom] [data-animation=perspective][data-state=visible] {
            -webkit-transform: perspective(700px) translateY(10px) rotateX(0);
            transform: perspective(700px) translateY(10px) rotateX(0)
        }

        .tippy-popper[x-placement^=bottom] [data-animation=perspective][data-state=hidden] {
            opacity: 0;
            -webkit-transform: perspective(700px) translateY(0) rotateX(-60deg);
            transform: perspective(700px) translateY(0) rotateX(-60deg)
        }

        .tippy-popper[x-placement^=bottom] [data-animation=fade][data-state=visible] {
            -webkit-transform: translateY(10px);
            transform: translateY(10px)
        }

        .tippy-popper[x-placement^=bottom] [data-animation=fade][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateY(10px);
            transform: translateY(10px)
        }

        .tippy-popper[x-placement^=bottom] [data-animation=shift-away][data-state=visible] {
            -webkit-transform: translateY(10px);
            transform: translateY(10px)
        }

        .tippy-popper[x-placement^=bottom] [data-animation=shift-away][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateY(0);
            transform: translateY(0)
        }

        .tippy-popper[x-placement^=bottom] [data-animation=scale] {
            -webkit-transform-origin: top;
            transform-origin: top
        }

        .tippy-popper[x-placement^=bottom] [data-animation=scale][data-state=visible] {
            -webkit-transform: translateY(10px) scale(1);
            transform: translateY(10px) scale(1)
        }

        .tippy-popper[x-placement^=bottom] [data-animation=scale][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateY(10px) scale(.5);
            transform: translateY(10px) scale(.5)
        }

        .tippy-popper[x-placement^=left] .tippy-backdrop {
            border-radius: 50% 0 0 50%
        }

        .tippy-popper[x-placement^=left] .tippy-roundarrow {
            right: -12px;
            -webkit-transform-origin: 33.33333333% 50%;
            transform-origin: 33.33333333% 50%;
            margin: 3px 0
        }

        .tippy-popper[x-placement^=left] .tippy-roundarrow svg {
            position: absolute;
            left: 0;
            -webkit-transform: rotate(90deg);
            transform: rotate(90deg)
        }

        .tippy-popper[x-placement^=left] .tippy-arrow {
            border-left: 8px solid #333;
            border-top: 8px solid transparent;
            border-bottom: 8px solid transparent;
            right: -7px;
            margin: 3px 0;
            -webkit-transform-origin: 0 50%;
            transform-origin: 0 50%
        }

        .tippy-popper[x-placement^=left] .tippy-backdrop {
            -webkit-transform-origin: 50% 0;
            transform-origin: 50% 0
        }

        .tippy-popper[x-placement^=left] .tippy-backdrop[data-state=visible] {
            -webkit-transform: scale(1) translate(-50%, -50%);
            transform: scale(1) translate(-50%, -50%)
        }

        .tippy-popper[x-placement^=left] .tippy-backdrop[data-state=hidden] {
            -webkit-transform: scale(.2) translate(-75%, -50%);
            transform: scale(.2) translate(-75%, -50%);
            opacity: 0
        }

        .tippy-popper[x-placement^=left] [data-animation=shift-toward][data-state=visible] {
            -webkit-transform: translateX(-10px);
            transform: translateX(-10px)
        }

        .tippy-popper[x-placement^=left] [data-animation=shift-toward][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateX(-20px);
            transform: translateX(-20px)
        }

        .tippy-popper[x-placement^=left] [data-animation=perspective] {
            -webkit-transform-origin: right;
            transform-origin: right
        }

        .tippy-popper[x-placement^=left] [data-animation=perspective][data-state=visible] {
            -webkit-transform: perspective(700px) translateX(-10px) rotateY(0);
            transform: perspective(700px) translateX(-10px) rotateY(0)
        }

        .tippy-popper[x-placement^=left] [data-animation=perspective][data-state=hidden] {
            opacity: 0;
            -webkit-transform: perspective(700px) translateX(0) rotateY(-60deg);
            transform: perspective(700px) translateX(0) rotateY(-60deg)
        }

        .tippy-popper[x-placement^=left] [data-animation=fade][data-state=visible] {
            -webkit-transform: translateX(-10px);
            transform: translateX(-10px)
        }

        .tippy-popper[x-placement^=left] [data-animation=fade][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateX(-10px);
            transform: translateX(-10px)
        }

        .tippy-popper[x-placement^=left] [data-animation=shift-away][data-state=visible] {
            -webkit-transform: translateX(-10px);
            transform: translateX(-10px)
        }

        .tippy-popper[x-placement^=left] [data-animation=shift-away][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateX(0);
            transform: translateX(0)
        }

        .tippy-popper[x-placement^=left] [data-animation=scale] {
            -webkit-transform-origin: right;
            transform-origin: right
        }

        .tippy-popper[x-placement^=left] [data-animation=scale][data-state=visible] {
            -webkit-transform: translateX(-10px) scale(1);
            transform: translateX(-10px) scale(1)
        }

        .tippy-popper[x-placement^=left] [data-animation=scale][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateX(-10px) scale(.5);
            transform: translateX(-10px) scale(.5)
        }

        .tippy-popper[x-placement^=right] .tippy-backdrop {
            border-radius: 0 50% 50% 0
        }

        .tippy-popper[x-placement^=right] .tippy-roundarrow {
            left: -12px;
            -webkit-transform-origin: 66.66666666% 50%;
            transform-origin: 66.66666666% 50%;
            margin: 3px 0
        }

        .tippy-popper[x-placement^=right] .tippy-roundarrow svg {
            position: absolute;
            left: 0;
            -webkit-transform: rotate(-90deg);
            transform: rotate(-90deg)
        }

        .tippy-popper[x-placement^=right] .tippy-arrow {
            border-right: 8px solid #333;
            border-top: 8px solid transparent;
            border-bottom: 8px solid transparent;
            left: -7px;
            margin: 3px 0;
            -webkit-transform-origin: 100% 50%;
            transform-origin: 100% 50%
        }

        .tippy-popper[x-placement^=right] .tippy-backdrop {
            -webkit-transform-origin: -50% 0;
            transform-origin: -50% 0
        }

        .tippy-popper[x-placement^=right] .tippy-backdrop[data-state=visible] {
            -webkit-transform: scale(1) translate(-50%, -50%);
            transform: scale(1) translate(-50%, -50%)
        }

        .tippy-popper[x-placement^=right] .tippy-backdrop[data-state=hidden] {
            -webkit-transform: scale(.2) translate(-25%, -50%);
            transform: scale(.2) translate(-25%, -50%);
            opacity: 0
        }

        .tippy-popper[x-placement^=right] [data-animation=shift-toward][data-state=visible] {
            -webkit-transform: translateX(10px);
            transform: translateX(10px)
        }

        .tippy-popper[x-placement^=right] [data-animation=shift-toward][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateX(20px);
            transform: translateX(20px)
        }

        .tippy-popper[x-placement^=right] [data-animation=perspective] {
            -webkit-transform-origin: left;
            transform-origin: left
        }

        .tippy-popper[x-placement^=right] [data-animation=perspective][data-state=visible] {
            -webkit-transform: perspective(700px) translateX(10px) rotateY(0);
            transform: perspective(700px) translateX(10px) rotateY(0)
        }

        .tippy-popper[x-placement^=right] [data-animation=perspective][data-state=hidden] {
            opacity: 0;
            -webkit-transform: perspective(700px) translateX(0) rotateY(60deg);
            transform: perspective(700px) translateX(0) rotateY(60deg)
        }

        .tippy-popper[x-placement^=right] [data-animation=fade][data-state=visible] {
            -webkit-transform: translateX(10px);
            transform: translateX(10px)
        }

        .tippy-popper[x-placement^=right] [data-animation=fade][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateX(10px);
            transform: translateX(10px)
        }

        .tippy-popper[x-placement^=right] [data-animation=shift-away][data-state=visible] {
            -webkit-transform: translateX(10px);
            transform: translateX(10px)
        }

        .tippy-popper[x-placement^=right] [data-animation=shift-away][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateX(0);
            transform: translateX(0)
        }

        .tippy-popper[x-placement^=right] [data-animation=scale] {
            -webkit-transform-origin: left;
            transform-origin: left
        }

        .tippy-popper[x-placement^=right] [data-animation=scale][data-state=visible] {
            -webkit-transform: translateX(10px) scale(1);
            transform: translateX(10px) scale(1)
        }

        .tippy-popper[x-placement^=right] [data-animation=scale][data-state=hidden] {
            opacity: 0;
            -webkit-transform: translateX(10px) scale(.5);
            transform: translateX(10px) scale(.5)
        }

        .tippy-tooltip {
            position: relative;
            color: #fff;
            border-radius: .25rem;
            font-size: .875rem;
            padding: .3125rem .5625rem;
            line-height: 1.4;
            text-align: center;
            background-color: #333
        }

        .tippy-tooltip[data-size=small] {
            padding: .1875rem .375rem;
            font-size: .75rem
        }

        .tippy-tooltip[data-size=large] {
            padding: .375rem .75rem;
            font-size: 1rem
        }

        .tippy-tooltip[data-animatefill] {
            overflow: hidden;
            background-color: transparent
        }

        .tippy-tooltip[data-interactive],
        .tippy-tooltip[data-interactive] .tippy-roundarrow path {
            pointer-events: auto
        }

        .tippy-tooltip[data-inertia][data-state=visible] {
            transition-timing-function: cubic-bezier(.54, 1.5, .38, 1.11)
        }

        .tippy-tooltip[data-inertia][data-state=hidden] {
            transition-timing-function: ease
        }

        .tippy-arrow,
        .tippy-roundarrow {
            position: absolute;
            width: 0;
            height: 0
        }

        .tippy-roundarrow {
            width: 18px;
            height: 7px;
            fill: #333;
            pointer-events: none
        }

        .tippy-backdrop {
            position: absolute;
            background-color: #333;
            border-radius: 50%;
            width: calc(110% + 2rem);
            left: 50%;
            top: 50%;
            z-index: -1;
            transition: all cubic-bezier(.46, .1, .52, .98);
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden
        }

        .tippy-backdrop:after {
            content: "";
            float: left;
            padding-top: 100%
        }

        .tippy-backdrop+.tippy-content {
            transition-property: opacity;
            will-change: opacity
        }

        .tippy-backdrop+.tippy-content[data-state=visible] {
            opacity: 1
        }

        .tippy-backdrop+.tippy-content[data-state=hidden] {
            opacity: 0
        }
    </style>
</head>

<body class="bg-transparent">
    <nav class="x-lobby-header navbar bg-transparent navbar-expand-lg -anon">
        <div class="container -inner-wrapper">
            <div class="-header-left">
                <a href="#" class="navbar-brand d-sm-block d-none">
                    <img class="-logo -header-logo" src="{{ secure_asset('/images/habanero/icons/logo-habanero-lobby.png')}}"
                        alt="habanero">
                </a>
            </div>
            <div id="headerContent">
                <ul class="navbar-nav ml-auto">
                    <div class="d-flex align-items-center m-auto --profile-detail">
                        @if (session('token'))
                        <div class="-profile-detail-box d-flex">
                            <div class="mr-3">Welcome {{ session('phone_number')}}</div>
                            <div class="mr-2 x-text-pinnacle">
                                <span class="text-muted">Balance : </span>
                                <span class="font-weight-bold" id="customer-balance">
                                    <span class="text-green-lighter">{{ number_format($balance, 2, ".", ",") }}</span>

                                </span>
                                <button type="button" class="btn btn-link p-0" onclick="Fx_refresh_credit()">
                                    <img id="imgrefresh" class="animated" src="{{secure_asset('img/refresh-button.png')}}"
                                        width="16px">
                                </button>
                            </div>
                        </div>
                        @endif
                        <a href="/lobby" class="-ic-home">
                            <img src="{{ secure_asset('/images/habanero/icons/lobby-ic-home.png')}}" alt="Home icon svg">
                        </a>
                    </div>
                </ul>
            </div>
        </div>
    </nav>
    <div id="main__content" class="">
        <div class="x-habanero-wrapper-container">
            <div class="-top-wrapper">
                <div class="container -inner-container">
                    <h1 class="-title h2">Explore our world</h1>
                    <p class="-subtitle mt-3">Discover Sheer Gaming with our collection of Slots, Table games and more!
                    </p>

                    <img src="{{ secure_asset('/images/habanero/icons/habanero-icon-pharaoh.png')}}"
                        alt="Recommend icon for Habanero" class="-recommended-icon img-fluid">
                </div>
            </div>
            <div class="-games-wrapper">
                <ul class="nav nav-pills" id="games-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="slot-tab" data-toggle="pill" role="tab" aria-controls="slot"
                            aria-selected="true">
                            Slots ({{ count($games["slot_game"]["game_id"]) }})
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="table-tab" data-toggle="pill" role="tab" aria-controls="table"
                            aria-selected="false">
                            Table Games ({{ count($games["table_game"]["game_id"]) }})
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="poker-tab" data-toggle="pill" role="tab" aria-controls="poker"
                            aria-selected="false">
                            Video Poker ({{ count($games["video_poker"]["game_id"]) }})
                        </a>
                    </li>
                </ul>

                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="slot" role="tabpanel" aria-labelledby="slot-tab">

                        @foreach ( $games as $key => $game )
                        <ul class="-inner-wrapper" id="{{ $key }}_store">
                            @foreach ($game["game_id"] as $key_game => $game_play)
                            <li>
                                @if ( $key != "video_poker" )
                                <div class="-item">
                                    <a href="/api/launch_game/habanero/{{$key_game}}" target="_blank">
                                        <img src="https://core.igame.bet/build/admin/img/lobby_habanero/{{$key_game}}.png"
                                            alt="" class="-image -img-fluid">
                                        <div class="-ic-play" style="padding: 0 0 0 0;">
                                            <img src="{{secure_asset('/images/habanero/icons/play-button.png')}}" alt=""
                                                class=" " style="width:100%;">
                                        </div>
                                        <div class="-title">
                                            <span>{{ $key_game }}</span>
                                        </div>
                                    </a>
                                </div>
                                @endif
                                @if ( $key == "video_poker" )
                                <div class="-item">
                                    <a href="/get_provider_games/habanero/{{$key_game}}" target="_blank">
                                        <img src="https://core.igame.bet/build/admin/img/lobby_habanero/{{$key_game}}.png"
                                            alt="" class="-image -img-fluid">
                                        <div class="-ic-play" style="padding: 0 0 0 0;">
                                            <img src="{{secure_asset('/images/habanero/icons/play-button.png')}}" alt=""
                                                class=" " style="width:100%;">
                                        </div>
                                        <div class="-title">
                                            <span>{{ $key_game }}</span>
                                        </div>
                                    </a>
                                </div>
                                @endif
                            </li>
                            @endforeach

                        </ul>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{secure_asset('js/header_logon.js?v='.time())}}"></script>
    <script src="{{secure_asset('js/main.js?v='.time())}}"></script>


    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script>
        window.jQuery ||
            document.write(
                '<script src="js/vendor/jquery-3.3.1.min.js"><\/script>'
            );

    </script>

    <script>
        $(document).ready(function () {
            html_slot_game_store = $("#slot_game_store")[0];
            html_table_game_store = $("#table_game_store")[0];
            html_video_poker_store = $("#video_poker_store")[0];
            $("#slot").html(html_slot_game_store);

            $("#slot-tab").on("click", function () {
                $("#slot-tab").addClass("active");
                $("#table-tab").removeClass("active");
                $("#poker-tab").removeClass("active");
                $("#slot").html(html_slot_game_store);

            });
            $("#table-tab").on("click", function () {
                $("#table-tab").addClass("active");
                $("#slot-tab").removeClass("active");
                $("#poker-tab").removeClass("active");
                $("#slot").html(html_table_game_store);
            });
            $("#poker-tab").on("click", function () {
                $("#poker-tab").addClass("active");
                $("#table-tab").removeClass("active");
                $("#slot-tab").removeClass("active");
                $("#slot").html(html_video_poker_store);

            });
        });
    </script>
</body>

</html>
