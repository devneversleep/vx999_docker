<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="canonical" href="{{ Request::url() }}">

        <link rel="manifest" href="{{secure_asset('site.webmanifest')}}">
        <!-- <link rel="apple-touch-icon" href="icon.png"> -->
        <link rel="icon" href="{{secure_asset('/img/favicon-32x32.png')}}" type="image/x-icon">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="{{secure_asset('css/styles.css?v=' . time())}}">
        <link rel="stylesheet" href="{{secure_asset('css/animate.css')}}">

        <link rel="preload" as="font" href="/fonts/DB HelvethaicaMon X Med.f4018213.ttf" type="font/ttf"
            crossorigin="anonymous">
        <link rel="preload" as="font" href="/fonts/DB HelvethaicaMon X.cfd3be53.ttf" type="font/ttf"
            crossorigin="anonymous">
        <link rel="stylesheet" href="{{secure_asset('css/owl.carousel.min.css')}}">

        {{-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/> --}}
        {{-- <link rel="stylesheet" type="text/css" href="{{secure_asset('css/slick.css')}}"/> --}}

        <!-- <link rel="stylesheet" type="text/css" href="slick/slick-theme.css" /> -->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script>
            window.jQuery || document.write('<script src="{{secure_asset("js/vendor/jquery-3.3.1.min.js")}}"><script>')

        </script>

        <script type="text/javascript" src="{{secure_asset('js/vendor/popper.min.js')}}"></script>
        <script type="text/javascript" src="{{secure_asset('js/vendor/bootstrap.min.js')}}"></script>

        <script type="text/javascript" src="{{secure_asset('js/plugins.js')}}"></script>
        <script type="text/javascript" src="{{secure_asset('js/vendor/owl.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{secure_asset('js/main.js?v='.time())}}"></script>

        {{-- <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> --}}
        {{-- <script type="text/javascript" src="{{secure_asset('js/vendor/slick.min.js')}}"></script> --}}

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <link rel="stylesheet" href="{{secure_asset('css/font-awesome.min.css')}}" />

        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        </script>
    </head>
    <body class="bg-exchange -login-admin">
        <div class="container-fluid -admin position-relative">
            <div class="box-light-admin">
                <img class="float-admin" src="{{secure_asset('img/bg_float.png')}}" alt="">
            </div>
            <div class="box-float-admin">
                <img class="img-float-admin" src="{{secure_asset('img/bg_extra8.png')}}" alt="">
            </div>
            <div class="row h-100">
                <div class="col-lg-6 col-12 box-hide">

                </div>
                <div class="col-lg-6 col-12 d-flex align-items-center justify-content-center -x-login_admin">
                    <div class="w-100">
                        <div class="text-center mb-4">
                            <img class="logo-login-admin" src="{{secure_asset('img/logo_main.png')}}" alt="">
                        </div>

                        <div class="zone-login-admin ">
                            <div class="box-login-admin m-auto">
                                <form action="/api/login-admin" method="post">
                                    @csrf
                                    
                                    <div class="text-center d-flex flex-column">
                                        
                                        <h3 class="custom-modal-title mb-4">เข้าสู่ระบบ             
                                        </h3>
                                        
                                        {{-- <div class="input-group mb-3">
                                            <div class="dropdown"> 
                                                <button class="btn btn-success-  
                                                        dropdown-toggle h-100 position-relative" type="button" 
                                                        data-toggle="dropdown"
                                                        aria-haspopup="true" 
                                                        aria-expanded="false"> 
                                                        <img class="flag-thai" src="{{secure_asset('img/ธง/1x/ic_mini_thai.png')}}" width="35" > 
                                                        <img class="flag-lao d-none" src="{{secure_asset('img/ธง/1x/ic_mini_lao.png')}}" width="35" > 
                                                </button> 
                                    
                                                <ul class="dropdown-menu" 
                                                    aria-labelledby="dropdownMenuButton"> 
                                                    <li class="dropdown-item select-thai"> 
                                                        <img class="mr-2" src="{{secure_asset('img/ธง/1x/ic_mini_thai.png')}}" width="35" > 
                                                        Thailand (ປະເທດໄທ) 
                                                        <span class="float-right">+66</span>                                     
                                                    </li> 
                                                    <li class="dropdown-item select-lao"> 
                                                        <img class="mr-2" src="{{secure_asset('img/ธง/1x/ic_mini_lao.png')}}" width="35" >        
                                                        laos (ປະເທດລາວ)   
                                                        <span class="float-right">+856</span>                                     
                                                    </li> 
                                                </ul> 
                                            </div>
                
                                            <input type="tel" id="login_phone_number" name="phone_number" required class="custom-form-control form-control" placeholder="ເບີ​ໂທລະ​ສັບ" >
                                        </div> --}}
                                        <div class="-x-input-icon mb-3 flex-column text-center">
                                            {{-- <img src="{{secure_asset('img/ic_mini_password.png')}}" class="-icon" alt="login" width="12"> --}}
                                            <i class="fas fa-user -admin"></i>
                                            <input type="text" id="name" name="name" required class="custom-form-control form-control" placeholder="บัญชีผู้ใช้" >

                                        </div>
                                        <div class="-x-input-icon mb-3 flex-column text-center">
                                            {{-- <img src="{{secure_asset('img/ic_mini_password.png')}}" class="-icon" alt="login" width="12"> --}}
                                            <i class="fas fa-key"></i>
                                            <input type="password" id="password" required name="password"
                                                class="custom-form-control form-control" placeholder="รหัสผ่าน">
                                        </div>
                                        {{-- <div class="text-right">
                                            <a href="#forgotModal" data-toggle="modal" data-dismiss="modal"
                                                class="text-red"><u>ລືມລະຫັດຜ່ານ</u></a>
                                        </div> --}}
                                        @if ($errors->any())
                                        <center>
                                            <div class="alert alert-danger mt-1 mb-0">
                                                {{ $errors->first() }}<br> 
                                            </div>
                                        </center>
                                        @enderror
                                        <div class="text-center mt-3">
                                            <button type="summit" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
                                                เข้าสู่ระบบ
                                            </button>
                                        </div>
                                        
                                    

                                        {{-- <hr class="hr-border-glow w-100 my-4">
                                        <div class="text-center modal-contact">
                                            <span>ພົບປັນຫາ</span>
                                            <a href="{{config('variable.line')}}" class="link-message " target="_blank">
                                                <span> ຕິດຕໍ່ບໍລິການລູກຄ້າ</span></a>
                                        </div> --}}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                    {{-- <div class="modal d-block custom-modal -login-admin" >
                        <div class="modal-dialog modal-dialog-centered modal-size">
                            
                            <div class="modal-content">
                                
                                <div class="modal-body">
                                    

                                    <form action="/api/login-admin" method="post">
                                        @csrf
                                        
                                        <div class="text-center d-flex flex-column">
                                            
                                            <h3 class="custom-modal-title mb-4">เข้าสู่ระบบ                
                                            </h3>
                                            
                                            <div class="input-group mb-3">
                                                <div class="dropdown"> 
                                                    <button class="btn btn-success-  
                                                            dropdown-toggle h-100 position-relative" type="button"                                                     
                                                            data-toggle="dropdown"
                                                            aria-haspopup="true" 
                                                            aria-expanded="false"> 
                                                            <img class="flag-thai" src="{{secure_asset('img/ธง/1x/ic_mini_thai.png')}}" width="35" > 
                                                            <img class="flag-lao d-none" src="{{secure_asset('img/ธง/1x/ic_mini_lao.png')}}" width="35" > 
                                                    </button> 
                                          
                                                    <ul class="dropdown-menu" 
                                                        aria-labelledby="dropdownMenuButton"> 
                                                        <li class="dropdown-item select-thai"> 
                                                            <img class="mr-2" src="{{secure_asset('img/ธง/1x/ic_mini_thai.png')}}" width="35" > 
                                                            Thailand (ປະເທດໄທ) 
                                                            <span class="float-right">+66</span>                                     
                                                        </li> 
                                                        <li class="dropdown-item select-lao"> 
                                                            <img class="mr-2" src="{{secure_asset('img/ธง/1x/ic_mini_lao.png')}}" width="35" >        
                                                            laos (ປະເທດລາວ)   
                                                            <span class="float-right">+856</span>                                     
                                                        </li> 
                                                    </ul> 
                                                </div>
                    
                                                <input type="tel" id="login_phone_number" name="phone_number" required class="custom-form-control form-control" placeholder="ເບີ​ໂທລະ​ສັບ" >
                                            </div>

                                            <div class="-x-input-icon mb-3 flex-column text-center">
                                                <i class="fas fa-key"></i>
                                                <input type="password" id="password" required name="password"
                                                    class="custom-form-control form-control" placeholder="รหัสผ่าน">
                                            </div>
                                           
                                            @if ($errors->any())
                                            <center>
                                                <div class="alert alert-danger mt-1 mb-0">
                                                    {{ $errors->first() }}<br> 
                                                </div>
                                            </center>
                                            @enderror
                                            <div class="text-center mt-3">
                                                <button type="summit" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit">
                                                    เข้าสู่ระบบ
                                                </button>
                                            </div>
                                          
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
        <script>
            $('.select-thai').click(function(){
                $('.flag-lao').addClass('d-none');
                $('.flag-thai').removeClass('d-none');
            });
            $('.select-lao').click(function(){
                $('.flag-lao').removeClass('d-none');
                $('.flag-thai').addClass('d-none');
            });
        </script>
    </body>
</html>
