<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>LAOCASINO</title>
        <link rel="canonical" href="{{ Request::url() }}">

        <link rel="manifest" href="{{secure_asset('site.webmanifest')}}">
        <!-- <link rel="apple-touch-icon" href="icon.png"> -->
        <link rel="icon" href="{{secure_asset('favicon-32x32.png')}}" type="image/x-icon">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="{{secure_asset('css/styles.css?v=' . time())}}">
        <link rel="stylesheet" href="{{secure_asset('css/animate.css')}}">

        <link rel="preload" as="font" href="/fonts/DB HelvethaicaMon X Med.f4018213.ttf" type="font/ttf"
            crossorigin="anonymous">
        <link rel="preload" as="font" href="/fonts/DB HelvethaicaMon X.cfd3be53.ttf" type="font/ttf"
            crossorigin="anonymous">
        <link rel="stylesheet" href="{{secure_asset('css/owl.carousel.min.css')}}">

        {{-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/> --}}
        {{-- <link rel="stylesheet" type="text/css" href="{{secure_asset('css/slick.css')}}"/> --}}

        <!-- <link rel="stylesheet" type="text/css" href="slick/slick-theme.css" /> -->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script>
            window.jQuery || document.write('<script src="{{secure_asset("js/vendor/jquery-3.3.1.min.js")}}"><script>')

        </script>

        <script type="text/javascript" src="{{secure_asset('js/vendor/popper.min.js')}}"></script>
        <script type="text/javascript" src="{{secure_asset('js/vendor/bootstrap.min.js')}}"></script>

        <script type="text/javascript" src="{{secure_asset('js/plugins.js')}}"></script>
        <script type="text/javascript" src="{{secure_asset('js/vendor/owl.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{secure_asset('js/main.js?v='.time())}}"></script>

        {{-- <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> --}}
        {{-- <script type="text/javascript" src="{{secure_asset('js/vendor/slick.min.js')}}"></script> --}}

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <link rel="stylesheet" href="{{secure_asset('css/font-awesome.min.css')}}" />

        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        </script>
    </head>
    <body class="bg-exchange">
        <div class="nav-admin">
            <nav class="navbar navbar-expand-lg navbar-light bg-light mt-0 nav-bg-exchange">
                <div class="container">
                    <a class="navbar-brand" href="/">
                        <img class="logo-exchange" src="{{secure_asset('img/logo.png')}}" alt="">
                    </a>
                    <button class="navbar-toggler -admin" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon">
                            <i class="fas fa-bars -admin"></i>
                        </span>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav w-100 -exchange">
                            <li class="nav-item mx-2">
                                <a class="nav-link text-white" href="exchange-rate">ตั้งค่าเรทค่าเงิน </a>
                                <hr class="hr-active m-0">
                            </li>
                            <li class="nav-item  mx-2 active">
                                <a class="nav-link text-white" href="select_bank">บัญชีขาเข้า</a>
                                <hr class="hr-active m-0">

                            </li>
                            <li class="nav-item  ml-2 logout-admin">
                                <a class="nav-link text-white"  data-toggle="modal" data-target="#LogoutModal-Admin">
                                    <i class="fas fa-power-off"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="box-all-bank d-flex align-items-center">
                        <div class="container ">
                            <form action="">
                                <div class="row w-100 m-auto">
                                    <div class="col-lg-10 col-md-12 col-sm-12">
                                        <div class="d-flex ">
                                            <div class="owl-carousel owl-theme text-center">
                                                @foreach($banks as $bank)
                                                    <div class="item">
                                                        <div class="zone-select-bank mr-3">
                                                            <div class="check-box-">
                                                                <input onclick="selectBank('cb-bank-{{ $bank->id }}')" class="from-checkbox d-none" id="cb-bank-{{ $bank->id }}" type="checkbox" aria-label="Checkbox for following text input">
                                                                <label class="form-check-label-" for="cb-bank-{{ $bank->id }}"></label>
                                                            </div>
                                                            <div class="box-bank">
                                                                <img class="ic_select_bank " src="{{secure_asset('img/ic_bank.png')}}" alt="">
                                                            </div>
                                                            <div class="box-data text-left">
                                                                <p class="m-0 f-5">ธนาคาร BCEL ONE</p>
                                                                <p class="m-0">{{ $bank->name }}</p>
                                                                <p class="m-0">เลขที่บัญชี</p>
                                                                <p class="m-0">{{ $bank->number }}</p>
                                                            </div>
                                                        </div>           
                                                    </div>  
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-12">
                                        <a  data-toggle="collapse" href="#collapseAddBank" role="button" aria-expanded="false" aria-controls="collapseAddBank">
                                            <div class="text-center box-add-bank ml-4">
                                                <div>
                                                    <div>
                                                        <i class="fas fa-university"></i>
                                                    </div>
                                                    <div>
                                                        <span class="text-white">
                                                            เพิ่มบัญชี <br>
                                                            ธนาคาร
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> 
                                    </div>

                                    <div class="col-12 mt-5 text-center">
                                        <button type="button" onclick="changeActiveBanks()" class="btn btn-primary-modal d-block btn-lg btn-submit -admin m-auto">
                                            ยืนยัน
                                        </button>
                                    </div>  
                                </div>
                            </form>
                        </div>            
                </div>
                <div class="collapse x-add-bank" id="collapseAddBank">
                    <div class="zone-add-bank">
                        <div class="text-center box-form-add-bank">
                            <form action="">
                                <h3 class="text-yellow">เพิ่มธนาคาร</h3>
                                <div class="mt-3">
                                    <img class="ic-add-bank" src="{{secure_asset('img/ic_bank.png')}}" alt="">
                                </div>
                                <div class="mt-4 position-relative">
                                    <i class="far fa-credit-card -sb"></i>
                                    <input type="text" id="number" class="custom-form-control form-control pl-5" placeholder="ใส่เลขบัญชี">
                                </div>
                                <div class="mt-2 position-relative">
                                    <i class="fas fa-user-circle -sb"></i>
                                    <input type="text" id="name" class="custom-form-control form-control pl-5" placeholder="ชื่อ - นามสกุล">
                                </div>
                                <button type="button" id="btn_add_bank" class="btn btn-primary-modal d-block w-100 btn-lg btn-submit mt-2">
                                    ยืนยัน
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                
                    
            </div>
        </div>
        
        <a class="fix-btn-add-bank" data-toggle="collapse" href="#collapseAddBank" role="button" aria-expanded="false" aria-controls="collapseAddBank">
            <i class="fas fa-university"></i>
            <span class="text-white"> เพิ่มบัญชีธนาคาร </span>
        </a>
  

        <div class="modal fade custom-modal" id="LogoutModal-Admin" tabindex="-1" role="dialog" aria-labelledby="LogoutModal-Admin"
        aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-size">
                <div class="modal-content">
                    <div class="modal-body">
                        <form action="/api/logout-admin" method="post">
                            <div class="text-center d-flex flex-column">
                                <div class="my-3 flex-column text-center">
                                    <p class="f-5 text-yellow">คุณต้องการออกจากระบบ ?</p>
                                </div>
                                <div class="row">
                                    <div class="col-6 d-flex align-items-stretch">
                                        <button type="summit" class="btn btn-primary-dark-cancel w-100">ตกลง</button>
                                    </div>
                                    <div class="col-6 d-flex align-items-stretch">
                                        <button type="button" class="btn btn-primary-dark-logout  w-100" data-dismiss="modal"
                                            aria-label="Close">ยกเลิก</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{secure_asset('js/exchange_rate.js?v=' . time())}}"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="{{secure_asset('js/vendor/owl.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{secure_asset('js/main.js?v='.time())}}"></script>
        {{-- <script src="{{secure_asset('OwlCarousel2-2.3.4/docs/assets/owlcarousel/owl.carousel.min.js')}}"></script> --}}
        <script>
              $('.owl-carousel').owlCarousel({
                    loop:true,
                    margin:10,
                    lazyLoad:true,
                    responsiveClass:true,
                    dots:true,
                    nav:false,
                    responsive:{
                        0:{
                            items:1,
                            nav:true,
                            loop:false

                        },
                        991:{
                            items:2,
                            nav:false,
                            loop:false

                        },
                        1024:{
                            items:2,
                            nav:true,
                            loop:false
                        },
                        1440:{
                            items:3,
                            nav:true,
                            loop:false
                        }
                    }
                })


                // $('.box-add-bank').click(function(){
                //     $('.x-add-bank').toggleClass('d-none');
                // });
        </script>
        <script src="{{secure_asset('js/banks.js?v=' . time())}}"></script>
    </body>
</html>
<script>
    var selected_bank_id = null
    var banks = {!! $banks !!}
    
    $( document ).ready(function() {
        selected_bank_id = 'cb-bank-' + banks.filter(bank => bank.status == 1)[0].id
        $('#' + selected_bank_id).prop('checked', true);
        // console.log(selected_bank_id)
    });
    
    function selectBank(id)
    {
        if ($('#' + id).is(":checked")) {
            $('#' + selected_bank_id).prop('checked', false);
            selected_bank_id = id
        } else {
            $('#' + id).prop('checked', true);
        }  
    }
</script>
