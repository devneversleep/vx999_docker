@extends('layouts.main')
{{-- @section('title', 'ทดลองเล่น Sexy Gaming ทางเข้าเล่น เซ็กซี่บาคาร่า บริการ 24 ชม. ฝาก-ถอน แถมเครดิตฟรี')
@section('description', 'เปิดให้ทดลองเล่นบาคาร่า จากค่าย Sexy Gaming ฟรี พร้อมเครดิตสูง เล่นได้ไม่จำกัดรอบ พร้อมให้เครดิตสูง  เล่นได้ไม่จำกัดรอบ รวมทุกเกมส์เว็บตรง AE SEXY เล่นเสือมังกร เซ็กซี่บาคาร่า (Sexy Baccarat) สล็อต ป๊อกเด้ง ไฮโล พนันออนไลน์ครบวงจรที่ดีที่สุดในเวลานี้') --}}


@section('content')
<section class="section-one">
    <div class="bg-container">
        {{-- <img src="{{secure_asset('img/bg_card.png')}}" alt="icon chip" class="bg-dice"> --}}
    </div><!-- bg-container -->
    <div class="index-tab-container">
        <header class="header lobby-header">
            <nav class="navbar">
                <div class="container d-flex justify-content-between p-0 pr-md-3">
                    <a class="navbar-brand" href="#"><img src="{{secure_asset('')}}" class="img-fluid logo d-none"></a>
                    <div class="navbar-right">
                        <div class="profile-detail d-flex py-2" {{--size21--}}>
                            @if (Session::has('token'))
                            <div class="mr-2">Welcome! 
                                 @if ( strlen(Session::get('phone_number')) > 11)
                                    {{ substr(Session::get('phone_number'), 3) }}
                                @else 
                                    {{ Session::get('phone_number') }}
                                @endif
                            </div>
                            <div class="mr-2 x-text-pinnacle">
                                <span class="text-muted">Balance : </span>
                                <span class="font-weight-bold" id="customer-balance">
                                    <span id="user_credits"
                                        class="text-orange">{{number_format($accountinfo['current_balance'], 2,'.',',')}}</span>
                                </span>
                            </div>
                            <button type="button" class="btn btn-link py-0 px-2" onclick="Fx_refresh_credit()">
                                <img id="imgrefresh" class="animated" src="{{secure_asset('img/refresh-button.png')}}"
                                    width="16px">
                            </button>
                            @endif
                            <a href="/" class="ic-home"><img src="{{secure_asset('img/lobby-ic-home.png')}}"
                                    alt="Home icon svg"></a>
                        </div>
                    </div><!-- navbar-login -->
                </div>
            </nav>
        </header>



        <!--- Navbar Desktop --->
        <div class="banner-wrapper">
            <!-- style="background-image: url('img/goodgame-lobby-bg.png') -->
            <div class="ruby-lobby-cover inner-wrapper p-0 -lobby">
                <div class="container inner-wrapper -lobby h-100 p-0">
                    <div class="row">
                        <div class="col-lg-8 col-md-7 col-sm-12 box-girl-extra position-relative">
                            <div class="box-girl">
                                <img class="img-girl w-100" src="{{secure_asset('img/bg_extra7.png')}}" alt="">
                            </div>
                            <div class="box-dice-lobby">
                                <img class="img-dice-lobby" src="{{secure_asset('img/bg_extra6.png')}}" alt="">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5 col-sm-12 d-flex align-items-center box-logo-">
                            <div class="text-center box-index-">
                                <div class="">
                                    <img class="img-logo-index " src="{{secure_asset('img/logo_main.png')}}" alt="">
                                </div>
                                <div>
                                    <img class="img-text-index w-100" src="{{secure_asset('img/bg_extra4.png')}}" alt="">
                                </div>
                                <hr class="hr-border-glow">
                                <p>
                                    ເວັບດຽວທີ່ຮວມຄາສິໂນຊັ້ນນຳໄວ້ແລ້ວ ບໍລິການດ້ວຍລະບົບຝາກ-ຖອນ ອັດຕະໂນມັດ ຜ່ານທີມງານມືອາຊີບດ້ວຍປະສົບການກວ່າ 5 ປີ
                                </p>
                                {{-- @if (!Session::has('token'))
                                    <a  data-target="#register_modal" data-toggle="modal"  class="btn btn-warning"> ສະໜັກສະມາຊິກ</a>
                                @endif --}}

                            </div>
                        </div>
                    </div>

                </div>
            </div><!-- goodgame-lobby-cover -->
        </div><!-- banner-wrapper -->
</section>

<section class="section-two -lobby position-relative">
    <div class="bg_float_lobby">
        <img class="w-100" src="{{secure_asset('img/bg_float.png')}}" alt="">
    </div>
    
    <!-- Games section -->
    <div class="agents-wrapper pt-3 pt-lg-5 pb-5">

        <div class="container container-selector mt-5 position-relative">
            <div class="item-coin">
                <img class="img-chip-1" src="{{secure_asset('img/bg_extra8.png')}}" alt="">
            </div>
            @php
            $icon_game = $games;
            usort($icon_game, function ($a, $b) {
            $c = $a['enabled'] < $b['enabled']; $c .=$a['support_test'] < $b['support_test']; return $c; });
                usort($icon_game, function ($a, $b) { $c=($a['enabled'] < $b['enabled']) || ($a['support_test'] <
                $b['support_test']); return $c; }); @endphp
            <div class="text-center">
                <h2 class="h4 mb-0 pt-4 " style="color: #FECC69;">ຄາສິໂນອອນລາຍ <span class="f-5  "> @if (!session('token')) (ຮູບແບບທົດລອງຫຼີ້ນ) @endif</span></h2>
            </div>
        <div class="recommend-wrapper">
            <div class="d-xl-block d-none">
                <div class="mt-3 d-flex flex-wrap b-lobby-logo">
                    <hr class="hr-border-glow-float-top">
                    @foreach ($icon_game as $game)
                    @if ( $game['provider_type'] == 'casino' )
                    <div
                        class="cover @if (!$game['enabled']) cannot-entry -cannot-entry  @endif @if (!$game['support_test'] && !session('token') && $game['enabled']) cannot-entry -cannot-entry untestable @endif ">

                        {{-- @if ($game["code"] == 'sexy-bac')
                            <a href="https://vx365bet.com/ " class="text-white" target="_blank">
                        @else
                            <a href="api/launch_game/{{ $game['code'] }} " class="text-white" target="_blank">
                        @endif --}}
                        {{-- <a href=" {{ ( $game["code"] == 'sexy-bac' ? 'https://vx365bet.com/' : $game["code"] ) }}" class="text-white" target="_blank"> --}}
                        <a href="api/launch_game/{{ $game['code'] }} " class="text-white" target="_blank">
                            <div class="image-box">
                                <img src="{{ $game['image_1'] }}" alt={{ $game['name'] }} class="image">
                            </div>
                        </a>
                    </div><!-- cover -->
                    @endif

                    @endforeach
                </div>
            </div>
            <script>
                console.log('{{$game["code"]}}')
            </script>
            <div class="d-xl-none d-block">
                <div class="d-flex">
                    @foreach ($icon_game as $game)
                    @if ( $game['provider_type'] == 'casino')
                    <div
                        class="cover @if (!$game['enabled']) cannot-entry -cannot-entry  @endif @if (!$game['support_test'] && !session('token') && $game['enabled']) cannot-entry -cannot-entry untestable @endif ">
                        <a href="api/launch_game/{{ $game['code'] }}" class="text-white" target="_blank">
                            <div class="image-box">
                                <img src="{{ $game['image_1'] }}" alt={{ $game['name'] }} class="image">
                            </div>
                        </a>
                    </div><!-- cover -->
                    @endif
                    @endforeach
                </div>
            </div>
        </div><!-- recommend-wrapper -->
        <div class="mt-3 mt-md-5">
            <div class="text-center">
                {{-- <img src="{{secure_asset('img/2x/ic_play365@2x.png')}}" alt="LAOCASINO slot online" width="80px"
                height="80px"> --}}
                <h2 class="h4 mb-0 pt-4 " style="color: #FECC69;">ເກມສະລັອດອອນລາຍ <span class="f-5  ">@if (!session('token')) (ຮູບແບບທົດລອງຫຼີ້ນ) @endif</span></h2>
            </div>
            <div class="slot-wrapper">
                <div class="d-xl-block d-none">
                    <div class="mt-3 d-flex flex-wrap b-lobby-logo">
                        <hr class="hr-border-glow-float-top">
                        {{-- cannot-entry
                            untestable
                            coming-soon --}}
                        @foreach ($icon_game as $game)

                        @if ( $game['provider_type'] == 'slot' )
                        <div
                            class="cover @if (!$game['enabled']) cannot-entry -cannot-entry  @endif @if (!$game['support_test'] && !session('token') && $game['enabled']) cannot-entry -cannot-entry untestable @endif ">
                            <a href="api/get_provider_games/{{ $game['code'] }}" class="text-white" target="_blank">
                                <div class="image-box">
                                    <img src="{{ $game['image_1'] }}" alt="{{$game['name']}}" class="image lazyload">
                                </div>
                                <div class="mt-2">
                                    <img src="{{ $game['image_2'] }}" alt="{{ $game['name'] }}"
                                        class="image-logo lazyload">
                                </div>
                            </a>
                        </div><!-- cover -->
                        @endif

                        @endforeach
                    </div>
                </div>
                <div class="d-xl-none d-block">
                    <div class="my-3 d-flex">
                        @foreach ($icon_game as $game)
                        @if ( $game['provider_type'] == 'slot')
                        <div
                            class="cover @if (!$game['enabled']) cannot-entry -cannot-entry  @endif @if (!$game['support_test'] && !session('token') && $game['enabled']) cannot-entry -cannot-entry untestable @endif ">
                            <a href="api/get_provider_games/{{ $game['code'] }}" class="text-white" target="_blank">
                                <div class="image-box">
                                    <img src="{{ $game['image_1'] }}" alt="{{$game['name']}}" class="image lazyload">
                                </div>
                                <div class="mt-2">
                                    <img src="{{ $game['image_2'] }}" alt="{{ $game['name'] }}"
                                        class="image-logo lazyload">
                                </div>
                            </a>
                        </div><!-- cover -->
                        @endif
                        @endforeach
                    </div>
                </div>
            </div><!-- slot-wrapper -->
        </div>
        <div class="mt-3 mt-md-5">
            <div class="text-center">
                {{-- <img src="{{secure_asset('img/2x/ic_play365@2x.png')}}" alt="LAOCASINO sport-game online" width="80px"
                height="80px"> --}}
                <h2 class="h4 mb-0 pt-4 " style="color: #FECC69;">ເກມສະລັອດອອນລາຍ <span class="f-5  ">@if (!session('token')) (ຮູບແບບທົດລອງຫຼີ້ນ) @endif</span></h2>
            </div>

            <div class="sport-wrapper">
                <div class="d-xl-block d-none">
                    <div class="mt-3 d-flex flex-wrap b-lobby-logo ">
                    <hr class="hr-border-glow-float-top">
                        @foreach ($icon_game as $game)
                        @if ( $game['provider_type'] == 'sport')
                        <div
                            class="cover @if (!$game['enabled']) cannot-entry -cannot-entry  @endif @if (!$game['support_test'] && !session('token') && $game['enabled']) cannot-entry -cannot-entry untestable @endif ">
                            <a href="api/launch_game/{{ $game['code'] }}" class="text-white" target="_blank">
                                <div class="image-box">
                                    <img src="{{ $game['image_1'] }}" alt="{{$game['name']}}" class="image lazyload">
                                </div>
                                <div class="mt-2">
                                    <img src="{{ $game['image_2'] }}" alt="{{ $game['name'] }}"
                                        class="image-logo lazyload">
                                </div>
                            </a>
                        </div><!-- cover -->
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="d-xl-none d-block">
                    <div class="my-3 d-flex">
                        @foreach ($icon_game as $game)

                        @if ( $game['provider_type'] == 'sport')
                        <div
                            class="cover @if (!$game['enabled']) cannot-entry -cannot-entry  @endif @if (!$game['support_test'] && !session('token') && $game['enabled']) cannot-entry -cannot-entry untestable @endif">
                            <a href="api/launch_game/{{ $game['code'] }}" class="text-white" target="_blank">
                                <div class="image-box">
                                    <img src="{{ $game['image_1'] }}" alt="{{$game['name']}}" class="image lazyload">
                                </div>
                                <div class="mt-2">
                                    <img src="{{ $game['image_2'] }}" alt="{{ $game['name'] }}"
                                        class="image-logo lazyload">
                                </div>
                            </a>
                        </div><!-- cover -->
                        @endif
                        @endforeach
                    </div>
                </div>
            </div><!-- sport-wrapper-->
        </div>
    </div><!-- container-selector -->
    </div>
    {{-- </div><!-- index-tab-container --> --}}
</section><!-- section-two -->


@endsection
