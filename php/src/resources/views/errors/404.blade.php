<link rel="stylesheet" href="{{secure_asset('404/css/style.css')}}">
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="{{secure_asset('404/js/script.js')}}"></script>

<style>
    p {
        font-size: 2rem;
    }
</style>

<div class="not-found parallax">
    <div class="sky-bg"></div>
    <div class="wave-7"></div>
    <div class="wave-6"></div>
    <a class="wave-island" href="/">
        <img src="http://res.cloudinary.com/andrewhani/image/upload/v1524501929/404/island.svg" alt="Island">
    </a>
    <div class="wave-5"></div>
    <div class="wave-lost wrp">
        <span>4</span>
        <span>0</span>
        <span>4</span>
    </div>
    <div class="wave-4"></div>
    <div class="wave-boat">
        <img class="boat" src="http://res.cloudinary.com/andrewhani/image/upload/v1524501894/404/boat.svg" alt="Boat">
    </div>
    <div class="wave-3"></div>
    <div class="wave-2"></div>
    <div class="wave-1"></div>
    <div class="wave-message">
        <p>Page Not Found </p>
        <p>Sorry, but the page you were trying to view does not exist.</p>
        <p>Click on the island to return</p>
    </div>
</div>
